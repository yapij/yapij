import React from "react";

class Test extends React.Component {
  render() {
    return (
      <div className="Test">
        <h3>This is a test</h3>
        <p>{this.props.text}</p>
      </div>
    );
  }
}

export default Test;
