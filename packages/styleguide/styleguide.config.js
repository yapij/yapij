module.exports = {
  components: ["../re/src/components/SimpleCurator/SimpleCurator.js"], //"node_modules/@yapij/re/src/components/**/*.{js,jsx,ts,tsx}",
  ignore: ["../re/src/components/index.js"],
  styleguideComponents: {
    hoc: "../re/src/hoc",
  },
};
