import YapijServer from "./server/index.js";
import { yapijTopics, yapijActions, yapijReducers } from "./redux";

import * as c from "./components";
import { default as HOC_ } from "./hoc";

import "./common/icons.js";
import "./styles";

export function setReduxDefaults(reduxKey, pubSubKey) {
  var h = HOC_(reduxKey, pubSubKey);

  return {
    HOC: h,
    YapijProvider: h.connectProvider()(c.YapijProvider),
    SimpleCurator: h.connectCurator()(c.SimpleCurator),
    SimpleBeacon: h.connectBeacon()(c.SimpleBeacon),
    Settings: h.connectConfig()(c.Settings),
    Shell: h.connectCurator()(c.Shell),
    MessageList: h.connectCurator()(c.MessageList),
    Beacon: h.connectCurator()(c.Beacon),
    WorkspaceMonitor: h.connectWorkspaceMonitor()(c.WorkspaceMonitor),
    InstallYapij: c.InstallYapij,
    Editor: h.connectEditor()(c.Editor),
  };
}

export const raw = {
  YapijProvider: c.YapijProvider,
  SimpleCurator: c.SimpleCurator,
  SimpleBeacon: c.SimpleBeacon,
  PyType: c.PyType,
  Settings: c.Settings,
  Inputs: c.Inputs,
  Shell: c.Shell,
  MessageList: c.MessageList,
  Beacon: c.Beacon,
  WorkspaceMonitor: c.WorkspaceMonitor,
  PyRunning: c.PyRunning,
  WorkspaceDialogs: c.WorkspaceDialogs,
  InstallYapij: c.InstallYapij,
  Editor: c.Editor,
  HOC: HOC_,
};

export const redux = {
  topics: yapijTopics,
  action: yapijActions,
  reducers: yapijReducers,
};

const h_ = HOC_("YAPIJ", "YAPIJ");
export const HOC = h_;
export const YapijProvider = h_.connectProvider()(c.YapijProvider);
export const SimpleCurator = h_.connectCurator()(c.SimpleCurator);
export const SimpleBeacon = h_.connectBeacon()(c.SimpleBeacon);
export const Settings = h_.connectConfig()(c.Settings);
export const Shell = h_.connectCurator()(c.Shell);
export const MessageList = h_.connectCurator()(c.MessageList);
export const Beacon = h_.connectCurator()(c.Beacon);
export const WorkspaceMonitor = h_.connectWorkspaceMonitor()(
  c.WorkspaceMonitor
);
export const Editor = h_.connectEditor()(c.Editor);
export const InstallYapij = c.InstallYapij;
// export const FileBrowser = c.FileBrowser;
