export function setExtensionIcon(f) {
  switch (f.split(".").slice(-1)[0]) {
    case "py":
      return "fas fa-python";
      break;
    case "js":
      return "fas node-js";
      break;
    case "doc":
      return "fas fa-file-word";
      break;
    case "docx":
      return "fas fa-file-word";
      break;
    case "xlsx":
      return "fas fa-file-excel";
      break;
    case "xls":
      return "fas fa-file-excel";
      break;
    case "md":
      return "fas fa-markdown";
      break;
    case "csv":
      return "fas fa-file-csv";
      break;
    case "zip":
      return "fas fa-file-archive";
      break;
    case "gz":
      return "fas fa-file-archive";
      break;
    case "git":
      return "fab fa-git";
      break;
    case "html":
      return "fab fa-html";
      break;
    case "css":
      return "fab fa-css3";
      break;
    case "scss":
      return "fab fa-sass";
      break;
    default:
      return "fas fa-file";
      break;
  }
}
