// import React from "react";
// import PropTypes from "prop-types";

// const FS = require("electron").remote.require("fs");
// import Path from "path";

// // Had to modify a la:
// // https://github.com/michaelwooley/primereact
// import { AutoComplete } from "primereact/autocomplete";
// import { InputText } from "primereact/inputtext";
// import { SlideMenu } from "primereact/slidemenu";

// import { setExtensionIcon } from "./utils.js";

// export default class FileBrowser extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       path: "/",
//       lastDir: "/",
//       model: [],
//       history: [],
//       suggestions: [],
//     };

//     this.setFileOrPath = this.setFileOrPath.bind(this);
//     this.setPath = this.setPath.bind(this);
//     this.suggestPath = this.suggestPath.bind(this);
//   }

//   setFileOrPath(v) {
//     var h = this.state.history,
//       newPath = v || this.state.path;

//     h.push(newPath);
//     this.setPath(newPath);

//     FS.lstat(newPath, {}, (callback = (err, stats) => console.log(err, stats)));

//     //   (err, stats) => {
//     //   if (err) {
//     //     console.warn("here");
//     //     console.error(err);
//     //     return null;
//     //   }

//     //   if (stats.isDirectory()) {
//     //     this.setPath(newPath);
//     //   } else if (stats.isFile()) {
//     //     if (v) {
//     //       this.setState({ history: h, path: v });
//     //     } else {
//     //       this.setState({ history: h });
//     //     }
//     //   } else {
//     //     // @todo handle errors
//     //     console.error("@todo handle errors");
//     //     return null;
//     //   }
//     // });
//   }

//   setPath(newPath, history, path) {
//     FS.readdir(
//       newPath,
//       { encoding: "utf8", withFileTypes: true },
//       (err, files) => {
//         // @todo figure out how to handle invalid paths - growler?
//         if (err) {
//           throw err;
//         }
//         const fi = files;
//         console.log(fi);

//         let m = files.map((f, ii) => {
//           let it = { label: f.name };

//           // if (f.isDirectory()) {
//           //   it.icon = "fas fa-folder";
//           //   if (this.props.selectDirectory) {
//           //     it.disabled = false;
//           //     it.command = () =>
//           //       this.setFileOrPath(Path.resolve(newPath, f.name));
//           //   } else {
//           //     it.disabled = true;
//           //   }
//           //   return it;
//           // } else if (f.isFile()) {
//           //   // Get the extension
//           //   it.icon = setExtensionIcon(f.name);
//           //   if (this.props.selectFile) {
//           //     it.disabled = false;
//           //     it.command = () =>
//           //       this.setFileOrPath(Path.resolve(newPath, f.name));
//           //   } else {
//           //     it.disabled = true;
//           //   }
//           //   return it;
//           // } else if (f.isSymbolicLink()) {
//           //   it.disabled = this.props.selectDirectory || this.props.selectFile;
//           //   it.icon = "fas fa-external-link-square-alt";
//           // } else {
//           //   it.disabled = true;
//           //   it.icon = "fas fa-question-circle";
//           // }
//         });

//         if (path) {
//           this.setState({
//             history: history,
//             model: m,
//             path: path,
//             lastDir: newPath,
//           });
//         } else {
//           this.setState({ history: history, model: m, lastDir: path });
//         }
//       }
//     );
//   }

//   suggestPath(e) {
//     let q = e.query.toLowerCase();
//     let res = this.state.history
//       .filter(h => h.toLowerCase().startsWith(q))
//       .concat(
//         this.state.model.map(m => {
//           m.label.toLowerCase().startsWith(q)
//             ? Path.resolve(this.state.lastDir, m.label)
//             : null;
//         })
//       );
//     this.setState({ suggestions: res });
//   }

//   render() {
//     return (
//       <div className="file-browser">
//         <AutoComplete
//           value={this.state.path}
//           onChange={e => this.setState({ path: e.value })}
//           suggestions={this.state.suggestions}
//           completeMethod={this.suggestPath}
//           onKeyPress={e => {
//             if (e.key === "Enter") this.setFileOrPath();
//           }}
//           dropdown={true}
//         />
//       </div>
//     );
//   }
// }

// FileBrowser.propTypes = {
//   maxHistory: PropTypes.number.isRequired,
//   selectFile: PropTypes.bool.isRequired,
//   selectDirectory: PropTypes.bool.isRequired,
//   onSubmit: PropTypes.func.isRequired,
// };
// FileBrowser.defaultProps = {
//   maxHistory: 100,
//   selectFile: true,
//   selectDirectory: true,
//   onSubmit: p => console.log(p),
// };
