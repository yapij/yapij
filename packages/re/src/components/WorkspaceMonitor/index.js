import React from "react";
import PropTypes from "prop-types";

import { Card, Tab, Tabs, Button, NonIdealState } from "@blueprintjs/core";

import WorkspacePanel from "./WorkspacePanel.js";
import "./index.scss";

const NoPyProcess = ({ yapij }) => (
  <NonIdealState
    icon="error"
    title="No Python Kernel Running"
    action={
      <div className="nis-button" onClick={yapij.start}>
        Start a Kernel
      </div>
    }
  />
);

const NoWorkspace = ({ yapij }) => (
  <NonIdealState
    icon="briefcase"
    title="No Workspace Fetched"
    action={
      <div
        className="nis-button"
        onClick={() => yapij.exec("%workspace_info", true)}
      >
        Refresh the Workspace
      </div>
    }
  />
);

class Main extends React.Component {
  render() {
    const { yapij, info, style, ...otherProps } = this.props;

    return (
      <div>
        <Tabs animate={false}>
          <Tab
            title="Workspace"
            id="ws"
            panel={<WorkspacePanel style={style} info={info} yapij={yapij} />}
          />
          <Tab
            title="File Information"
            id="fi"
            panel={<div>To be done.</div>}
          />
          <Tabs.Expander />
          <Button
            icon="refresh"
            onClick={() => yapij.exec("%workspace_info", true)}
            disabled={Object.keys(info).length === 0}
            className="refresh"
          />
        </Tabs>
        <div className="footer">
          <span className="title">Last Updated:</span>{" "}
          {`${info.workspace.date.toLocaleTimeString()} (${info.workspace.date.toLocaleDateString()})`}
        </div>
      </div>
    );
  }
}

export default class WorkspaceMonitor extends React.Component {
  constructor(props) {
    super(props);

    this.style = {
      self: {
        maxHeight: `${props.maxHeight}px`,
        maxWidth: `${props.maxWidth}px`,
      },
      inner: {
        maxHeight: `${props.maxHeight - 90}px`,
        maxWidth: `${props.maxWidth - 50}px`,
      },
    };
  }

  render() {
    const {
      yapij,
      info,
      dispatch,
      maxWidth,
      maxHeight,
      ...otherProps
    } = this.props;

    var Inner;
    if (info.workspace) {
      Inner = <Main info={info} yapij={yapij} style={this.style.inner} />;
    } else if (Object.keys(info).length > 0) {
      Inner = <NoWorkspace yapij={yapij} />;
    } else {
      Inner = <NoPyProcess yapij={yapij} />;
    }

    return (
      <Card elevation={2} className="workspace-monitor" {...otherProps}>
        <div className="card-title">Workspace</div>
        {Inner}
      </Card>
    );
  }
}

WorkspaceMonitor.propTypes = {
  info: PropTypes.object.isRequired,
  yapij: PropTypes.object.isRequired,
  maxHeight: PropTypes.number.isRequired,
  maxWidth: PropTypes.number.isRequired,
};

WorkspaceMonitor.defaultProps = {
  maxHeight: 400,
  maxWidth: 400,
};
