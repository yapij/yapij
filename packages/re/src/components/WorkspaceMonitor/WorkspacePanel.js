import React from "react";
import PropTypes from "prop-types";

import {
  Card,
  Tab,
  Tabs,
  Button,
  NonIdealState,
  Icon,
  Tooltip,
} from "@blueprintjs/core";

const NoObjects = ({}) => (
  <NonIdealState
    title="No objects in workspace."
    description="Objects will be added once you issue python commands."
    icon="asterisk"
  />
);

class WorkspacePanelNonEmpty extends React.Component {
  render() {
    return (
      <div style={this.props.style} className="object-table-container">
        <table className="object-table bp3-html-table bp3-html-table-striped bp3-small bp3-interactive">
          <thead>
            <tr>
              <th>Object</th>
              <th>Type</th>
              <th>Size (bytes)</th>
              <th>
                <Tooltip content={"Deletes entire workspace"} minimal>
                  <div
                    className="reset"
                    onClick={() => this.props.yapij.exec("%reset", true)}
                  >
                    <div>Reset</div>
                    <div>Workspace</div>
                  </div>
                </Tooltip>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.props.info.workspace.workspace.env.map((k, ii) => (
              <tr key={`k${ii}`}>
                <td>{k.var}</td>
                <td>{k.type}</td>
                <td>{k.size}</td>
                <td>
                  <Tooltip
                    content={`Deletes object "${k.var}"`}
                    minimal
                    style={{ width: "100%" }}
                  >
                    <div className="reset">
                      <Icon
                        icon="cross"
                        onClick={() =>
                          this.props.yapij.exec(`del ${k.var}`, true)
                        }
                      />
                    </div>
                  </Tooltip>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default class WorkspacePanel extends React.Component {
  render() {
    if (this.props.info.workspace.workspace.env.length === 0) {
      return <NoObjects />;
    } else {
      return <WorkspacePanelNonEmpty {...this.props} />;
    }
  }
}

WorkspacePanel.propTypes = {
  info: PropTypes.object.isRequired,
  yapij: PropTypes.objectOf(
    PropTypes.oneOf([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
  style: PropTypes.objectOf(PropTypes.string).isRequired,
};
