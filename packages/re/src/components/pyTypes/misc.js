import React from "react";
import PropTypes from "prop-types";
import { Button } from "@blueprintjs/core";

import {
  SyntaxHighlighter,
  ReactJson,
  themes as syntaxThemes,
} from "../../common/syntaxHighlighting";

const Placeholder = ({ config, onClick, ...props }) => {
  return (
    <Button onClick={onClick} style={config} minimal className="hidden-button">
      <code>
        <em>[Hidden - Click to open.]</em>
      </code>
    </Button>
  );
};
export { Placeholder };
