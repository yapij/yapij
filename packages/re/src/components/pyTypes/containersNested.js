import React from "react";
import numeral from "numeral";
import { Button } from "@blueprintjs/core";

import * as containers from "./containers.js";

function AsReactJson({ type, v, config, style, ...props }) {
  return <ReactJson name={type} src={v} style={style} {...config} {...props} />;
}

export function ListNested({ v, config, ...props }) {
  return (
    <AsReactJson
      type={"list"}
      v={v}
      style={config.style}
      config={config.reactJsonView}
      {...props}
    />
  );
}


