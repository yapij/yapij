import React from "react";
import PropTypes from "prop-types";
import numeral from "numeral";
import { Button, Icon } from "@blueprintjs/core";

import {
  SyntaxHighlighter,
  ReactJson,
  syntaxThemes,
} from "../../common/syntaxHighlighting";

import "./containers.scss";

class AsSyntaxHighlighter extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      JSON.stringify(nextProps.style) !== JSON.stringify(this.props.style)
    );
  }

  render() {
    const { config, v, style, type, ...otherProps } = this.props;
    const { theme, ...otherPropsConfig } = config;

    return (
      <div className="container" {...otherProps}>
        <SyntaxHighlighter
          language="python3"
          style={theme}
          customStyle={style}
          {...otherPropsConfig}
        >
          {v}
        </SyntaxHighlighter>
      </div>
    );
  }
}

AsSyntaxHighlighter.propTypes = {
  type: PropTypes.string,
  v: PropTypes.string.isRequired,
  config: PropTypes.object.isRequired,
  style: PropTypes.object.isRequired,
};

class AsReactJson extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      JSON.stringify(nextProps.style) !== JSON.stringify(this.props.style)
    );
  }

  render() {
    const { config, v, style, type, ...otherProps } = this.props;
    return (
      <div className="container" {...otherProps}>
        <ReactJson name={null} src={v} style={style} {...config} />
      </div>
    );
  }
}
AsReactJson.propTypes = {
  type: PropTypes.string,
  v: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
  style: PropTypes.object.isRequired,
};

/**
 * All components falling under AsReactJson
 */

const PyList = ({ v, config, ...props }) => {
  return (
    <AsReactJson
      type={"list"}
      v={v}
      style={config.style}
      config={config.reactJsonView}
      {...props}
    />
  );
};

const PySet = ({ v, config, ...props }) => {
  return (
    <AsReactJson
      type={"set"}
      v={v}
      style={config.style}
      config={config.reactJsonView}
      {...props}
    />
  );
};

const PyTuple = ({ v, config, ...props }) => {
  return (
    <AsReactJson
      type={"tuple"}
      v={v}
      style={config.style}
      config={config.reactJsonView}
      {...props}
    />
  );
};

const PyDict = ({ v, config, ...props }) => {
  return (
    <AsReactJson
      type={"dict"}
      v={v}
      config={config.reactJsonView}
      {...props}
      style={config.style}
    />
  );
};

const PyNpArray = ({ v, config, ...props }) => {
  const collapsed = v.value.values[0].length > 10 || v.value.values > 10;
  return (
    <AsReactJson
      type={null}
      v={v}
      config={config.reactJsonView}
      style={config.style}
      collapsed={collapsed}
      {...props}
    />
  );
};

const PyStr = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"str"}
      v={`"${v.value}"`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyFloat = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"float"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyBool = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"bool"}
      v={v.value === true ? "True" : "False"}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyInt = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"int"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyComplex = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"complex"}
      v={`${v.value.r} + ${v.value.i} j`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyCallable = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"callable"}
      v={`${v.value.s}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyType = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"type"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyUnknownPyType = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"UnknownPyType"}
      v={v.value}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyUnhandledType = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"UnhandledType"}
      v={v.value}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyNotImplemented = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"NotImplemented"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyEllipsis = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"Ellipsis"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyNoneType = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"NoneType"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyRange = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"range"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyCode = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"code"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyMethod = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"method"}
      v={`${v.value.n}\n${v.value.s}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyDatetime = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"iter"}
      v={v.value.toLocaleString()}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyContextManager = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"ContextManager"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyFrozenSet = ({ v, config, ...props }) => {
  return (
    <AsReactJson
      type={"frozenset"}
      v={v.value}
      style={config.style}
      config={config.reactJsonView}
      {...props}
    />
  );
};

const PyMemoryView = ({ v, config, ...props }) => {
  const vv = { list: v.value.l, shape: v.value.s, nbytes: v.value.b };
  return (
    <AsReactJson
      type={"memoryview"}
      v={vv}
      style={config.style}
      config={config.reactJsonView}
      {...props}
    />
  );
};

const PyByteArray = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"bytearray"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyBytes = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"bytes"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

const PyIterator = ({ v, config, ...props }) => {
  return (
    <AsSyntaxHighlighter
      type={"iter"}
      v={`${v.value}`}
      config={config.reactSyntaxHighlighter}
      style={config.style}
      {...props}
    />
  );
};

class PyModule extends React.Component {
  constructor(props) {
    super(props);

    this.state = { expand: false };
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config)
    );
  }

  render() {
    const { v, config, ...props } = this.props;

    const expanded = (
      <div>
        <div>
          {v.value.doc.split("\n").map((k, ii) => (
            <div key={`k${ii}`}>{k}</div>
          ))}
        </div>
        <AsReactJson
          type={"dir"}
          v={v.value.dir}
          style={config.style}
          config={config.reactJsonView}
          {...props}
        />
      </div>
    );

    return (
      <div className={`container py-module ${config.theme}`} {...props}>
        <Button
          minimal
          onClick={() => this.setState({ expand: !this.state.expand })}
          style={config.style}
        >
          {`module: ${v.value.n}`}
        </Button>
        {this.state.expand ? expanded : null}
      </div>
    );
  }
}

PyModule.propTypes = {
  v: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
};

class PyFigure extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config)
    );
  }

  render() {
    const { v, config, ...otherProps } = this.props;
    return (
      <div
        className="container"
        dangerouslySetInnerHTML={{ __html: v.value }}
      />
    );
  }
}

PyFigure.propTypes = {
  v: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
};

class PyException extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config)
    );
  }
  render() {
    const { v, config, ...otherProps } = this.props;

    const v_ = v.value;
    return (
      <div className={`container exception ${config.theme}`} {...otherProps}>
        <div className="label title">{"".padStart(30, "-")}</div>
        <div>
          <span className="label title">{v_.type}</span> @ line {v_.lineno}
        </div>
        <div>
          <span className="label">Traceback</span> (Most recent call last):
          <SyntaxHighlighter
            language="bash"
            style={config.reactSyntaxHighlighter.theme}
            customStyle={{
              backgroundColor: "rgba(0,0,0,0)",
              overflow: "visible",
            }}
          >
            {v_.tb}
          </SyntaxHighlighter>
        </div>
        <div>
          <span className="label">{v_.type}</span>:{" "}
          {v_.args.map((aa, ii) => (
            <span key={`exception-arg-${0}`}>{aa}</span>
          ))}
        </div>
      </div>
    );
  }
}

PyException.propTypes = {
  v: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
};

class PyWarning extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config)
    );
  }
  render() {
    const { v, config, ...otherProps } = this.props;

    var c = config.theme === "dark" ? "#f8f8f2" : "#8e908c";
    const v_ = v.value;
    return (
      <div className={`container warning ${config.theme}`} {...otherProps}>
        <div className="label title">{"".padStart(30, "-")}</div>
        <div>
          <span className="label title">{v_.type}</span> @ line {v_.lineno}
        </div>
        {v_.msg ? <div>{v_.msg}</div> : null}
        {v_.source ? (
          <div>
            <span className="label">Source: </span>
            {` ${v_.source}`}
          </div>
        ) : null}
        {v_.file ? (
          <div>
            <span className="label">File: </span>
            {` ${v_.file}`}
          </div>
        ) : null}
      </div>
    );
  }
}

PyWarning.propTypes = {
  v: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
};

class PyPandasSeries extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config)
    );
  }
  render() {
    const { v, config, ...otherProps } = this.props;

    return (
      <table
        className={`bp3-html-table bp3-small bp3-html-table-striped bp3-interactive yapij-shell-table ${
          config.theme
        }`}
        style={config.style}
        {...otherProps}
      >
        <tbody>
          {v.value.d.map((vi, ii) => (
            <tr key={`v-${ii}`}>
              <th>{v.value.i.v[ii]}</th>
              <td>{vi}</td>
            </tr>
          ))}
        </tbody>
        <tfoot>
          <tr>
            <td>{`(Pandas.Series) Name: ${v.value.c} (${v.value.s})`}</td>
          </tr>
        </tfoot>
      </table>
    );
  }
}

PyPandasSeries.propTypes = {
  v: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
};

class PyPandasDataFrame extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config)
    );
  }
  render() {
    const { v, config, ...otherProps } = this.props;

    return (
      <table
        className={`bp3-html-table bp3-small bp3-html-table-striped bp3-interactive yapij-shell-table ${
          config.theme
        }`}
        style={config.style}
      >
        <thead>
          <tr>
            <th />
            {v.value.c.v.map((ci, ii) => (
              <th key={`c-${ii}`}>{ci}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {v.value.d.map((vi, ii) => (
            <tr key={`r-${ii}`}>
              <th>{v.value.i.v[ii]}</th>
              {vi.map((vij, jj) => (
                <td key={`vij-${jj}`}>{vij}</td>
              ))}
            </tr>
          ))}
        </tbody>
        <tfoot>
          <tr>
            <td>{`(Pandas.DataFrame) (${v.value.s})`}</td>
          </tr>
        </tfoot>
      </table>
    );
  }
}

PyPandasDataFrame.propTypes = {
  v: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
};

class PyDocstring extends React.Component {
  constructor(props) {
    super(props);

    this.state = { docstringExpanded: true, dirExpanded: false };

    this.docstring = (
      <div
        className="docstring inset"
        onClick={() =>
          this.setState({ docstringExpanded: !this.state.docstringExpanded })
        }
      >
        {this.props.v.value.docstring
          ? this.props.v.value.docstring
              .split("\n")
              .map((v, ii) => <div key={`k${ii}`}>{v}</div>)
          : "[None]"}
      </div>
    );

    this.dir = (
      <div
        className="dir inset"
        onClick={() => {
          this.setState({ dirExpanded: !this.state.dirExpanded });
        }}
      >
        {this.props.v.value.dir
          ? this.props.v.value.dir
              .split("\n")
              .map((v, ii) => <div key={`k${ii}`}>{v}</div>)
          : "[N/A]"}
      </div>
    );

    if (this.props.v.value.file) {
      this.file = (
        <div className="file">
          <span className="label">File:</span>
          <span>{` "${this.props.v.value.file}"`}</span>
        </div>
      );
    } else {
      this.file = null;
    }

    if (this.props.v.value.module) {
      this.module = (
        <div className="module">
          <span className="label">Module:</span>
          <span>{` ${this.props.v.value.module}`}</span>
        </div>
      );
    } else {
      this.module = null;
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      nextState.dirExpanded !== this.state.dirExpanded ||
      nextState.docstringExpanded !== this.state.docstringExpanded
    );
  }

  render() {
    const { v, config, ...otherProps } = this.props;
    const v_ = v.value;

    return (
      <div className={`container py-docstring ${config.theme}`}>
        <div className="header">
          <span className="label title">Init Docstring:</span>
          {"  "}
          <span className="elan">{v_.signature ? v_.signature : v_.name}</span>
        </div>
        <div className="type">
          <span className="label title">Type:</span>{" "}
          <span className="elan">{v_.type || "[Not Reported]"}</span>
        </div>
        <div className="docstring-container">
          <div
            className="button"
            onClick={() =>
              this.setState({
                docstringExpanded: !this.state.docstringExpanded,
              })
            }
          >
            <span className="label">
              {" "}
              <Icon
                icon={
                  this.state.docstringExpanded ? "caret-down" : "caret-right"
                }
              />{" "}
              Docstring:
            </span>
          </div>
          {this.state.docstringExpanded ? this.docstring : null}
        </div>
        <div className="dir-container">
          <div
            className="button"
            onClick={() =>
              this.setState({
                dirExpanded: !this.state.dirExpanded,
              })
            }
          >
            <span className="label">
              {" "}
              <Icon
                icon={this.state.dirExpanded ? "caret-down" : "caret-right"}
              />{" "}
              Directory:
            </span>
          </div>
          {this.state.dirExpanded ? this.dir : null}
        </div>
        {this.module}
        {this.file}
      </div>
    );
  }
}

const Containers = {
  PyList,
  PySet,
  PyTuple,
  PyDict,
  PyStr,
  PyFloat,
  PyBool,
  PyInt,
  PyComplex,
  PyCallable,
  PyType,
  PyUnknownPyType,
  PyUnhandledType,
  PyException,
  PyWarning,
  PyPandasSeries,
  PyPandasDataFrame,
  PyFigure,
  PyNotImplemented,
  PyEllipsis,
  PyNoneType,
  PyRange,
  PyCode,
  PyMethod,
  PyModule,
  PyContextManager,
  PyFrozenSet,
  PyMemoryView,
  PyByteArray,
  PyBytes,
  PyIterator,
  PyDatetime,
  PyNpArray,
  PyDocstring,
};

export default Containers;
