import React from "react";
import PropTypes from "prop-types";
import { Tooltip, Button } from "@blueprintjs/core";

import containers from "./containers.js";
import { Placeholder } from "./misc.js";
import initialState from "../../redux/initialState.js";

import "./index.scss";

export default class PyType extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      nextProps.collapse !== this.props.collapse
    );
  }

  render() {
    const { config, output } = this.props;
    var Inner = containers[output.pyType];

    if (!Inner) {
      Inner = ({ v, theme, ...props }) => (
        <h5>
          <code>{`[Nothing found for type ${output.pyType}]`}</code>
        </h5>
      );
    }

    const element = this.props.collapse ? (
      <Placeholder config={config.style} onClick={this.props.toggleCollapse} />
    ) : (
      <Inner v={output} config={config} />
    );

    return (
      <div className="py-type" style={config.style}>
        <div className="py-type-container">{element}</div>
      </div>
    );
  }
}

PyType.propTypes = {
  output: PropTypes.object.isRequired,
  config: PropTypes.object,
  collapse: PropTypes.bool,
  toggleCollapse: PropTypes.func,
};

PyType.defaultProps = {
  config: initialState.config,
  collapse: false,
  toggleCollapse: () => null,
};
