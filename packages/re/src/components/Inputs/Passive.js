import React from "react";
import PropTypes from "prop-types";

import {
  SyntaxHighlighter,
  ReactJson,
  syntaxThemes
} from "../../common/syntaxHighlighting";

export default class Passive extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { theme, ...otherProps } = this.props.config.reactSyntaxHighlighter;

    return (
      <SyntaxHighlighter
        language="python3"
        style={theme}
        customStyle={this.props.config.style}
        {...otherProps}
      >
        {this.props.input}
      </SyntaxHighlighter>
    );
  }
}

Passive.propTypes = {
  config: PropTypes.object.isRequired,
  input: PropTypes.string.isRequired
};
Passive.defaultProps = {};
