import React from "react";
import PropTypes from "prop-types";

import { ResizeSensor } from "@blueprintjs/core";
import AceEditor from "react-ace";
import shortid from "shortid";

import initialState from "../../redux/initialState.js";

export default class Active extends React.Component {
  constructor(props) {
    super(props);

    this.historyUp = this.historyUp.bind(this);
    this.historyDown = this.historyDown.bind(this);
    this.onSubmitInner = this.onSubmitInner.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.focusSelf = this.focusSelf.bind(this);

    this.state = {
      history: [],
      future: [],
      nlines: 1,
      width: `500px`,
      focus: false,
    };
    this.uid = shortid.generate();
    this.commands = [
      {
        name: "getHistory",
        bindKey: { win: "PageUp", mac: "PageUp" },
        exec: this.historyUp,
      },
      {
        name: "getFuture",
        bindKey: { win: "PageDown", mac: "PageDown" },
        exec: this.historyDown,
      },
    ];
  }

  componentDidMount() {
    if (this.props.mainRef) {
      this.props.mainRef.addEventListener("mousedown", this.focusSelf);
    }
  }

  componentWillUnmount() {
    if (this.props.mainRef) {
      this.props.mainRef.removeEventListener("mousedown", this.focusSelf);
    }
  }

  focusSelf(e) {
    this.setState({ focus: true });
  }

  /**
   * When press PageUp will go to previously-input command
   */
  historyUp(e, ii) {
    const hl = this.state.history.length;

    if (hl > 0) {
      let f = this.state.future.slice(-99),
        v = this.state.history[hl - 1].split("\n");
      if (this.props.value !== "") {
        f.push(this.props.value);
      }

      this.setState(
        {
          history:
            this.state.history.slice(-this.props.historySize + 1, -1) || [],
          future: f,
        },
        () => {
          e.gotoLine(v.length, v.slice(-1)[0].length);
          e.selection.anchor.setPosition(v.length, v.slice(-1)[0].length);
          this.props.onChange(v.join("\n"));
        }
      );
    }
  }

  /**
   * When press PageDown will go to "future" command. If there is no future will go to blank.
   */
  historyDown(e, ii) {
    let fl = this.state.future.length;

    if (fl > 0) {
      let h = this.state.history.slice(-this.props.historySize + 1),
        v = this.props.value.split("\n");
      if (this.props.value !== "") {
        h.push(this.props.value);
      }

      this.setState(
        {
          history: h,
          future: this.state.future.slice(0, -1) || [],
        },
        () => {
          e.gotoLine(v.length, v.slice(-1)[0].length);
          e.selection.anchor.setPosition(v.length, v.slice(-1)[0].length);
        }
      );
      this.props.onChange(this.state.future[fl - 1]);
    } else {
      let h = this.state.history.slice(-this.props.historySize + 1),
        v = this.props.value.split("\n");
      h.push(this.props.value);
      this.setState({
        history: h,
      });
      this.props.onChange("");
    }
  }

  /**
   * When submit is hit, will carry out callback, push value to history, and reset value.
   */
  onSubmitInner(e) {
    let v = this.props.value,
      h = this.state.history;
    h.push(v);
    this.setState({ history: h });
    this.props.onSubmit(v);
    this.props.onChange("");
  }

  /**
   * When keypress occurs, will update the value in state.
   *
   * If the value is a new line (i.e. `["", ""]`) will update the number of lines that exist, which feeds into the height of the element.
   */
  updateValue(v, e) {
    if (e.lines.length === 2 && e.lines.every(el => el === "")) {
      let n = this.state.nlines;
      if (e.action === "insert") {
        n += 1;
      } else if (e.action === "remove") {
        n -= 1;
      }
      this.setState({ nlines: n });
      this.props.onChange(v);
    } else {
      this.props.onChange(v);
    }
  }

  render() {
    const commands = this.commands.concat(
      this.props.execCommands.map((ec, ii) => {
        return { name: `onSubmit${ii}`, bindKey: ec, exec: this.onSubmitInner };
      })
    );

    return (
      <ResizeSensor
        onResize={e => {
          this.setState({ width: `${e[0].contentRect.width}px` });
        }}
      >
        <div className="input active ace-editor">
          <AceEditor
            mode="python"
            name={this.uid}
            editorProps={{ $blockScrolling: Infinity }}
            showGutter={false}
            showPrintMargin={false}
            onChange={this.updateValue}
            value={this.props.value}
            commands={commands}
            minLines={1}
            height={`${this.props.minHeight +
              this.props.config.reactAce.fontSize * this.state.nlines}px`}
            width={this.state.width}
            style={{ overflow: "visible" }}
            onBlur={e => this.setState({ focus: !this.state.focus })}
            focus={this.state.focus}
            {...this.props.config.reactAce}
          />
        </div>
      </ResizeSensor>
    );
  }
}

Active.propTypes = {
  value: PropTypes.string.isRequired,
  config: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  execCommands: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string))
    .isRequired,
  historySize: PropTypes.number,
  minHeight: PropTypes.number,
  mainRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
};

Active.defaultProps = {
  minHeight: 100,
  config: initialState.config,
  historySize: 100,
  execCommands: [
    { win: "f5", mac: "f5" },
    { win: "shift-enter", mac: "shift-enter" },
  ],
};
