import React from "react";
import PropTypes from "prop-types";

const { dialog } = require("electron").remote;
import { homedir } from "os";

import { Accordion, AccordionTab } from "primereact/accordion";
import { Card } from "primereact/card";
import { Button } from "primereact/button";
import { Fieldset } from "primereact/fieldset";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/message";
import { Messages } from "primereact/messages";
import { InputSwitch } from "primereact/inputswitch";
import { OverlayPanel } from "primereact/overlaypanel";
import { SplitButton } from "primereact/splitbutton";
import { Checkbox } from "primereact/checkbox";
import { ProgressSpinner } from "primereact/progressspinner";
import { TieredMenu } from "primereact/tieredmenu";
import { Dialog } from "primereact/dialog";

import Status from "../Utils/Status.js";

const Header = ({ toggleHelp }) => (
  <div className="header">
    <h3>
      Set up Python Virtual Environments
      <a className="help-button" onClick={toggleHelp}>
        <span className="fas fa-question-circle" />
      </a>
    </h3>
  </div>
);

const HelpText = ({ style, toggleHelp }) => (
  <Status
    intent="help"
    title="What does this step do?"
    raised
    onClick={toggleHelp}
    style={style}
    className="help-text"
  >
    Help Text TBA.
  </Status>
);

class NonEmpty extends React.Component {
  constructor(props) {
    super(props);

    this.makeActionsMenu = this.makeActionsMenu.bind(this);
  }

  makeActionsMenu(v) {
    return [
      {
        label: "Rename",
        icon: "fas fa-strikethrough",
        command: () => this.props.onRename(v.name),
      },
      {
        label: "Install Yapij",
        icon: "fas fa-box",
        command: () => {
          this.props.setPanelProps("status", {
            intent: "default",
            title: "Yapij Download",
            msg: "Starting...",
          });
          this.props.Installer.YapijPy.download(v.name)
            .then(v =>
              this.props.setPanelProps("status", {
                intent: "success",
                title: "Yapij Download",
                msg: "Complete",
              })
            )
            .catch(err =>
              this.props.setPanelProps("status", {
                intent: "error",
                title: "Yapij Download Failed",
                msg: `Error Message: ${err}`,
              })
            );
        },
      },
      {
        label: "Delete Reference & Directory",
        icon: "fas fa-trash-alt",
        command: () => {
          this.props.Installer.Venvs.remove(v.name, true);
          let vv = this.props.venvs.filter(vvi => vvi.name !== v.name);
          this.props.setPanelProps("venvDirs", vv);
        },
      },
      {
        label: "Delete Reference Only",
        icon: "far fa-trash-alt",
        command: () => {
          this.props.Installer.Venvs.remove(v.name, false);
          let vv = this.props.venvs.filter(vvi => vvi.name !== v.name);
          this.props.setPanelProps("venvDirs", vv);
        },
      },
    ];
  }

  render() {
    const { venvs, setPanelProps, Installer, ...otherProps } = this.props;

    return (
      <table className="non-empty-table bp3-html-table bp3-html-table-bordered bp3-html-table-condensed bp3-html-table-striped bp3-interactive">
        <thead>
          <tr>
            <th>Name</th>
            <th>Path</th>
            <th>Default</th>
            <th>
              Yapij
              <br />
              Installed
            </th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {venvs.map((v, ii) => (
            <tr key={`tr-${ii}`}>
              <td className="name">{v.name}</td>
              <td className="path">{v.path}</td>
              <td className="default">
                <Checkbox
                  onChange={e => {
                    let venv_ = venvs.map(vv => {
                      if (vv.name === v.name) {
                        vv.isDefault = true;
                      } else if (vv.isDefault) {
                        vv.isDefault = false;
                      }
                      return vv;
                    });
                    setPanelProps("venvDirs", venv_);
                    Installer.Venvs.setDefault(v.name);
                  }}
                  checked={v.isDefault}
                />
              </td>
              <td
                className="installed"
                onClick={() => Installer.Venvs.setDefault(v.name)}
              >
                {v.yapijInstalled ? (
                  <span className="fas fa-check" />
                ) : (
                  <span className="fas fa-times" />
                )}
              </td>
              <td className="actions">
                <SplitButton
                  icon="fas fa-ellipsis-h"
                  model={this.makeActionsMenu(v)}
                  className="p-button-secondary"
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

NonEmpty.propTypes = {
  Installer: PropTypes.object.isRequired,
  setPanelProps: PropTypes.func.isRequired,
  venvs: PropTypes.array.isRequired,
  onRename: PropTypes.func.isRequired,
};

const EmptyVenvs = ({}) => (
  <div className="empty-venvs">
    No virtual environments are currently set. Click on one of the options above
    to add one.
  </div>
);

const AddVenvsButton = ({ toggleAddDialog }) => (
  <div className="add-venvs-button">
    <Button
      icon="fas fa-plus"
      onClick={toggleAddDialog}
      className="p-button-secondary"
    />
  </div>
);

const AddVenvTab = ({
  pathName,
  path,
  onSubmit,
  setPanelProps,
  getFileDirectory,
}) => (
  <div>
    <div className="floater">
      <span className="p-float-label">
        <InputText
          value={path.name}
          onChange={e => setPanelProps([pathName, "name"], e.target.value)}
          id="name-loc"
        />
        <label htmlFor="name-loc">Name</label>
      </span>
      <div className="form-helper-text">
        A common name used to refer to the environment.
      </div>
    </div>
    <div>
      <div className="p-inputgroup floater">
        <span className="p-float-label">
          <InputText
            value={path.path}
            onChange={e => setPanelProps([pathName, "path"], e.target.value)}
            id="exe-loc"
          />
          <label htmlFor="exe-loc">Virtual Environment Path</label>
        </span>
        <Button
          icon="fas fa-folder-open"
          label="browse"
          onClick={() =>
            getFileDirectory(
              "Python Path",
              ["openDirectory"],
              [pathName, "path"]
            )
          }
        />
      </div>
      <div className="form-helper-text">
        Should lead to the directory created by the virtual environment command.
        This directory should <code>Lib</code>, <code>Include</code>, and{" "}
        <code>Scripts</code> subdirectories as well as a <code>pyvenv.cfg</code>{" "}
        file.
      </div>
    </div>
    <div className="floater">
      <Checkbox
        inputId="cb1"
        onChange={e => setPanelProps([pathName, "isDefault"], !path.isDefault)}
        checked={path.isDefault}
      />
      <label htmlFor="cb1" className="p-checkbox-label">
        Make this the default virtual environment?
      </label>
      <div className="form-helper-text">
        The default virtual environment will be used to run any python code.
      </div>
    </div>
    <div className="floater">
      <Checkbox
        inputId="cb2"
        onChange={e =>
          setPanelProps([pathName, "yapijInstalled"], !path.yapijInstalled)
        }
        checked={path.yapijInstalled}
      />
      <label htmlFor="cb2" className="p-checkbox-label">
        Install yapij python package?
      </label>
      <div className="form-helper-text">
        Required in order to use this environment.
      </div>
    </div>
    <Button
      icon="fas fa-plus"
      label="Add"
      onClick={onSubmit}
      disabled={
        path.name === "" || !path.path || path.path === "" || !path.name
      }
    />
  </div>
);

const AddVenvs = ({
  Installer,
  addPath,
  existingPath,
  setPanelProps,
  toggleAddDialog,
  getFileDirectory,
  style,
  venvDirs,
}) => (
  <Status
    intent="default"
    title="Add a virtual environment."
    raised
    style={style}
    className="help-text"
    icon="fas fa-plus"
    style={style}
  >
    <Accordion>
      <AccordionTab header="Add pre-existing">
        <AddVenvTab
          path={existingPath}
          pathName="existingPath"
          setPanelProps={setPanelProps}
          getFileDirectory={getFileDirectory}
          onSubmit={() => {
            setPanelProps("status", {
              intent: "default",
              title: "Adding Virtual Environment",
              msg: "1. Adding Environment",
              icon: "fas fa-hourglass-start",
            });
            Installer.Venvs.add(
              existingPath.name,
              existingPath.path,
              existingPath.isDefault
            )
              .then(v => {
                if (!v) {
                  throw new Error("Unexpected condition encountered.");
                }
                if (existingPath.yapijInstalled) {
                  setPanelProps("status", {
                    intent: "default",
                    title: "Adding Virtual Environment",
                    msg: "1. Adding Environment [Done] | 2.Installing Yapij",
                    icon: "fas fa-hourglass-start",
                  });
                  Installer.YapijPy.download("NULL", existingPath).then(() => {
                    setPanelProps("venvDirs", venvDirs.concat([existingPath]));
                    setPanelProps("status", {
                      intent: "success",
                      title: "Virtual Environment Added",
                      msg: `You can always modify ${
                        existingPath.name
                      } in the table above.`,
                    });
                  });
                } else {
                  setPanelProps("venvDirs", venvDirs.concat([existingPath]));
                  setPanelProps("status", {
                    intent: "success",
                    title: "Virtual Environment Added",
                    msg: `You can always modify ${
                      existingPath.name
                    } in the table above. You will need to install the yapij python package in order to use this environment.`,
                  });
                }
              })
              .catch(e =>
                setPanelProps("status", {
                  intent: "error",
                  title: "Error: Virtual Environment Addition",
                  msg: `Received error:\n ${e}`,
                })
              );
          }}
        />
      </AccordionTab>
      <AccordionTab header="Create new">
        <AddVenvTab
          path={addPath}
          pathName="addPath"
          setPanelProps={setPanelProps}
          getFileDirectory={getFileDirectory}
          onSubmit={() => {
            setPanelProps("status", {
              intent: "default",
              title: "Creating Virtual Environment",
              msg: "1. Creating Environment (This may take a moment.)",
              icon: "fas fa-hourglass-start",
            });
            Installer.Venvs.create(
              addPath.name,
              addPath.path,
              addPath.isDefault
            )
              .then(v => {
                if (!v) {
                  throw new Error("Unexpected condition encountered.");
                }
                if (addPath.yapijInstalled) {
                  setPanelProps("status", {
                    intent: "default",
                    title: "Creating Virtual Environment",
                    msg: "1. Creating Environment [Done] | 2.Installing Yapij",
                    icon: "fas fa-hourglass-start",
                  });
                  Installer.YapijPy.download("2$V!YG1d5YHo", addPath).then(
                    () => {
                      setPanelProps("venvDirs", venvDirs.concat([addPath]));
                      setPanelProps("status", {
                        intent: "success",
                        title: "Virtual Environment Created",
                        msg: `You can always modify ${
                          addPath.name
                        } in the table above.`,
                      });
                    }
                  );
                } else {
                  setPanelProps("venvDirs", venvDirs.concat([addPath]));
                  setPanelProps("status", {
                    intent: "success",
                    title: "Virtual Environment Created",
                    msg: `You can always modify ${
                      addPath.name
                    } in the table above. You will need to install the yapij python package in order to use this environment.`,
                  });
                }
              })
              .catch(e =>
                setPanelProps("status", {
                  intent: "error",
                  title: "Error: Virtual Environment Creation",
                  msg: `Received error:\n ${e}`,
                })
              );
          }}
        />
      </AccordionTab>
    </Accordion>
    <hr />
    <Button
      icon="fas fa-times"
      label="Cancel"
      className="p-button-raised p-button-danger"
      onClick={toggleAddDialog}
    />
  </Status>
);

const RenameDialog = ({
  isOpen,
  onHide,
  Installer,
  setPanelProps,
  renameName,
  venv,
  name,
}) => (
  <Dialog
    header={`Rename ${name}`}
    footer={
      <div>
        <Button
          label="Cancel"
          icon="fas fa-times"
          onClick={() => {
            setPanelProps("renameName", "");
            onHide();
          }}
        />
        <Button
          label="Rename"
          icon="fas fa-check"
          onClick={() => {
            Installer.Venvs.rename(name, renameName);
            let venv_ = venv.map(v => {
              if (v.name === name) {
                v.name = renameName;
              }
              return v;
            });
            setPanelProps("venvDirs", venv_);
            setPanelProps("renameName", "");
            onHide();
          }}
        />
      </div>
    }
    visible={isOpen}
    width="350px"
    modal={false}
    onHide={onHide}
    baseZIndex={99}
  >
    <span className="p-float-label">
      <InputText
        value={renameName}
        onChange={e => setPanelProps("renameName", e.target.value)}
        id="name-loc"
      />
      <label htmlFor="name-loc">New Name</label>
    </span>
  </Dialog>
);

export default class Venv extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showHelp: false,
      showAdd: false,
      showRenameDialog: false,
      renameDialogName: "",
    };

    this.getFileDirectory = this.getFileDirectory.bind(this);
  }

  getFileDirectory(title, properties, key) {
    dialog.showOpenDialog(
      null,
      {
        title: title,
        defaultPath: homedir(),
        buttonLabel: "Ok",
        properties: properties,
      },
      (filepaths, bookmarks) => {
        this.props.setPanelProps(key, filepaths[0]);
      }
    );
  }

  render() {
    const {
      Installer,
      status,
      setPanelProps,
      addPath,
      existingPath,
      venvDirs,
      renameName,
      ...otherProps
    } = this.props;

    const helpStyle = {
      display: this.state.showHelp ? "grid" : "none",
    };
    const addVenvsStyle = { display: this.state.showAdd ? "grid" : "none" };

    return (
      <Card
        header={
          <Header
            toggleHelp={e => this.setState({ showHelp: !this.state.showHelp })}
          />
        }
        className="panel venv"
      >
        <HelpText
          toggleHelp={() => this.setState({ showHelp: !this.state.showHelp })}
          style={helpStyle}
        />
        <AddVenvsButton
          toggleAddDialog={() =>
            this.setState({ showAdd: !this.state.showAdd })
          }
        />
        <AddVenvs
          Installer={Installer}
          style={addVenvsStyle}
          toggleAddDialog={() =>
            this.setState({ showAdd: !this.state.showAdd })
          }
          addPath={addPath}
          existingPath={existingPath}
          getFileDirectory={this.getFileDirectory}
          setPanelProps={setPanelProps}
          venvDirs={venvDirs}
        />
        <HelpText
          toggleHelp={() => this.setState({ showHelp: !this.state.showHelp })}
          style={helpStyle}
        />
        {venvDirs.length > 0 ? (
          <NonEmpty
            Installer={Installer}
            venvs={venvDirs}
            setPanelProps={setPanelProps}
            onRename={nm =>
              this.setState({ showRenameDialog: true, renameDialogName: nm })
            }
          />
        ) : (
          <EmptyVenvs />
        )}

        <div className="panel-status">
          <Status {...status} />
        </div>
        <RenameDialog
          renameName={renameName}
          venv={venvDirs}
          onHide={() =>
            this.setState({ showRenameDialog: false, renameDialogName: null })
          }
          setPanelProps={setPanelProps}
          isOpen={this.state.showRenameDialog}
          name={this.state.renameDialogName}
          Installer={Installer}
        />
      </Card>
    );
  }
}

Venv.propTypes = {
  Installer: PropTypes.object.isRequired,
  status: PropTypes.object.isRequired,
  setPanelProps: PropTypes.func.isRequired,
  addPath: PropTypes.object.isRequired,
  existingPath: PropTypes.object.isRequired,
  venvDirs: PropTypes.array.isRequired,
  renameName: PropTypes.string.isRequired,
};

Venv.defaultProps = {};
