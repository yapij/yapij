import React from "react";
import PropTypes from "prop-types";

const { dialog } = require("electron").remote;
import { homedir } from "os";

import { Accordion, AccordionTab } from "primereact/accordion";
import { Card } from "primereact/card";
import { Button } from "primereact/button";
import { Fieldset } from "primereact/fieldset";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/message";
import { Messages } from "primereact/messages";
import { InputSwitch } from "primereact/inputswitch";
import { OverlayPanel } from "primereact/overlaypanel";

import Status from "../Utils/Status.js";

const PythonOptionPath = ({
  name,
  label,
  advancedOptions,
  setPanelProps,
  ...props
}) => (
  <div className="form-field path" {...props}>
    <br />
    <div className="p-inputgroup">
      <span className="p-float-label">
        <InputText
          value={advancedOptions[name]}
          onChange={e =>
            setPanelProps(["advancedOptions", name], e.target.value)
          }
          id="install-loc"
        />
        <label htmlFor="install-loc">{label}</label>
      </span>
      <Button
        icon="fas fa-folder-open"
        label="Browse"
        onClick={() => {
          dialog.showOpenDialog(
            null,
            {
              title: label,
              defaultPath: homedir(),
              buttonLabel: "Ok",
              properties: ["openDirectory"],
            },
            (filepaths, bookmarks) => {
              setPanelProps(["advancedOptions", name], filepaths[0]);
            }
          );
        }}
      />
    </div>
  </div>
);

const PythonOptionSwitch = ({
  name,
  label,
  advancedOptions,
  setPanelProps,
  ...props
}) => (
  <div className="form-field switch" {...props}>
    <InputSwitch
      checked={advancedOptions[name]}
      onChange={e => setPanelProps(["advancedOptions", name], 1 * e.value)}
      id={name}
    />
    <label htmlFor={name}>{label}</label>
  </div>
);

const advancedOptionsSwitch_ = [
  { name: "InstallAllUsers", label: "Perform a system-wide installation." },
  {
    name: "AssociatedFiles",
    label: "Create file associations if the launcher is also installed.",
  },
  { name: "CompileAll", label: "Compile all .py files to .pyc." },
  {
    name: "PrependPath",
    label: "Add install and Scripts directories to PATH and .PY to PATHEXT",
  },
  {
    name: "Shortcuts",
    label:
      "Create shortcuts for the interpreter, documentation and IDLE if installed.",
  },
  { name: "Include_doc", label: "Install Python manual" },
  { name: "Include_debug", label: "Install debug binaries" },
  { name: "Include_dev", label: "Install developer headers and libraries" },
  { name: "Include_exe", label: "Install python.exe and related files" },
  { name: "Include_launcher", label: "Install Python Launcher for Windows." },
  {
    name: "InstallLauncherAllUsers",
    label: "Installs Python Launcher for Windows for all users.",
  },
  {
    name: "Include_lib",
    label: "Install standard library and extension modules",
  },
  { name: "Include_pip", label: "Install bundled pip and setuptools" },
  { name: "Include_tcltk", label: "Install Tcl/Tk support and IDLE" },
  { name: "Include_test", label: "Install standard library test suite" },
  { name: "Include_tools", label: "Install utility scripts" },
  {
    name: "LauncherOnly",
    label: "Only installs the launcher. This will override most other options.",
  },
  { name: "SimpleInstall", label: "Disable most install UI" },
];

const advancedOptionsPath_ = [
  { name: "DefaultAllUsersTargetDir", label: "The installation directory" },
  {
    name: "DefaultJustForMeTargetDir",
    label: "The default installation directory for all-user installs",
  },
  {
    name: "DefaultCustomTargetDir",
    label: "The default install directory for just-for-me installs",
  },
];

const NoPython = ({
  Installer,
  installPath,
  advancedOptions,
  setPanelProps,
  getFileDirectory,
}) => (
  <div className="no-python">
    That's fine! We'll download it now.
    <div>
      Where would you like to install Python?
      <br />
      <br />
      <div className="p-inputgroup">
        <span className="p-float-label">
          <InputText
            value={installPath}
            onChange={e => setPanelProps("installPath", e.target.value)}
            id="install-loc"
          />
          <label htmlFor="install-loc">Installation Directory</label>
        </span>
        <Button
          icon="fas fa-folder-open"
          label="browse"
          onClick={() =>
            getFileDirectory("Python Installation Directory", ["openDirectory"])
          }
        />
      </div>
      <br />
      <Button
        label="Install"
        icon=""
        disabled={installPath === "" || !installPath}
        onClick={() =>
          Installer.PythonVersion.install(installPath, advancedOptions, true)
            .then(() =>
              setPanelProps("status", {
                title: "Installation Complete",
                msg: `Move to the next step.`,
                intent: "success",
              })
            )
            .catch(e =>
              setPanelProps("status", {
                title: "Installation Failed",
                msg: `Received error message:\n${e}`,
                intent: "error",
              })
            )
        }
      />
    </div>
    <br />
    <Fieldset legend="Advanced Options" toggleable={true} collapsed={true}>
      For more information about each of these variables see the{" "}
      <a
        href="https://docs.python.org/3/using/windows.html#installing-without-ui"
        target="_blank"
      >
        Python documention
      </a>
      .<br />
      <div className="advanced-options-controls">
        {advancedOptionsSwitch_.map((k, ii) => (
          <PythonOptionSwitch
            name={k.name}
            label={k.label}
            advancedOptions={advancedOptions}
            setPanelProps={setPanelProps}
            key={`pos-${ii}`}
          />
        ))}
        {advancedOptionsPath_.map((k, ii) => (
          <PythonOptionPath
            name={k.name}
            label={k.label}
            advancedOptions={advancedOptions}
            setPanelProps={setPanelProps}
            key={`pos-${ii}`}
          />
        ))}
      </div>
    </Fieldset>
  </div>
);

const HavePython = ({
  Installer,
  installPath,
  advancedOptions,
  setPanelProps,
  getFileDirectory,
}) => (
  <div className="have-python">
    <p>
      If you already have python version &#8805; 3.5 then there is no need to
      install python again. However, you do need to indicate where python
      (either the executable or program) installed on your computer.
    </p>
    <br />
    <div className="p-inputgroup">
      <span className="p-float-label">
        <InputText
          value={installPath}
          onChange={e => setPanelProps("installPath", e.target.value)}
          id="exe-loc"
        />
        <label htmlFor="exe-loc">Python Path</label>
      </span>
      <Button
        icon="fas fa-folder-open"
        label="browse"
        onClick={() => getFileDirectory("Python Path", ["openFile"])}
      />
    </div>
    <br />
    <Button
      label="Validate Python"
      icon=""
      disabled={installPath === "" || !installPath}
      onClick={() =>
        Installer.PythonVersion.search(installPath, true)
          .then(s => {
            if (s.status === 1) {
              setPanelProps("status", {
                title: "Validation Successful",
                msg: "Move to the next step.",
                intent: "success",
              });
            } else if (s.status === -1) {
              setPanelProps("status", {
                title: "Validation Failed",
                msg: `Python version must be greater than 3.5. Found version ${s.p.v.join(
                  "."
                )}`,
                intent: "error",
              });
            } else if (s.status === -2) {
              setPanelProps("status", {
                title: `Validation failed`,
                msg: `Python path invalid.`,
                intent: "error",
              });
            } else {
              setPanelProps("status", {
                title: `Validation failed`,
                msg: `Uknown validation code received.`,
                intent: "error",
              });
            }
          })
          .catch(e =>
            setPanelProps("status", {
              title: `Validation failed`,
              msg: `Received error message:\n${e}.`,
              intent: "error",
            })
          )
      }
    />
  </div>
);

const Header = ({ toggleHelp }) => (
  <div className="header">
    <h3>
      Locate or Install Python
      <a className="help-button" onClick={toggleHelp}>
        <span className="fas fa-question-circle" />
      </a>
    </h3>
  </div>
);

const HelpText = ({ style, toggleHelp }) => (
  <Status
    intent="help"
    title="What does this step do?"
    raised
    onClick={toggleHelp}
    style={style}
    className="help-text"
  >
    <p>
      In order for <code>yapij</code> to work, it needs to be able to run and
      access an actual python process. In this step you will either install
      python or point <code>yapij</code> in the right direction.
    </p>
    <h4>What does installing python do?</h4>
    <p>
      <code>@yapij/js</code> comes pre-packaged with a{" "}
      <a
        href="https://www.python.org/downloads/release/python-371/"
        target="_blank"
      >
        Windows x86-64 web-based installer
      </a>
      for python version 3.7.1 (released 2018-10-20). This wizard provides a
      simple interface for choosing install settings.
    </p>
    <h5>In what directory should I install python?</h5>
    <p>
      A common place for Windows users to install python is{" "}
      <code>
        C:/Users/[YOUR USERNAME]/AppData/Local/Programs/Python/Python37-32
      </code>
      .
    </p>
    <h5>I don't want to use this service to download python. Is that okay?</h5>
    <p>
      Sure! Download python from either{" "}
      <code>
        <a href="https://www.python.org/" target="_blank">
          python.org
        </a>
      </code>{" "}
      or from a python packager like{" "}
      <a href="https://www.anaconda.com/" target="_blank">
        Anaconda
      </a>
      . Make sure that you're downloading and installing a python version ≥ 3.5.
      Once your installation is complete you can finish the configuration by
      choosing the option "I have python version ≥ 3.5".
    </p>
    <h4>I have python ≥ 3.5. Now what?</h4>
    <p>
      You need to point <code>yapij</code> to the place where your python
      program or executable is installed. The wizard will validate this path to
      make sure that it is a valid python installation.
    </p>
    <h5>
      I know I have python. I just don't know where to find it! (Windows Only)
    </h5>
    <p>
      If you ordinarily run python from the command line/powershell then the
      correct executable will be on your path. Windows users can run{" "}
      <code>echo $Env:PATH</code> from PowerShell to print the path. In the
      printed output look for a directory ending in something like "
      <code>..\python.exe</code>
      ". Check to make sure that it is the correct path by copying the path and
      running it from your shell. A python repl should appear.
    </p>
    <p>
      If you find the output of the path variables to be annoying, then you can
      always do it by looking at your System settings. See a tutorial{" "}
      <a
        href="https://www.computerhope.com/issues/ch000549.htm"
        target="_blank"
      >
        here
      </a>
      .
    </p>
    <h5>I have IPython, will that work?</h5>
    <p>
      If you have IPython then you have python! Just look for the ordinary "
      <code>Scripts/python.exe</code>" wherever IPython is installed.
    </p>
    <h4>Non-Windows Users</h4>
    <p>
      At this time (December 2018) this wizard will only work as expected for
      Windows users. It is possible to set configuration variables elsewhere and
      some (even most) features should still work.
    </p>
  </Status>
);

export default class PythonVersion extends React.Component {
  constructor(props) {
    super(props);

    this.state = { showHelp: false };

    this.getFileDirectory = this.getFileDirectory.bind(this);
  }
  getFileDirectory(title, properties) {
    dialog.showOpenDialog(
      null,
      {
        title: title,
        defaultPath: homedir(),
        buttonLabel: "Ok",
        properties: properties,
      },
      (filepaths, bookmarks) => {
        this.props.setPanelProps("installPath", filepaths[0]);
      }
    );
  }

  render() {
    const {
      Installer,
      installPath,
      advancedOptions,
      status,
      setPanelProps,
      getFileDirectory,
      ...otherProps
    } = this.props;

    const helpStyle = {
      display: this.state.showHelp ? "grid" : "none",
    };

    return (
      <Card
        header={
          <Header
            toggleHelp={e => this.setState({ showHelp: !this.state.showHelp })}
          />
        }
        className="panel python-version"
      >
        <HelpText
          toggleHelp={() => this.setState({ showHelp: !this.state.showHelp })}
          style={helpStyle}
        />
        <Accordion>
          <AccordionTab header="I do not have python.">
            <NoPython
              Installer={Installer}
              setPanelProps={setPanelProps}
              installPath={installPath}
              advancedOptions={advancedOptions}
              getFileDirectory={this.getFileDirectory}
            />
          </AccordionTab>
          <AccordionTab header="I have python version ≥ 3.5">
            <HavePython
              Installer={Installer}
              setPanelProps={setPanelProps}
              installPath={installPath}
              advancedOptions={advancedOptions}
              getFileDirectory={this.getFileDirectory}
            />
          </AccordionTab>
        </Accordion>
        <div className="panel-status">
          <Status {...status} />
        </div>
      </Card>
    );
  }
}

PythonVersion.propTypes = {
  Installer: PropTypes.object.isRequired,
  installPath: PropTypes.string.isRequired,
  advancedOptions: PropTypes.object.isRequired,
  status: PropTypes.object.isRequired,
  setPanelProps: PropTypes.func.isRequired,
};

PythonVersion.defaultProps = {};
