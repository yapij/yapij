import React from "react";
import PropTypes from "prop-types";
import { newContext } from "immutability-helper";

import { Card } from "primereact/card";
import { Menu } from "primereact/menu";

import { Installer } from "@yapij/js";

import PythonVersion from "./PythonVersion.js";
import Venv from "./Venv.js";

import "./index.scss";

const installYapijUpdate = newContext();

export default class InstallYapij extends React.Component {
  constructor(props) {
    super(props);

    this.Installer = new Installer();
    this.state = this.setInitialState(this.Installer, props);

    this.panels = [PythonVersion, Venv, null];
    this.panelProps = ["pythonVersion", "venv"];

    this.setPanelProps = this.setPanelProps.bind(this);
    this.setInitialState = this.setInitialState.bind(this);
  }

  setInitialState(Installer, props) {
    var state = {
      activeStep: 0,
      pythonVersion: {
        installPath: "",
        advancedOptions: this.Installer.PythonVersion.defaultAdvancedOptions,
        status: {},
      },
      venv: {
        addPath: { name: "", path: "", isDefault: false, yapijInstalled: true },
        existingPath: {
          name: "",
          path: "",
          isDefault: false,
          yapijInstalled: true,
        },
        renameName: "",
        venvDirs: this.Installer.config.get("python.venvDirs"),
        status: {
          intent: "info",
          title: "Why Virtual Environments?",
          msg:
            "It is highly recommended that you configure . For more information about virtual environments, see the help pane.",
        },
      },
    };

    if (
      Installer.config.get("python.python.exec") &&
      Installer.config.get("python.python.v")
    ) {
      state.pythonVersion.status = {
        intent: "info",
        title: "Python Installation Ready",
        msg: "Go to the next step.",
      };
    } else {
      state.pythonVersion.status = {
        intent: "warn",
        title: "Python Installation Incomplete",
        msg: "Select and option below to configure the installation.",
      };
    }

    return state;
  }

  setPanelProps(k, v) {
    var set;
    if (typeof k === "string") {
      set = { [k]: { $set: v } };
    } else {
      set = { [k[0]]: { [k[1]]: { $set: v } } };
    }

    let sk = this.panelProps[this.state.activeStep];

    this.setState({
      [sk]: installYapijUpdate(this.state[sk], set),
    });
  }

  render() {
    const { className, ...otherProps } = this.props;

    const model = [
      {
        label: "Install Python",
        icon: "fab fa-python",
        command: () => this.setState({ activeStep: 0 }),
        className: this.state.activeStep === 0 ? "active" : "",
      },
      {
        label: "Venvs + yapij",
        icon: "fas fa-box",
        command: () => this.setState({ activeStep: 1 }),
        className: this.state.activeStep === 1 ? "active" : "",
      },
    ];

    const Body = this.panels[this.state.activeStep];

    return (
      <div className="install-yapij">
        <Menu model={model} className="install-menu" />
        <Body
          Installer={this.Installer}
          setPanelProps={this.setPanelProps}
          {...this.state[this.panelProps[this.state.activeStep]]}
        />
      </div>
    );
  }
}

InstallYapij.propTypes = {
  className: PropTypes.string,
};

InstallYapij.defaultProps = {
  className: "",
};
