import React from "react";
import PropTypes from "prop-types";
import { Popover, Button, Menu, MenuItem, Icon, Tag } from "@blueprintjs/core";

import "./index.scss";

const icons = {
  0: "info-sign",
  1: "info-sign",
  2: "warning-sign",
  3: "error",
  4: "tick-circle",
};

const intents = { 0: "info", 1: "info", 2: "warn", 3: "error", 4: "success" };

const OneMsg = ({ msg, ii, onDelete, fontSize, ...props }) => (
  <tr className={`message-list-row ${intents[msg.intent]}`} {...props}>
    <td className="icon">
      <Icon icon={icons[msg.intent]} iconSize={fontSize} />
    </td>
    <td>{`[${msg.server}] ${msg.msg}`}</td>
    <td className="date">{`${msg.date.toLocaleTimeString()}`}</td>
    <td className="delete">
      <Icon
        icon="cross"
        iconSize={fontSize}
        onClick={() => onDelete(ii)}
        className="clickable-icon"
      />
    </td>
  </tr>
);

const TableInner = ({ msgs, fontSize, removeOneServerMsg, maxHeight }) => (
  <div className="table-inner-container" style={{ maxHeight: maxHeight }}>
    <table>
      <tbody>
        {msgs.map((msg, ii) => (
          <OneMsg
            key={`ml-${ii}`}
            ii={ii}
            msg={msg}
            fontSize={fontSize}
            onDelete={removeOneServerMsg}
          />
        ))}
      </tbody>
    </table>
  </div>
);

export default class MessageList extends React.Component {
  constructor(props) {
    super(props);

    this.fontSize = 12;
  }

  render() {
    const { config, msgs, maxHeight, width, ...otherProps } = this.props;

    const style = {
      width: `${width}px`,
    };

    var Inner = msgs.length ? (
      <TableInner
        msgs={msgs}
        fontSize={this.fontSize}
        removeOneServerMsg={this.props.removeOneServerMsg}
        maxHeight={maxHeight}
      />
    ) : (
      <code className="no-msg">No Messages</code>
    );

    return (
      <div className={`msg-list ${config.theme}`} style={style}>
        {Inner}
        <div className="footer">
          <Tag
            icon="cross"
            minimal
            interactive
            onClick={this.props.removeAllServerMsg}
            className="delete-all-tag"
          >
            Clear All
          </Tag>
        </div>
      </div>
    );
  }
}

MessageList.propTypes = {
  config: PropTypes.object.isRequired,
  msgs: PropTypes.arrayOf(PropTypes.object),
  removeOneServerMsg: PropTypes.func.isRequired,
  removeAllServerMsg: PropTypes.func.isRequired,
  maxHeight: PropTypes.number,
  width: PropTypes.number,
};
MessageList.defaultProps = {
  maxHeight: 300,
  width: 350,
};
