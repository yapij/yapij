import React from "react";
import PropTypes from "prop-types";

import "./Status.scss";

const Status = ({
  msg,
  title,
  intent,
  className,
  raised,
  children,
  icon,
  ...props
}) => {
  var Title = null;
  if (typeof title === "string") {
    Title = <span className="string-title">{title}</span>;
  } else {
    Title = title;
  }

  return (
    <div
      className={`status ${intent} ${!title ? "no-title" : "with-title"} ${
        raised ? "raised" : ""
      } ${className}`}
      {...props}
    >
      <div className="icon">
        {icon ? <span className={icon} /> : <span className="fas no-icon" />}
      </div>
      <div className="title">{Title}</div>
      <div className="msg">{msg || children}</div>
    </div>
  );
};

Status.propTypes = {
  msg: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  intent: PropTypes.oneOf([
    "help",
    "default",
    "info",
    "secondary",
    "warn",
    "error",
    "success",
  ]),
  className: PropTypes.string,
  raised: PropTypes.bool,
  icon: PropTypes.string,
};

Status.defaultProps = {
  className: "",
  raised: true,
  intent: "default",
};

export default Status;
