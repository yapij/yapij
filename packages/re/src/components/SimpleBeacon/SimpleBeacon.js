import React from "react";
import PropTypes from "prop-types";

export default class SimpleBeacon extends React.Component {
  constructor(props) {
    super(props);
    this.colors_ = {
      0: "white",
      1: "#d9edf7",
      2: "#FFA07A",
      3: "#f2dede",
      4: "#90EE90"
    };
  }

  render() {
    var { yapij, msgs, ...otherProps } = this.props;

    return (
      <table
        className="simple-curator"
        style={{
          fontSize: "8px",
          width: "50vw",
          maxWidth: "50vw",
          minWidth: "50vw"
        }}
      >
        <thead>
          <tr>
            <td>Server</td>
            <td>Message</td>
            <td>Date</td>
          </tr>
        </thead>
        <tbody>
          {msgs.map((msg, ii) => (
            <tr
              key={`msg-${ii}`}
              style={{
                backgroundColor: this.colors_[msg.intent],
                borderBottom: "1px solid rgba(0,0,0,0.5)"
              }}
            >
              <td>{msg.server}</td>
              <td style={{ fontSize: "10px" }}>{msg.msg}</td>
              <td>{`${msg.date.toLocaleString()}`}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
