import React from "react";
import PropTypes from "prop-types";
import { Button, Icon, Tag } from "@blueprintjs/core";
import { OverlayPanel } from "primereact/overlaypanel";

import MessageList from "../MessageList";

const intentMap = {
  0: { intent: "default", icon: "inbox" },
  1: { intent: "info", icon: "info-sign" },
  2: { intent: "warn", icon: "warning-sign" },
  3: { intent: "error", icon: "error" },
  4: { intent: "success", icon: "tick-circle" },
};

const intentMapDefault = {
  0: { intent: "default", icon: "inbox" },
  1: { intent: "default", icon: "info-sign" },
  2: { intent: "default", icon: "warning-sign" },
  3: { intent: "default", icon: "error" },
  4: { intent: "default", icon: "tick-circle" },
};

const StatusIcon = ({ intentIcon, count, theme, ...props }) => {
  return (
    <span className={`status-icon ${theme} ${intentIcon.intent}`} {...props}>
      <Icon iconSize={12} icon={intentIcon.icon} />
      <span className="count">{count > 8 ? "9+" : count}</span>
    </span>
  );
};

export default class Beacon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const iM = this.props.colors ? intentMap : intentMapDefault;

    var notAllZero = false;
    const si = Object.keys(this.props.msgsCount).map(k => {
      if (this.props.msgsCount[k] > 0) {
        notAllZero = true;
        return (
          <StatusIcon
            intentIcon={iM[k]}
            count={this.props.msgsCount[k]}
            theme={this.props.config.theme}
            key={`si-${k}`}
          />
        );
      }
    });

    const inbox = notAllZero ? (
      si
    ) : (
      <StatusIcon
        intentIcon={{ intent: "default", icon: "inbox" }}
        count={0}
        theme={this.props.config.theme}
        className={`status-icon ${
          this.props.config.theme
        } 'default' no-messages`}
      />
    );

    return (
      <React.Fragment>
        <OverlayPanel ref={el => (this.ol = el)}>
          <MessageList
            config={this.props.config}
            msgs={this.props.msgs}
            removeOneServerMsg={this.props.removeOneServerMsg}
            removeAllServerMsg={this.props.removeAllServerMsg}
          />
        </OverlayPanel>
        <Button
          minimal
          className={`${this.props.config.theme}`}
          onClick={e => this.ol.toggle(e)}
        >
          {inbox}
        </Button>
      </React.Fragment>
    );
  }
}

Beacon.propTypes = {
  config: PropTypes.object.isRequired,
  msgs: PropTypes.arrayOf(PropTypes.object).isRequired,
  msgsCount: PropTypes.objectOf(PropTypes.number).isRequired,
  removeOneServerMsg: PropTypes.func.isRequired,
  removeAllServerMsg: PropTypes.func.isRequired,
  colors: PropTypes.bool,
};
Beacon.defaultProps = {
  colors: true,
};
