import React from "react";
import PropTypes from "prop-types";
import { Card } from "primereact/card";

const NotStartedAtAll = ({ onStart }) => (
  <Card title="No Python Kernel Running" className="not-started-splash">
    <span className="fas fa-running icon" />
    <div className="nis-button" onClick={onStart}>
      Start a Kernel
    </div>
  </Card>
);

const Starting = ({ onCancel }) => (
  <Card
    title="Starting Python Kernel..."
    subtitle="(This may take up to a minute depending on your computer.)"
    className="not-started-splash"
  >
    <span className="fas fa-hourglass-start icon" />
    <div className="nis-button" onClick={onCancel}>
      Cancel
    </div>
  </Card>
);

export default class NotStartedSplash extends React.Component {
  constructor(props) {
    super(props);
    this.state = { notStarted: this.props.notStarted };
  }
  render() {
    if (this.state.notStarted) {
      return (
        <NotStartedAtAll
          onStart={() => {
            this.setState({ notStarted: false });
            this.props.onStart();
          }}
        />
      );
    } else {
      return (
        <Starting
          onCancel={() => {
            this.setState({ notStarted: true });
            this.props.onCancel();
          }}
        />
      );
    }
  }
}

NotStartedSplash.propTypes = {
  onStart: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  notStarted: PropTypes.bool,
};

NotStartedSplash.defaultProps = {
  notStarted: true,
};
