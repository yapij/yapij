import React from "react";
import PropTypes from "prop-types";
import { Button, Icon } from "@blueprintjs/core";

import Inputs from "../Inputs";

export default class InputRow extends React.Component {
  render() {
    return (
      <tr className="main-row">
        <td>
          <table>
            <tbody>
              <tr className="input-row">
                <td className="id">
                  <Button
                    icon={
                      <Icon icon="play" iconSize={this.props.config.fontSize} />
                    }
                    minimal
                    onClick={this.props.exec}
                  />
                </td>
                <td className="main">
                  <Inputs.Active
                    config={this.props.config}
                    onSubmit={this.props.exec}
                    onChange={this.props.onChange}
                    value={this.props.value}
                    mainRef={this.props.mainRef}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    );
  }
}

InputRow.propTypes = {
  config: PropTypes.object.isRequired,
  exec: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  mainRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
};
InputRow.defaultProps = {};
