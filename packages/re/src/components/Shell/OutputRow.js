import React from "react";
import PropTypes from "prop-types";
import { Button, Icon, Popover, Menu, MenuItem } from "@blueprintjs/core";

import PyTypes from "../PyTypes";
import { copyToClipboard } from "../../utils";

const PyTypeTooltip = ({
  type,
  uid,
  sessionId,
  date,
  toggleSelf,
  toggleCollapseAll,
  onCopy,
}) => (
  <Menu className="py-type-tooltip">
    <Menu.Divider title={type} />
    <MenuItem text="Copy" icon="duplicate" onClick={onCopy} />
    <MenuItem text="Toggle" icon="layer" onClick={toggleSelf} />
    <MenuItem
      text="Toggle All Outputs"
      icon="layers"
      onClick={() => toggleCollapseAll(["outputs"])}
    />
    <MenuItem text="Info" icon="info-sign">
      <MenuItem
        text={
          <div className="inner">
            <div>
              <b>Completed @</b> {date.toLocaleTimeString()}
              {` (${date.toLocaleDateString()})`}
            </div>
            <div>
              <b>Command ID:</b>
              {uid}
            </div>
            <div>
              <b>Session ID: </b>
              {sessionId}
            </div>
          </div>
        }
      />
    </MenuItem>
  </Menu>
);

export default class OutputRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = { collapse: this.props.collapse };
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.collapse !== this.props.collapse &&
      this.state.collapse !== this.props.collapse
    ) {
      this.setState({ collapse: this.props.collapse });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      nextProps.collapse !== this.state.collapse ||
      nextState.collapse !== this.state.collapse
    );
  }

  render() {
    const {
      output,
      config,
      toggleCollapseAll,
      collapse,
      ...otherProps
    } = this.props;

    return (
      <tr className="output-row" {...otherProps}>
        <td className="id">
          <Popover
            content={
              <PyTypeTooltip
                type={output.type}
                uid={output.uid}
                sessionId={output.sessionId}
                date={output.date}
                toggleSelf={() =>
                  this.setState({ collapse: !this.state.collapse })
                }
                toggleCollapseAll={toggleCollapseAll}
                onCopy={() => copyToClipboard(output.value)}
              />
            }
            minimal
            position={"bottom-left"}
          >
            <Button minimal>
              <div style={config.style}>>>></div>
            </Button>
          </Popover>
        </td>
        <td className="main output">
          <PyTypes
            config={config}
            output={output}
            collapse={this.state.collapse}
            toggleCollapse={() =>
              this.setState({ collapse: !this.state.collapse })
            }
          />
        </td>
      </tr>
    );
  }
}

OutputRow.propTypes = {
  config: PropTypes.object.isRequired,
  output: PropTypes.object.isRequired,
  collapse: PropTypes.bool.isRequired,
  toggleCollapseAll: PropTypes.func.isRequired,
};
OutputRow.defaultProps = {};
