import React from "react";
import PropTypes from "prop-types";
import { Button, Icon, Popover, Menu, MenuItem } from "@blueprintjs/core";

import OutputRow from "./OutputRow.js";
import CmdRow from "./CmdRow.js";

export default class OneCmd extends React.Component {
  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      nextProps.collapse !== this.props.collapse ||
      this.props.cmd.out.length !== nextProps.cmd.out.length ||
      this.props.cmd.status.length !== nextProps.cmd.status.length
    );
  }

  render() {
    const { cmd, config, toggleCollapseAll, exec, ...otherProps } = this.props;
    return (
      <tr className="main-row one-cmd" {...otherProps}>
        <td>
          <table>
            <tbody>
              <CmdRow
                config={config}
                cmd={cmd}
                toggleCollapseAll={toggleCollapseAll}
                collapse={otherProps.collapse.inputs}
                exec={exec}
              />
              {cmd.out.map((outi, ii) => (
                <OutputRow
                  key={`out-${cmd.uid}-${ii}`}
                  output={outi}
                  config={config}
                  toggleCollapseAll={toggleCollapseAll}
                  collapse={otherProps.collapse.outputs}
                />
              ))}
            </tbody>
          </table>
        </td>
      </tr>
    );
  }
}

OneCmd.propTypes = {
  config: PropTypes.object.isRequired,
  cmd: PropTypes.object.isRequired,
  collapse: PropTypes.object.isRequired,
  toggleCollapseAll: PropTypes.func.isRequired,
  exec: PropTypes.func.isRequired,
};
OneCmd.defaultProps = {};
