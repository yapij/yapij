import React from "react";
import PropTypes from "prop-types";
import { Button } from "@blueprintjs/core";

import Inputs from "../Inputs";

import InputRow from "./InputRow.js";
import OneCmd from "./OneCmd.js";

export default class Main extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <table className={`yapij-shell ${this.props.config.theme}`}>
        <tbody>
          {Object.keys(this.props.collection).map((k, ii) => (
            <OneCmd
              key={`oc-${ii}`}
              cmd={this.props.collection[k]}
              config={this.props.config}
              collapse={this.props.collapse}
              toggleCollapseAll={this.props.toggleCollapseAll}
              exec={this.props.yapij.exec}
            />
          ))}
          <InputRow
            onChange={this.props.onInputChange}
            value={this.props.inputValue}
            exec={this.props.onExecCommand}
            config={this.props.config}
            mainRef={this.props.mainRef}
          />
        </tbody>
      </table>
    );
  }
}

Main.propTypes = {
  config: PropTypes.object.isRequired,
  collection: PropTypes.object.isRequired,
  yapij: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
  onExecCommand: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  inputValue: PropTypes.string.isRequired,
  collapse: PropTypes.object.isRequired,
  toggleCollapseAll: PropTypes.func.isRequired,
  mainRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
};
Main.defaultProps = {};
