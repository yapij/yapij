import React from "react";
import PropTypes from "prop-types";
import { Button, Icon, Popover, Menu, MenuItem } from "@blueprintjs/core";

import { copyToClipboard } from "../../utils";
import Inputs from "../Inputs";
import OutputStatus from "./OutputStatus.js";

const Placeholder = ({ config, onClick, ...props }) => {
  return (
    <Button onClick={onClick} style={config} minimal className="hidden-button">
      <code>
        <em>[Hidden - Click to open.]</em>
      </code>
    </Button>
  );
};

const PyTypeTooltip = ({ toggleSelf, toggleCollapseAll, runAgain, onCopy }) => (
  <Menu className="py-type-tooltip">
    <MenuItem text="Copy" icon="duplicate" onClick={onCopy} />
    <MenuItem text="Toggle" icon="layer" onClick={toggleSelf} />
    <MenuItem
      text="Toggle All Commands"
      icon="layers"
      onClick={() => toggleCollapseAll(["inputs"])}
    />
    <MenuItem text="Run Again" icon="repeat" onClick={runAgain} />
  </Menu>
);

export default class CmdRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = { collapse: this.props.collapse };

    this.runAgain = this.runAgain.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.collapse !== this.props.collapse &&
      this.state.collapse !== this.props.collapse
    ) {
      this.setState({ collapse: this.props.collapse });
    }
  }

  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      nextProps.collapse !== this.props.collapse ||
      this.props.cmd.status.length !== nextProps.cmd.status.length
    );
  }

  runAgain() {
    this.props.exec(this.props.cmd.in);
  }

  render() {
    const {
      toggleCollapseAll,
      config,
      cmd,
      collapse,
      exec,
      ...otherProps
    } = this.props;

    const Input_ = this.state.collapse ? (
      <Placeholder
        config={config.style}
        onClick={() => this.setState({ collapse: !this.state.collapse })}
      />
    ) : (
      <Inputs.Passive config={config} input={cmd.in} />
    );

    return (
      <tr className="cmd-row" {...otherProps}>
        <td className="id">
          <Popover
            content={
              <PyTypeTooltip
                toggleSelf={() =>
                  this.setState({ collapse: !this.state.collapse })
                }
                toggleCollapseAll={toggleCollapseAll}
                runAgain={this.runAgain}
                onCopy={() => copyToClipboard(cmd.in)}
              />
            }
            minimal
            position={"bottom-left"}
          >
            <Button minimal>
              <OutputStatus
                config={config}
                index={cmd.index}
                status={cmd.status}
              />
            </Button>
          </Popover>
        </td>
        <td className="main">{Input_}</td>
      </tr>
    );
  }
}

CmdRow.propTypes = {
  config: PropTypes.object.isRequired,
  cmd: PropTypes.object.isRequired,
  collapse: PropTypes.bool.isRequired,
  toggleCollapseAll: PropTypes.func.isRequired,
  exec: PropTypes.func.isRequired,
};

CmdRow.defaultProps = {};
