import React from "react";
import PropTypes from "prop-types";
import { Button, Icon } from "@blueprintjs/core";
import { OverlayPanel } from "primereact/overlaypanel";

import ShellMenuButton from "./ShellMenuButton";
import PyRunning from "../PyRunning";
import Beacon from "../Beacon";
import WorkspaceMonitor from "../WorkspaceMonitor";
import "./Navbar.scss";

export default class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }
  shouldComponentUpdate(nextProps) {
    return (
      JSON.stringify(nextProps.config) !== JSON.stringify(this.props.config) ||
      JSON.stringify(nextProps.info) !== JSON.stringify(this.props.info) ||
      nextProps.top !== this.props.top ||
      nextProps.msgsCount !== this.props.msgsCount ||
      nextProps.colors !== this.props.colors
    );
  }

  render() {
    return (
      <div className={`shell-navbar ${this.props.config.theme}`}>
        <ShellMenuButton
          yapij={this.props.yapij}
          theme={this.props.config.theme}
          info={this.props.info}
          top={this.props.top}
          toggleCollapseAll={this.props.toggleCollapseAll}
        />
        <div className="py-running-container">
          <PyRunning
            running={this.props.info.active || false}
            started={this.props.info.ready || false}
            theme={this.props.config.theme}
            size={17}
          />
        </div>
        <div />
        <div className="beacon">
          <Beacon
            config={this.props.config}
            msgs={this.props.msgs}
            msgsCount={this.props.msgsCount}
            removeOneServerMsg={this.props.removeOneServerMsg}
            removeAllServerMsg={this.props.removeAllServerMsg}
            popoverPosition={this.props.top ? "bottom-right" : "top-right"}
            colors={this.props.colors}
          />
        </div>
        <div className="workspace">
          <OverlayPanel ref={el => (this.WorkspaceMonitorOverlay = el)}>
            <WorkspaceMonitor yapij={this.props.yapij} info={this.props.info} />
          </OverlayPanel>
          <Button
            icon="briefcase"
            className={`workspace ${this.props.config.theme}`}
            minimal
            onClick={e => this.WorkspaceMonitorOverlay.toggle(e)}
          />
        </div>
      </div>
    );
  }
}

Navbar.propTypes = {
  yapij: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
  info: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
  top: PropTypes.bool.isRequired,
  toggleCollapseAll: PropTypes.func.isRequired,
  msgs: PropTypes.arrayOf(PropTypes.object).isRequired,
  msgsCount: PropTypes.objectOf(PropTypes.number).isRequired,
  removeOneServerMsg: PropTypes.func.isRequired,
  removeAllServerMsg: PropTypes.func.isRequired,
  colors: PropTypes.bool,
};
Navbar.defaultProps = {};
