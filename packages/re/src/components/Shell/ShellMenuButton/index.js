import React from "react";
import PropTypes from "prop-types";
import { Popover, Button, Menu, MenuItem, Icon } from "@blueprintjs/core";
import { TieredMenu } from "primereact/tieredmenu";

import PyIcon from "../../PyRunning/PythonIcon.js";
import ShellMenu from "./ShellMenu.js";
import "./index.scss";

class ShellMenuButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    const {
      theme,
      yapij,
      info,
      toggleCollapseAll,
      className,
      top,
      ...otherProps
    } = this.props;

    return (
      <div className={`kernel-menu ${theme} ${className}`} {...otherProps}>
        <ShellMenu
          yapij={yapij}
          info={info}
          toggleCollapseAll={toggleCollapseAll}
          ref_={el => (this.menu = el)}
        />
        <Button
          className="menu-button"
          minimal
          onClick={e => this.menu.toggle(e)}
        >
          <Icon icon="menu" iconSize={14} />
        </Button>
      </div>
    );
  }
}
ShellMenuButton.propTypes = {
  yapij: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
  info: PropTypes.object.isRequired,
  theme: PropTypes.string.isRequired,
  top: PropTypes.bool.isRequired,
  toggleCollapseAll: PropTypes.func.isRequired,
};
ShellMenuButton.defaultProps = {};

export default ShellMenuButton;
