import React from "react";
import PropTypes from "prop-types";
import { TieredMenu } from "primereact/tieredmenu";

export default class ShellMenu extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { yapij, info, toggleCollapseAll, ref_, ...otherProps } = this.props;

    const model = [
      {
        label: "New",
        icon: "fas fa-file",
        command: () => yapij.workspace.new(),
      },
      { separator: true },
      {
        label: "Load",
        icon: "fas fa-folder-open",
        command: () => yapij.workspace.load(),
      },
      {
        label: "Save",
        icon: "fas fa-save",
        command: () => yapij.workspace.save(),
      },
      {
        label: "Save As...",
        icon: "far fa-save",
        command: () => yapij.workspace.saveAs(),
      },
      { separator: true },
      {
        label: "Rename",
        icon: "fas fa-strikethrough",
        command: () => yapij.workspace.rename(),
      },
      {
        label: "Copy",
        icon: "fas fa-copy",
        command: () => yapij.workspace.copy(),
      },
      { separator: true },
      {
        label: "Kernel",
        icon: "fab fa-python",
        items: [
          {
            label: "Start",
            icon: "pi pi-caret-right",
            command: () => yapij.start(),
            disabled: info.ready,
          },
          {
            label: "Stop",
            icon: "fas fa-stop",
            command: () => yapij.stop(),
            disabled: !info.ready,
          },
          {
            label: "Restart",
            icon: "fas fa-sync-alt",
            command: () => yapij.restart(),
            disabled: !info.ready,
          },
          { separator: true },
          {
            label: "Interrupt",
            icon: "fas fa-pause",
            command: () => yapij.interrupt(),
            disabled: !info.running,
          },
          {
            label: "Ready",
            icon: "fas fa-check",
            command: () => yapij.ready(),
          },
        ],
      },
      {
        label: "Toggle",
        icon: "fas fa-toggle-on",
        items: [
          {
            label: "All",
            command: () => toggleCollapseAll(["inputs", "outputs"]),
          },
          {
            label: "Commands",
            command: () => toggleCollapseAll(["inputs"]),
          },
          {
            label: "Output",
            command: () => toggleCollapseAll(["outputs"]),
          },
        ],
      },
    ];
    return <TieredMenu model={model} popup={true} ref={ref_} {...otherProps} />;
  }
}

ShellMenu.propTypes = {
  yapij: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
  info: PropTypes.object.isRequired,
  toggleCollapseAll: PropTypes.func.isRequired,
  ref_: PropTypes.func.isRequired,
};
ShellMenu.defaultProps = {};
