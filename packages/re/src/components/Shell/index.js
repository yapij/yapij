import React from "react";
import PropTypes from "prop-types";
import { Button } from "@blueprintjs/core";
import ReactResizeDetector from "react-resize-detector";
import { Growl } from "primereact/growl";
import update from "immutability-helper";

import Inputs from "../Inputs";

import "./index.scss";
import Main from "./Main.js";
import Navbar from "./Navbar.js";
import NotStartedSplash from "./NotStartedSplash.js";

export default class Shell extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      collapse: { inputs: false, outputs: false },
      dims: {
        height: "500px",
        width: "500px",
        maxHeight: "500px",
        maxWidth: "500px",
      },
    };

    this.execCommand = this.execCommand.bind(this);
    this.toggleCollapseAll = this.toggleCollapseAll.bind(this);
  }

  componentDidUpdate() {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  }

  execCommand(v) {
    if (typeof v === "string") {
      this.props.yapij.exec(v);
    } else {
      this.props.yapij.exec(this.state.inputValue);
    }

    this.setState({ inputValue: "" });
  }

  toggleCollapseAll(k) {
    if (typeof k === "string") k = [k];
    this.setState({ collapse: update(this.state.collapse, { $toggle: k }) });
  }

  render() {
    var footer = this.props.headerfooter === "footer",
      header = this.props.headerfooter === "header";
    const ownClass = `shell ${footer ? "include-footer" : ""} ${
      header ? "include-header" : ""
    }`;

    return (
      <ReactResizeDetector
        handleWidth
        handleHeight
        onResize={(w, h) => {
          let d;
          if (this.props.headerfooter) {
            d = {
              height: `${h - 30}px`,
              width: `${w}px`,
              maxHeight: `${h - 30}px`,
              maxWidth: `${w}px`,
            };
          } else {
            d = {
              height: `${h}px`,
              width: `${w}px`,
              maxHeight: `${h}px`,
              maxWidth: `${w}px`,
            };
          }
          this.setState({ dims: d });
        }}
      >
        <div className={ownClass}>
          {header ? (
            <Navbar
              config={this.props.config}
              yapij={this.props.yapij}
              info={this.props.info}
              top={true}
              toggleCollapseAll={this.toggleCollapseAll}
              msgs={this.props.msgs}
              msgsCount={this.props.msgsCount}
              removeOneServerMsg={this.props.removeOneServerMsg}
              removeAllServerMsg={this.props.removeAllServerMsg}
              colors={this.props.colors}
            />
          ) : null}
          <div
            className={`main ${this.props.config.theme}`}
            style={this.state.dims}
            ref={el => {
              this.mainRef = el;
            }}
          >
            {this.props.info.ready ? (
              <Main
                yapij={this.props.yapij}
                collection={this.props.collection}
                config={this.props.config}
                inputValue={this.state.inputValue}
                onExecCommand={this.execCommand}
                onInputChange={v => this.setState({ inputValue: v })}
                toggleCollapseAll={this.toggleCollapseAll}
                collapse={this.state.collapse}
                mainRef={this.mainRef}
              />
            ) : (
              <NotStartedSplash
                onStart={this.props.yapij.start}
                onCancel={() =>
                  this.cancelKernelStart.show({
                    summary: "Cancel not yet implemented",
                    detail: (
                      <span>
                        If you're having issues with starting the component, do
                        a hard-reset of the window. Please also{" "}
                        <a
                          href="https://gitlab.com/yapij/yapij/issues/32"
                          target="_blank"
                        >
                          comment on this
                        </a>{" "}
                        issue so that it will be resolved.
                      </span>
                    ),
                    severity: "error",
                  })
                }
              />
            )}
            <div
              className="scroll-to-bottom"
              ref={el => {
                this.messagesEnd = el;
              }}
            />
            <Growl ref={el => (this.cancelKernelStart = el)} />
          </div>
          {footer ? (
            <Navbar
              config={this.props.config}
              yapij={this.props.yapij}
              info={this.props.info}
              top={false}
              toggleCollapseAll={this.toggleCollapseAll}
              msgs={this.props.msgs}
              msgsCount={this.props.msgsCount}
              removeOneServerMsg={this.props.removeOneServerMsg}
              removeAllServerMsg={this.props.removeAllServerMsg}
              colors={this.props.colors}
            />
          ) : null}
        </div>
      </ReactResizeDetector>
    );
  }
}

Shell.propTypes = {
  config: PropTypes.object.isRequired,
  collection: PropTypes.object.isRequired,
  yapij: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
  headerfooter: PropTypes.string,
  info: PropTypes.object.isRequired,
  msgs: PropTypes.arrayOf(PropTypes.object).isRequired,
  msgsCount: PropTypes.objectOf(PropTypes.number).isRequired,
  removeOneServerMsg: PropTypes.func.isRequired,
  removeAllServerMsg: PropTypes.func.isRequired,
  colors: PropTypes.bool,
};
Shell.defaultProps = {
  headerfooter: "header",
  colors: true,
};
