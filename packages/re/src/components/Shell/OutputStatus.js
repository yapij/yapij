import React from "react";
import PropTypes from "prop-types";

import { Icon, Button } from "@blueprintjs/core";

const NotYetReceived = ({ config, index, status }) => {
  const color = config.intent.warn,
    fontSize = config.fontSize;
  return <Icon icon="asterisk" iconSize={fontSize} color={color} />;
};

const ReceivedQueued = ({ config, index, status }) => {
  const color = config.intent.info,
    fontSize = config.fontSize;
  return <Icon icon="asterisk" iconSize={fontSize} color={color} />;
};

const Started = ({ config, index, status }) => {
  const color = config.intent.info,
    fontSize = config.fontSize;
  return <Icon icon="walk" iconSize={fontSize} color={color} />;
};

const Finished = ({ config, index, status }) => {
  const inner = status.codes[1] in [7, 8] ? `{${index}}` : `[${index}]`;
  const color =
    status.codes[1] in [5, 8]
      ? config.intent.danger
      : status.codes[1] === 4
        ? config.intent.warn
        : config.intent.success;
  const style = { color: color, fontSize: config.fontSize };
  return <div style={style}>{inner}</div>;
};

const Statii = {
  0: ReceivedQueued,
  1: ReceivedQueued,
  2: Started,
  3: Finished,
};

export default class OutputStatus extends React.Component {
  render() {
    const { status, index, config, ...otherProps } = this.props;
    const s = status.slice(-1)[0];
    const StatusInner = s ? Statii[s.codes[0]] : NotYetReceived;

    return <StatusInner status={s} index={index} config={config} />;
  }
}

OutputStatus.propTypes = {
  config: PropTypes.object.isRequired,
  status: PropTypes.arrayOf(PropTypes.object).isRequired,
  index: PropTypes.number.isRequired,
};
OutputStatus.defaultProps = {};
