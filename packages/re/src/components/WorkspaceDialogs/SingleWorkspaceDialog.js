import React from "react";
import PropTypes from "prop-types";

import { Overlay, Tag, InputGroup } from "@blueprintjs/core";
import { Dialog } from "primereact/dialog";
import Default from "./default.js";

export default class SingleWorkspaceDialog extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      className,
      children,
      isOpen,
      header,
      footer,
      ...otherProps
    } = this.props;

    return (
      <Dialog
        visible={isOpen}
        header={header}
        footer={footer}
        className={`abstract-workspace-dialog ${className}`}
        closeOnEscape={true}
        dismissableMask={true}
        blockScroll={true}
        closeable={true}
        responsive={true}
        {...otherProps}
      >
        {children}
      </Dialog>
    );
  }
}

SingleWorkspaceDialog.propType = {
  isOpen: PropTypes.bool.isRequired,
  header: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  footer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  children: PropTypes.element.isRequired,
  className: PropTypes.string,
};

SingleWorkspaceDialog.defaultProps = {
  className: "",
};
