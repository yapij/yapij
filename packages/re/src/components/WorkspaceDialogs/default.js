import React from "react";
import PropTypes from "prop-types";

import {
  Tag,
  InputGroup,
  FormGroup,
  Icon,
  ControlGroup,
  Callout,
  H3,
} from "@blueprintjs/core";
import shortid from "shortid";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";

import "./default.scss";

class AbstractDefault extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      v: "",
    };

    this.handleKeypress = this.handleKeypress.bind(this);
    this._id = shortid.generate();
  }

  handleKeypress(e) {
    if (e.key === "Enter") {
      this.setState({ v: "" });
      this.props.onSubmit(this.state.v);
    } else if (e.key === "Escape") {
      this.setState({ v: "" });
      this.props.onCancel();
    }
  }

  render() {
    const {
      isOpen,
      title,
      icon,
      className,
      workspace,
      onSubmit,
      onCancel,
      ...otherProps
    } = this.props;

    const header = title,
      footer = null;
    return (
      <Dialog
        visible={isOpen}
        header={header}
        footer={footer}
        className={`workspace-dialog-abstract-default ${className}`}
        closeOnEscape={true}
        dismissableMask={true}
        blockScroll={true}
        closeable={true}
        responsive={true}
        onHide={e => onCancel()}
        onShow={e => null}
        {...otherProps}
      >
        <span className="p-float-label">
          <br />
          <InputText
            id={this._id}
            value={this.state.value}
            onChange={e => this.setState({ v: e.target.value })}
            onKeyDown={this.handleKeypress}
            type="text"
            size="40"
          />
          <label htmlFor={this._id}>{title}</label>
        </span>
        <div className="helpertext">
          <div>Press "Enter" to submit and "Escape" to cancel.</div>
          <div>
            (Current File:{" "}
            <em>{workspace.metadata.info.filename || "No Current File"}</em>)
          </div>
        </div>
      </Dialog>
    );
  }
}

AbstractDefault.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  workspace: PropTypes.objectOf(PropTypes.object),
  className: PropTypes.string,
};

AbstractDefault.defaultProps = {
  className: "",
};

const SaveAs = ({ isOpen, onSubmit, onCancel, workspace }) => (
  <AbstractDefault
    title="Save As"
    icon="fas fa-save"
    onSubmit={onSubmit}
    onCancel={onCancel}
    workspace={workspace}
    isOpen={isOpen}
  />
);

const Save = ({ isOpen, onSubmit, onCancel, workspace }) => (
  <AbstractDefault
    title="Save"
    icon="far fa-save"
    onSubmit={onSubmit}
    onCancel={onCancel}
    workspace={workspace}
    isOpen={isOpen}
  />
);

const New = ({ isOpen, onSubmit, onCancel, workspace }) => (
  <AbstractDefault
    title="New"
    icon="fas fa-file"
    onSubmit={onSubmit}
    onCancel={onCancel}
    workspace={workspace}
    isOpen={isOpen}
  />
);

const Load = ({ isOpen, onSubmit, onCancel, workspace }) => (
  <AbstractDefault
    title="Load"
    icon="fas fa-folder-open"
    onSubmit={onSubmit}
    onCancel={onCancel}
    workspace={workspace}
    isOpen={isOpen}
  />
);

const Rename = ({ isOpen, onSubmit, onCancel, workspace }) => (
  <AbstractDefault
    title="Rename"
    icon="fas fa-strikethrough"
    onSubmit={onSubmit}
    onCancel={onCancel}
    workspace={workspace}
    isOpen={isOpen}
  />
);

const Copy = ({ isOpen, onSubmit, onCancel, workspace }) => (
  <AbstractDefault
    title="Copy"
    icon="fas fa-copy"
    onSubmit={onSubmit}
    onCancel={onCancel}
    workspace={workspace}
    isOpen={isOpen}
  />
);

const CD = ({ isOpen, onSubmit, onCancel, workspace }) => (
  <AbstractDefault
    title="Set the working directory"
    icon="fas fa-code"
    onSubmit={onSubmit}
    onCancel={onCancel}
    workspace={workspace}
    isOpen={isOpen}
  />
);

class Delete extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      v: "",
    };

    this.handleKey = this.handleKey.bind(this);
  }

  handleKey(e) {
    if (e.key === "Enter") {
      this.setState({ v: "" });
      this.props.onSubmit(this.state.v);
    } else if (e.key === "Escape") {
      this.setState({ v: "" });
      this.props.onCancel();
    }
  }

  render() {
    const {
      isOpen,
      onCancel,
      onSubmit,
      workspace,
      className,
      ...otherProps
    } = this.props;

    const Footer = (
      <div>
        <Button
          label="Cancel"
          onClick={onCancel}
          className="p-button-raised p-button-secondary"
        />{" "}
        <Button
          label="Delete"
          onClick={onSubmit}
          className="p-button-raised p-button-danger"
        />
        <div className="helpertext">
          Press "Enter" to delete and "Escape" to cancel.
        </div>
      </div>
    );

    return (
      <Dialog
        visible={isOpen}
        header={"Delete?"}
        footer={Footer}
        className={`workspace-dialog-abstract-default ${className}`}
        closeOnEscape={true}
        dismissableMask={true}
        blockScroll={true}
        closeable={true}
        responsive={true}
        className={className}
        onHide={e => onCancel()}
        onShow={e => null}
        {...otherProps}
      >
        <div className="workspace-dialog-abstract" onKeyDown={this.handleKey}>
          {workspace.metadata.info.filename
            ? `Are you sure you want to delete "${
                workspace.metadata.info.filename
              }"?`
            : `(No file to delete)`}
        </div>
      </Dialog>
    );
  }
}

Delete.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  workspace: PropTypes.objectOf(PropTypes.object).isRequired,
  className: PropTypes.string,
};

Delete.defaultProps = {};

export default { Save, SaveAs, Load, New, Rename, Delete, Copy, CD };
