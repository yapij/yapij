# WorkspaceDialogs

Dialogs for handling save, load, and other workspace/server-specific functions.

## Architecture

- Each server has a designated `WorkspaceDialog` component, which is created as a child of `YapijProvider`.
- `WorkspaceDialog` listens for pubsub commands issued via methods in components hooked via `connectYapij`.
- Relevant commands related to `WorkspaceDialog`:
  - SaveAs
  - Save
  - Load
  - New
  - Rename
  - Delete
  - Copy
  - CD (Set the working directory.)
- Each of the commands are associated with a dialog.
- Required attributes of each dialog:
  - `isOpen`
  - `onSubmit`
  - `onCancel`
  - `workspace`
