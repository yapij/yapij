import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import { Growl } from "primereact/growl";
import PubSub from "pubsub-js";

import Default from "./default.js";
import SingleWorkspaceDialog from "./SingleWorkspaceDialog.js";

export default class WorkspaceDialogs extends React.Component {
  constructor(props) {
    super(props);

    this.toggleOverlays = this.toggleOverlays.bind(this);
    this.onEnter = this.onEnter.bind(this);

    this.onSaveSubmit = this.onSaveSubmit.bind(this);
    this.onSaveAsSubmit = this.onSaveAsSubmit.bind(this);
    this.onLoadSubmit = this.onLoadSubmit.bind(this);
    this.onNewSubmit = this.onNewSubmit.bind(this);
    this.onRenameSubmit = this.onRenameSubmit.bind(this);
    this.onDeleteSubmit = this.onDeleteSubmit.bind(this);
    this.onCopySubmit = this.onCopySubmit.bind(this);
    this.onCDSubmit = this.onCDSubmit.bind(this);

    this.methods = [
      "SaveAs",
      "Save",
      "Load",
      "New",
      "Rename",
      "Delete",
      "Copy",
      "CD",
    ];

    this.state = {
      isOpen: {
        SaveAs: false,
        Save: false,
        Load: false,
        New: false,
        Rename: false,
        Delete: false,
        Copy: false,
        CD: false,
      },
    };
  }

  componentDidMount() {
    PubSub.subscribe(this.props.pubSubKey, (topic, msg) =>
      this.handleCall(topic, msg)
    );
  }

  componentWillUnmount() {
    PubSub.clearAllSubscriptions();
  }

  toggleOverlays(k) {
    this.setState({ isOpen: update(this.state.isOpen, { $toggle: [k] }) });
  }

  handleCall(topic, { method, serverName, collectorName, args, e }) {
    if (serverName !== this.props.serverName) {
      return null;
    } else if (!this.props.info || !this.props.info.ready) {
      console.warn(
        `Cannot call workspace method because server "${serverName}" is not started.`
      );
      return null;
    }

    switch (method) {
      case "SaveAs":
        this.onEnter("SaveAs", "filename", args);
        break;
      case "Save":
        this.onEnter("Save", "filename", args);
        break;
      case "Load":
        this.onEnter("Load", "filename", args);
        break;
      case "New":
        this.onEnter("New", "filename", args);
        break;
      case "Rename":
        this.onEnter("Rename", "filename", args);
        break;
      case "Delete":
        this.onEnter("Delete", "filename", args);
        break;
      case "Copy":
        this.onEnter("Copy", "filename", args);
        break;
      case "CD":
        this.onEnter("CD", "cd", args);
        break;
      default:
        throw new Error(
          `Unknown method "${method}" passed to WorkspaceDialog for server "${serverName}"`
        );
    }
  }

  onEnter(k, ak, args) {
    if (!this.props.info.ready) {
      this.props.yapij.start(() => {
        this.props.yapij.exec("%workspace_info", true, () =>
          this.onEnter(k, ak, args)
        );
      });
    } else if (!this.props.info.workspace) {
      this.props.yapij.exec("%workspace_info", true, () =>
        this.onEnter(k, ak, args)
      );
    } else if (args[ak]) {
      this[`on${k}Submit`](args[ak]);
    } else if (
      k === "Save" &&
      !this.props.info.workspace.metadata.info.filename.startsWith("untitled")
    ) {
      this.onSaveSubmit(this.props.info.workspace.metadata.info.filename);
    } else {
      this.toggleOverlays(k);
    }
  }

  onSaveAsSubmit(v) {
    this.toggleOverlays("SaveAs");
    this.props.yapij.exec("%copy", true, () =>
      this.props.yapij.exec(`%rename "${v}"`, true, () =>
        this.props.yapij.exec("%workspace_info", true)
      )
    );
  }
  onSaveSubmit(v) {
    this.toggleOverlays("Save");
    this.props.yapij.exec("%save", false, () =>
      this.props.yapij.exec("%workspace_info", true)
    );
  }
  onLoadSubmit(v) {
    this.toggleOverlays("Load");
    this.props.yapij.exec(`%load "${v}"`, false, () =>
      this.props.yapij.exec("%workspace_info", true)
    );
  }
  onNewSubmit(v) {
    this.toggleOverlays("New");
    this.props.yapij.exec(`%new "${v}"`, false, () =>
      this.props.yapij.exec("%workspace_info", true)
    );
  }
  onRenameSubmit(v) {
    this.toggleOverlays("Rename");
    this.props.yapij.exec(`%rename "${v}"`, false, () =>
      this.props.yapij.exec("%workspace_info", true)
    );
  }
  onDeleteSubmit(v) {
    this.toggleOverlays("Delete");
    this.props.yapij.exec(`%delete "${v}"`, false, () =>
      this.props.yapij.exec("%workspace_info", true)
    );
  }
  onCopySubmit(v) {
    this.toggleOverlays("Copy");
    this.props.yapij.exec(`%copy "${v}"`, false, () =>
      this.props.yapij.exec("%workspace_info", true)
    );
  }
  onCDSubmit(v) {
    this.toggleOverlays("CD");
    this.props.yapij.exec(`%cd "${v}"`, false, () =>
      this.props.yapij.exec("%workspace_info", true)
    );
  }

  render() {
    const {
      className,
      subscribe,
      workspaceDialogs,
      dispatch,
      info,
      serverName,
      pubSubKey,
      ...otherProps
    } = this.props;
    const isOpen = this.state.isOpen;
    const workspace =
      info && info.workspace
        ? info.workspace.workspace
        : { metadata: { info: { filename: null } } };

    return (
      <div className={`workspace-dialogs ${className}`} {...otherProps}>
        {info ? (
          <div>
            <workspaceDialogs.SaveAs
              onSubmit={this.onSaveAsSubmit}
              onCancel={() => this.toggleOverlays("SaveAs")}
              workspace={workspace}
              isOpen={isOpen.SaveAs}
            />
            <workspaceDialogs.Save
              onSubmit={this.onSaveAsSubmit}
              onCancel={() => this.toggleOverlays("Save")}
              workspace={workspace}
              isOpen={isOpen.Save}
            />
            <workspaceDialogs.Load
              onSubmit={this.onLoadSubmit}
              onCancel={() => this.toggleOverlays("Load")}
              workspace={workspace}
              isOpen={isOpen.Load}
            />
            <workspaceDialogs.New
              onSubmit={this.onNewSubmit}
              onCancel={() => this.toggleOverlays("New")}
              workspace={workspace}
              isOpen={isOpen.New}
            />
            <workspaceDialogs.Rename
              onSubmit={this.onRenameSubmit}
              onCancel={() => this.toggleOverlays("Rename")}
              workspace={workspace}
              isOpen={isOpen.Rename}
            />
            <workspaceDialogs.Delete
              onSubmit={this.onDeleteSubmit}
              onCancel={() => this.toggleOverlays("Delete")}
              workspace={workspace}
              isOpen={isOpen.Delete}
            />
            <workspaceDialogs.Copy
              onSubmit={this.onCopySubmit}
              onCancel={() => this.toggleOverlays("Copy")}
              workspace={workspace}
              isOpen={isOpen.Copy}
            />
            <workspaceDialogs.CD
              onSubmit={this.onCDSubmit}
              onCancel={() => this.toggleOverlays("CD")}
              workspace={workspace}
              isOpen={isOpen.CD}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

WorkspaceDialogs.propType = {
  serverName: PropTypes.string.isRequired,
  info: PropTypes.objectOf(PropTypes.object).isRequired,
  yapij: PropTypes.objectOf(PropTypes.func).isRequired,
  workspaceDialogs: PropTypes.exact({
    SaveAs: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Save: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Load: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    New: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Rename: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Delete: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Copy: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    CD: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
  }).isRequired,
  pubSubKey: PropTypes.string.isRequired,
};

WorkspaceDialogs.defaultProps = {
  workspaceDialogs: {
    Save: Default.Save,
    SaveAs: Default.SaveAs,
    Load: Default.Load,
    New: Default.New,
    Rename: Default.Rename,
    Delete: Default.Delete,
    Copy: Default.Copy,
    CD: Default.CD,
  },
};

export { SingleWorkspaceDialog, Default };
