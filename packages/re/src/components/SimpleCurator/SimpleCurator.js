import React from "react";
import PropTypes from "prop-types";

class SimpleCurator extends React.Component {
  constructor(props) {
    super(props);
    this.state = { code: "" };
  }

  render() {
    var { yapij, collection, ...otherProps } = this.props;

    return (
      <div
        className="Simple-curator"
        style={{ width: "50vw", maxWidth: "50vw" }}
      >
        <nav>
          <b>Python</b> |<button onClick={() => yapij.start()}>Start</button>
          <button onClick={yapij.stop}>Stop</button>
          <button onClick={yapij.restart}>Restart</button>
          <button onClick={yapij.interrupt}>Interrupt</button>
        </nav>
        <div
          style={{
            fontSize: "0.6em",
          }}
        >
          <table>
            <tbody>
              {Object.keys(collection).map((uid, ii) => {
                var cmd = collection[uid];
                return (
                  <tr key={`cmd-${ii}`}>
                    <td>
                      <table
                        style={{
                          borderBottom: "1px solid rgba(0,0,0,0.1)",
                          width: "49vw",
                          maxWidth: "49vw",
                        }}
                      >
                        <colgroup>
                          <col width="80" />
                          <col width="20" />
                          <col width="900" />
                          <col width="100" />
                        </colgroup>
                        <thead>
                          <tr key={`in-${ii}`}>
                            <td>{`In[${cmd.index}]`}</td>
                            <td>{cmd.uid}</td>
                            <td style={{ fontSize: "12px" }}>
                              <code>{cmd.in}</code>
                            </td>
                            <td>{`${cmd.date.toLocaleString()}`}</td>
                          </tr>
                        </thead>
                        <tbody>
                          {cmd.out.map((out, jj) => (
                            <tr key={`out-${ii}-${jj}`}>
                              <td>Out</td>
                              <td>{out.uid}</td>
                              <td
                                style={{
                                  maxWidth: "500px",
                                  textOverflow: "ellipses",
                                  fontSize: "12px",
                                }}
                              >
                                <code>
                                  {out.value.toString().slice(0, 30) + "..."}
                                </code>
                              </td>
                              <td>{`${out.date.toLocaleString()}`}</td>
                            </tr>
                          ))}
                        </tbody>
                        <tfoot>
                          {(() => {
                            if (cmd.status.length) {
                              var s = cmd.status.slice(-1)[0];
                              return (
                                <tr className="status" key={`status-${ii}`}>
                                  <td />

                                  <td>{`${s.names[0]}`}</td>
                                  <td>{`${s.names[1]}`}</td>
                                  <td>{`(${s.date.toLocaleString()})`}</td>
                                </tr>
                              );
                            } else {
                              return (
                                <tr className="status">
                                  <td />
                                  <td />
                                  <td />
                                  <td />
                                </tr>
                              );
                            }
                          })()}
                        </tfoot>
                      </table>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <hr />
        <div>
          <textarea
            placeholder="python code here"
            value={this.state.code}
            onChange={e => this.setState({ code: e.target.value })}
            style={{ width: "49vw" }}
          />
          <button
            onClick={() => {
              this.props.yapij.exec(this.state.code);
              this.setState({ code: "" });
            }}
          >
            Run
          </button>
        </div>
      </div>
    );
  }
}

export default SimpleCurator;
