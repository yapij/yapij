import React from "react";
import PropTypes from "prop-types";

import { FormGroup, NumericInput, Switch } from "@blueprintjs/core";

class Autocompletion extends React.Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }

  onSelect(v) {
    if (v === "1") {
      this.props.setConfig("reactAce.enableBasicAutocompletion", false);
      this.props.setConfig("reactAce.enableLiveAutocompletion", false);
    } else if (v === "2") {
      this.props.setConfig("reactAce.enableBasicAutocompletion", true);
      this.props.setConfig("reactAce.enableLiveAutocompletion", false);
    } else if (v === "3") {
      this.props.setConfig("reactAce.enableBasicAutocompletion", false);
      this.props.setConfig("reactAce.enableLiveAutocompletion", true);
    }
  }

  render() {
    const { basic, live, setConfig } = this.props;
    var v;
    if (basic) {
      v = "2";
    } else if (live) {
      v = "3";
    } else {
      v = "1";
    }

    return (
      <FormGroup
        label="Autocompletion"
        labor-for="autocomplete"
        helperText="Allow editor to attempt to complete what you are writing."
      >
        <div className="bp3-select bp3-fill">
          <select onChange={e => this.onSelect(e.target.value)} value={v}>
            <option value="1">Off</option>
            <option value="2">Basic</option>
            <option value="3">Live</option>
          </select>
        </div>
      </FormGroup>
    );
  }
}

class Main extends React.Component {
  render() {
    return (
      <div className="yapij-settings">
        {this.props.title ? <h4>{this.props.title}</h4> : null}
        <FormGroup label="Theme" labor-for="theme">
          <Switch
            id="theme"
            label={this.props.config.theme}
            checked={this.props.config.theme === "light"}
            onChange={v =>
              this.props.setConfig(
                "theme",
                this.props.config.theme === "light" ? "dark" : "light"
              )
            }
          />
        </FormGroup>
        <FormGroup
          helperText="In pixels"
          label="Font Size"
          labor-for="font-size"
        >
          <NumericInput
            id="font-size"
            value={this.props.config.reactAce.fontSize}
            onValueChange={(v, s) => this.props.setConfig("fontSize", v)}
          />
        </FormGroup>
        <FormGroup
          label="Numeric Precision"
          labor-for="numeric-precision"
          helperText="Number of decimals to report in numbers."
        >
          <NumericInput
            id="numeric-precision"
            value={this.props.config.prec.prec}
            onValueChange={(v, s) => this.props.setConfig("precision", v)}
          />
        </FormGroup>
        <FormGroup
          label="Tab"
          labor-for="tab-size"
          helperText="Number of spaces in each tab."
        >
          <NumericInput
            id="tab-size"
            value={this.props.config.style.tabSize}
            onValueChange={(v, s) => this.props.setConfig("tabSize", v)}
            majorStepSize={1}
            minorStepSize={1}
            min={1}
          />
        </FormGroup>
        <FormGroup
          label="Wrap Lines"
          labor-for="wrap-lines"
          helperText="Prevent long lines by 'wrapping' them."
        >
          <Switch
            id="wrap-lines"
            label={
              this.props.config.reactSyntaxHighlighter.wrapLines
                ? "Wrapped"
                : "Not Wrapped"
            }
            checked={this.props.config.reactSyntaxHighlighter.wrapLines}
            onChange={v =>
              this.props.setConfig(
                "reactSyntaxHighlighter.wrapLines",
                this.props.config.reactSyntaxHighlighter.wrapLines
                  ? false
                  : true
              )
            }
          />
        </FormGroup>
        <Autocompletion
          basic={this.props.config.reactAce.enableBasicAutocompletion}
          live={this.props.config.reactAce.enableLiveAutocompletion}
          setConfig={this.props.setConfig}
        />
      </div>
    );
  }
}

Main.propTypes = {
  title: PropTypes.string,
  include: PropTypes.array,
  config: PropTypes.object,
  setConfig: PropTypes.func
};

Main.defaultProps = {
  title: "yapij Settings",
  include: null
};

export default Main;
