import React from "react";
import PropTypes from "prop-types";
import { Tabs, Tab } from "@blueprintjs/core";

import Main from "./Main.js";
import EditorSettings from "./EditorSettings.js";

class Settings extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Tabs id="yapij-settings-tabs" defaultSelectedTabId="general">
        {this.props.title ? (
          <div style={{ fontSize: "1.1em", fontWeight: "bold" }}>
            {this.props.title}
          </div>
        ) : null}
        <Tabs.Expander />
        <Tab
          id="general"
          title="General"
          panel={
            <Main
              config={this.props.config}
              setConfig={this.props.setConfig}
              title={null}
            />
          }
        />
        <Tab
          id="editor"
          title="Editor"
          panel={
            <EditorSettings
              config={this.props.config}
              setConfig={this.props.setConfig}
              title={null}
            />
          }
        />
      </Tabs>
    );
  }
}

Settings.propTypes = {
  title: PropTypes.string,
  include: PropTypes.array,
  config: PropTypes.object,
  setConfig: PropTypes.func
};

Settings.defaultProps = {
  title: "yapij Settings",
  include: null
};

export default { Settings, Main, EditorSettings };
