import React from "react";
import PropTypes from "prop-types";

import { FormGroup, NumericInput, Switch } from "@blueprintjs/core";

class EditorSettings extends React.Component {
  render() {
    return (
      <div className="yapij-ace-settings">
        {this.props.title ? <h4>{this.props.title}</h4> : null}
        To Be ADDED at later date.
      </div>
    );
  }
}

EditorSettings.propTypes = {
  title: PropTypes.string,
  include: PropTypes.array,
  config: PropTypes.object,
  setConfig: PropTypes.func
};

EditorSettings.defaultProps = {
  title: "yapij Editor Settings",
  include: null
};

export default EditorSettings;
