import React from "react";
import PropTypes from "prop-types";
import PubSub from "pubsub-js";

import { Dialog } from "primereact/dialog";
import InstallYapij from "../InstallYapij";
import "./GlobalDialogs.scss";

const Installer = ({ isOpen, onHide, ...otherProps }) => (
  <Dialog
    header={
      <span>
        <b>
          <code>yapij</code>
        </b>{" "}
        Installation Settings
      </span>
    }
    visible={isOpen}
    onHide={onHide}
    className="install-yapij-dialog"
    closable={true}
    {...otherProps}
  >
    <InstallYapij />
  </Dialog>
);

export default class GlobalDialogs extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isOpenInstaller: false, isOpenSettings: false };

    this.pubSubKey = props.pubSubKey + "-global-dialogs";

    this.handleCall = this.handleCall.bind(this);
  }

  componentDidMount() {
    PubSub.subscribe(this.pubSubKey, (topic, msg) =>
      this.handleCall(topic, msg)
    );
  }

  componentWillUnmount() {
    PubSub.clearAllSubscriptions();
  }

  handleCall(topic, msg) {
    switch (msg) {
      case "installer":
        this.setState({ isOpenInstaller: !this.state.isOpenInstaller });
        break;
      case "settings":
        this.setState({ isOpenSettings: !this.state.isOpenSettings });
        break;
    }
  }

  render() {
    const { include, ...otherProps } = this.props;
    return (
      <div className="yapij-global-dialogs">
        {include.installer ? (
          <Installer
            onHide={() => this.setState({ isOpenInstaller: false })}
            isOpen={this.state.isOpenInstaller}
          />
        ) : null}
      </div>
    );
  }
}

GlobalDialogs.propTypes = {
  include: PropTypes.exact({
    settings: PropTypes.bool.isRequired,
    installer: PropTypes.bool.isRequired,
  }),
};

GlobalDialogs.defaultProps = {};
