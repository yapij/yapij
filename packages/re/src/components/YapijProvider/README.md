# YapijProvider

## Notes:

- The component re-renders selectively:
  - It will re-render if one of the following are changed:
    - A new registered collector (i.e. `this.state.registeredCollectors` has changed)
    - A non-standard prop changes.
  - This means that ad hoc updates to, e.g., `workspaceDialogs` or servers will not lead to re-renders.
- Servers cannot be specified/renamed on the fly.
  - The servers specified when the first `YapijProvider` component mounts for the first time are the only servers that will be initiated.
  - From there on out YapijProvider components reference servers that are already saved in the redux store.
  - Example: If you specify `YapijProvider` elements in two parts of the app then these providers will reference the same servers. To get around this, place only one `YapijProvider` element at the top of your app (like react-redux's `Provider`).
