import React from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import PubSub from "pubsub-js";

import Server from "../../server";
import { checkForDuplicates } from "../../utils";
import WorkspaceDialogs, {
  Default as DefaultWorkspaceDialog,
} from "../WorkspaceDialogs";
import GlobalDialogs from "./GlobalDialogs.js";

class YapijProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      registeredCollectors: {},
      workspaceDialogs: [],
    };

    this.handleServerCalls = this.handleServerCalls.bind(this);
    this.initServer = this.initServer.bind(this);
  }

  componentDidMount() {
    this.initServer();
    PubSub.subscribe(this.props.pubSubKey, (topic, msg) =>
      this.handleServerCalls(msg)
    );
  }

  componentWillUnmount() {
    PubSub.clearAllSubscriptions();
    this.props.servers.map(s => s.stop());
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      JSON.stringify(nextState.registeredCollectors) !==
      JSON.stringify(this.state.registeredCollectors)
    ) {
      return true;
    } else if (
      this.state.workspaceDialogs.length === 0 &&
      nextState.workspaceDialogs.length > 0
    ) {
      return true;
    } else {
      var {
        serverNames,
        serverActions,
        yapijConfig,
        setServers,
        workspaceDialogs,
        pubSubKey,
        children,
        servers,
        connectWorkspaceDialog,
        ...others
      } = this.props;
      var {
        serverNames,
        serverActions,
        yapijConfig,
        setServers,
        workspaceDialogs,
        pubSubKey,
        children,
        servers,
        connectWorkspaceDialog,
        ...nextOthers
      } = nextProps;

      if (JSON.stringify(others) !== JSON.stringify(nextOthers)) {
        return true;
      }
    }
    return false;
  }

  initServer() {
    if (
      this.props.servers.size !== 0 ||
      this.state.workspaceDialogs.length > 0
    ) {
      return null;
    }

    const {
      serverNames,
      serverActions,
      yapijConfig,
      setServers,
      workspaceDialogs,
      pubSubKey,
      children,
      servers,
      connectWorkspaceDialog,
      ...others
    } = this.props;

    var names = serverNames instanceof Array ? serverNames : [serverNames];
    // Check to make sure all A-OK
    if (!checkForDuplicates(names)) {
      throw new Error(`Duplicate names found in servers prop.`);
    }

    // Create Servers and corresponding WorkspaceDialog components
    var s = {},
      rc = {};
    var wd = Object.assign(DefaultWorkspaceDialog, workspaceDialogs);

    var dialogs = names.map((nm, ii) => {
      s[nm] = new Server(nm, serverActions, yapijConfig);
      rc[nm] = {};
      let CWD = connectWorkspaceDialog(nm, wd)(WorkspaceDialogs);
      return <CWD key={`cwd-${ii}`} />;
    });

    // (A redux method that adds these to the redux state)
    setServers(s);
    // Local state
    this.setState({ registeredCollectors: rc, workspaceDialogs: dialogs });
  }

  handleServerCalls({ method, serverName, collectorName, args, callback, e }) {
    var out,
      server = this.props.servers.get(serverName);
    if (!server) {
      throw new Error(`Invalid server name "${serverName}" given.`);
    }

    switch (method) {
      case "exec":
        out = server.exec(args.cmd, args.quiet);
        break;
      case "interrupt":
        out = server.interrupt();
        break;
      case "start":
        out = server.start();
        break;
      case "stop":
        out = server.stop();
        break;
      case "restart":
        out = server.restart();
        break;
      case "ready":
        out = server.ready(args.verbose);
        break;
      case "register":
        out = new Promise((resolve, reject) => {
          this.setState(
            {
              registeredCollectors: update(this.state.registeredCollectors, {
                $set: { [collectorName]: {} },
              }),
            },
            () => {
              resolve(true);
            }
          );
        });
        break;
      case "on":
        out = new Promise((resolve, reject) => {
          if (
            !(
              args.on in
              ["exec", "interrupt", "start", "stop", "restart", "ready"]
            )
          ) {
            reject(
              'Invalid "on" argument. Must be one of ["exec", "interrupt", "start", "stop", "restart", "ready"]'
            );
          }

          this.setState(
            {
              registeredCollectors: update(this.state.registeredCollectors, {
                [collectorName]: {
                  $set: { [args.on]: args.callback },
                },
              }),
            },
            () => {
              resolve(true);
            }
          );
        });
        break;
      default:
        out = new Promise((resolve, reject) => {
          reject("Invalid method name");
        });
        break;
    }

    // Handle callbacks
    callback = typeof callback === "function" ? callback : null;
    if (
      !callback &&
      this.state.registeredCollectors[collectorName] &&
      this.state.registeredCollectors[collectorName][method]
    ) {
      callback = this.state.registeredCollectors[collectorName][method];
    }

    if (callback) {
      out.then(v => callback(v, null)).catch(e => callback(null, e));
    }
  }

  render() {
    const {
      workspaceDialogs,
      children,
      serverNames,
      yapijConfig,
      serverActions,
      servers,
      setServers,
      pubSubKey,
      connectWorkspaceDialog,
      include,
      ...otherProps
    } = this.props;

    return (
      <div className="yapij-provider" {...otherProps}>
        <div>{children}</div>
        {this.state.workspaceDialogs}
        <GlobalDialogs include={include} pubSubKey={pubSubKey} />
      </div>
    );
  }
}

YapijProvider.propTypes = {
  serverNames: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.string,
  ]),
  connectWorkspaceDialog: PropTypes.func.isRequired,
  yapijConfig: PropTypes.objectOf(PropTypes.object),
  workspaceDialogs: PropTypes.exact({
    SaveAs: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Save: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Load: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    New: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Rename: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Delete: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    Copy: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    CD: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
  }).isRequired,
  serverActions: PropTypes.objectOf(PropTypes.func).isRequired,
  setServers: PropTypes.func.isRequired,
  servers: PropTypes.object.isRequired,
  pubSubKey: PropTypes.string.isRequired,
  include: PropTypes.exact({
    settings: PropTypes.bool.isRequired,
    installer: PropTypes.bool.isRequired,
  }),
};

YapijProvider.defaultProps = {
  serverNames: ["default"],
  yapijConfig: { pythonArgs: null, config: { python: null, spawn: null } },
  workspaceDialogs: DefaultWorkspaceDialog,
  include: { settings: true, installer: true },
};

export default YapijProvider;
