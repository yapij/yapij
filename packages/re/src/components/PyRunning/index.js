import React from "react";
import PropTypes from "prop-types";

import "./index.scss";
import PythonIcon from "./PythonIcon.js";

export default class PyRunning extends React.Component {
  shouldComponentUpdate(nextProps) {
    return (
      this.props.running !== nextProps.running ||
      this.props.started !== nextProps.started ||
      this.props.size !== nextProps.size ||
      this.props.theme !== nextProps.theme
    );
  }

  render() {
    const { size, running, started, theme, ...otherProps } = this.props;

    var color,
      runningColor,
      s = "";
    if (theme === "dark") {
      runningColor = "#e7c547";
      if (started) {
        s += " started";
        if (running) {
          s += " running";
          color = "#7aa6da";
        } else {
          color = "#81a2be";
        }
      } else {
        color = "#969896";
      }
    } else {
      runningColor = "#eab700";
      if (started) {
        s += " started";
        if (running) {
          s += " running";
          color = "#4271ae";
        } else {
          color = "#81a2be";
        }
      } else {
        color = "#8e908c";
      }
    }

    return (
      <PythonIcon
        color={color}
        runningColor={runningColor}
        size={size}
        running={running * started}
      />
    );
  }
}

PyRunning.propTypes = {
  size: PropTypes.number.isRequired,
  running: PropTypes.bool.isRequired,
  started: PropTypes.bool.isRequired,
  theme: PropTypes.oneOf(["light", "dark"]).isRequired,
};

PyRunning.defaultProps = {
  size: 12,
  running: false,
  started: false,
  theme: "dark",
};
