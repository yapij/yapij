import React from "react";
import PropTypes from "prop-types";

export default class EditorTab extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      className,
      onSelect,
      onDelete,
      id,
      name,
      unsaved,
      ...otherProps
    } = this.props;

    return (
      <div
        className={`editor-tab ${className}`}
        onClick={e => {
          e.stopPropagation();
          onSelect(id);
        }}
        {...otherProps}
      >
        {name}
        {unsaved ? "*" : " "}
        <span
          className="icon fas fa-times"
          onClick={e => {
            e.stopPropagation();
            onDelete(id);
          }}
        />
      </div>
    );
  }
}

EditorTab.propTypes = {
  name: PropTypes.string.isRequired,
  unsaved: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  className: PropTypes.string,
};

EditorTab.defaultProps = {
  className: "",
};
