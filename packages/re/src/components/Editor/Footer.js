import React from "react";
import PropTypes from "prop-types";

export default class Footer extends React.Component {
  constructor(props) {
    super(props);

    this.fmtDate = this.fmtDate.bind(this);
  }

  fmtDate(d) {
    if (!d) {
      return "unsaved";
    } else if (d.toLocaleDateString() !== new Date().toLocaleDateString()) {
      return d.toLocalTimeString();
    } else {
      return d.toLocaleDateString();
    }
  }

  render() {
    const { config, file, ...otherProps } = this.props;

    const d = this.fmtDate(file.lastSaved);

    return (
      <div className={`footer ${config.theme}`}>
        <div className="value">{`${file.name} (saved @ ${d})`}</div>
      </div>
    );
  }
}

Footer.propTypes = {
  file: PropTypes.object.isRequired,
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.string])
  ).isRequired,
};

Footer.defaultProps = {};
