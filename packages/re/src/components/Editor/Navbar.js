import React from "react";
import PropTypes from "prop-types";

import EditorTab from "./EditorTab.js";

export default class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      tabs,
      onTabSelect,
      onTabDelete,
      onTabNew,
      activeTab,
      config,
      ...otherProps
    } = this.props;

    return (
      <div className={`navbar ${config.theme}`} onClick={onTabNew}>
        <div
          className="menu"
          onClick={e => {
            e.stopPropagation();
            console.log("Will open menu");
          }}
        >
          <span className="icon fas fa-bars" />
        </div>
        <div className="tabs">
          {Object.keys(tabs).map((t, ii) => {
            const cn = ` ${activeTab === tabs[t].id ? "active" : ""} ${
              config.theme
            }`;
            return (
              <EditorTab
                id={tabs[t].id}
                name={tabs[t].name}
                unsaved={tabs[t].unsaved}
                onSelect={onTabSelect}
                onDelete={onTabDelete}
                className={cn}
                key={`ek-${ii}`}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

Navbar.propTypes = {
  tabs: PropTypes.object.isRequired,
  activeTab: PropTypes.string.isRequired,
  onTabSelect: PropTypes.func.isRequired,
  onTabDelete: PropTypes.func.isRequired,
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.string])
  ).isRequired,
};

Navbar.defaultProps = {};
