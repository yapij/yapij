import React from "react";
import PropTypes from "prop-types";

import { AceEditor } from "../../common/syntaxHighlighting";

export default class EditorPanel extends React.Component {
  constructor(props) {
    super(props);

    const {
      config,
      id,
      value,
      onChange,
      onSelectionChange,
      onRun,
      onRunLine,
      onSave,
      onSaveAs,
      onOpen,
      onClose,
      onFontSizeIncrease,
      onFontSizeDecrease,
      onNewFile,
      dims,
      ...otherProps
    } = props;

    this.commands = [
      {
        name: "RunScript",
        bindKey: { win: "f5", mac: "f5" },
        exec: e => onRun(e),
      },
      {
        name: "RunLine",
        bindKey: { win: "f9", mac: "f9" },
        exec: e => onRunLine(e),
      },
      {
        name: "Open",
        bindKey: { win: "Ctrl-o", mac: "Command-o" },
        exec: e => onOpen(e),
      },
      {
        name: "SaveAs",
        bindKey: { win: "Ctrl-shift-s", mac: "Command-shift-s" },
        exec: e => onSaveAs(e),
      },
      {
        name: "Save",
        bindKey: { win: "Ctrl-s", mac: "Command-s" },
        exec: e => onSave(e),
      },
      {
        name: "Close",
        bindKey: { win: "Ctrl-shift-w", mac: "Command-shift-w" },
        exec: e => onClose(e),
      },
      {
        name: "FontSizeIncrease",
        bindKey: { win: "Ctrl-=", mac: "Command-=" },
        exec: e => onFontSizeIncrease(e),
      },
      {
        name: "FontSizeDecrease",
        bindKey: { win: "Ctrl--", mac: "Command--" },
        exec: e => onFontSizeDecrease(e),
      },
      {
        name: "New",
        bindKey: { win: "Ctrl-n", mac: "Command-n" },
        exec: e => onNewFile(),
      },
    ];
  }

  render() {
    const {
      config,
      id,
      value,
      onChange,
      onSelectionChange,
      onRun,
      onRunLine,
      onSave,
      onSaveAs,
      onOpen,
      onClose,
      onFontSizeIncrease,
      onFontSizeDecrease,
      onNewFile,
      dims,
      ...otherProps
    } = this.props;

    return (
      <AceEditor
        mode="python"
        name={id}
        height={dims.height}
        width={dims.width}
        onChange={onChange}
        onSelectionChange={onSelectionChange}
        value={value}
        commands={this.commands}
        {...config.reactAce}
        editorProps={{ $blockScrolling: Infinity }}
      />
    );
  }
}

EditorPanel.propTypes = {
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.string])
  ).isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onSelectionChange: PropTypes.func.isRequired,
  onRun: PropTypes.func.isRequired,
  onRunLine: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onSaveAs: PropTypes.func.isRequired,
  onOpen: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onFontSizeIncrease: PropTypes.func.isRequired,
  onFontSizeDecrease: PropTypes.func.isRequired,
  onNewFile: PropTypes.func.isRequired,
};

EditorPanel.defaultProps = {};
