import React from "react";
import PropTypes from "prop-types";

import shortid from "shortid";
import { TabMenu } from "primereact/tabmenu";

import EditorPanel from "./EditorPanel.js";

export default class Main extends React.Component {
  constructor(props) {
    super(props);

    let id1 = shortid.generate();
    var m = [
      {
        label: "(untitled)",
        icon: "fas fa-file",
        command: () => this.setState({ activeItem: id1 }),
        id: id1,
      },
      { icon: "fas fa-plus", command: this.onAddPanel },
    ];
    this.state = {
      activeItem: id1,
      model: m,
      panels: {
        [id1]: (
          <EditorPanel
            yapij={props.yapij}
            serverName={props.serverName}
            config={props.config}
          />
        ),
      },
    };
  }

  onNameChange(id, name) {
    console.log("Change name to:", id, name);
  }

  onAddPanel() {}

  render() {
    return (
      <div className="main">{this.state.panels[this.state.activeItem]}</div>
    );
  }
}

Main.propTypes = {
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.string])
  ).isRequired,
  serverName: PropTypes.string.isRequired,
  yapij: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
};

Main.defaultProps = {};
