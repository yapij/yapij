import React from "react";
import PropTypes from "prop-types";

const { dialog } = require("electron").remote;
import FS from "fs";
import Path from "path";
import { homedir } from "os";

import ReactResizeDetector from "react-resize-detector";
import update from "immutability-helper";
import shortid from "shortid";

import Footer from "./Footer.js";
import Navbar from "./Navbar.js";
import EditorPanel from "./EditorPanel.js";

import "./index.scss";

export default class Editor extends React.Component {
  constructor(props) {
    super(props);

    var init = this.createBlankEditor(false);
    this.state = {
      activeItem: init.id,
      tabs: { [init.id]: init.m },
      cursorPosition: { full: false, lastLine: 0, rows: null },
      dims: {
        height: "500px",
        width: "450px",
      },
    };

    this.onTabDelete = this.onTabDelete.bind(this);
    this.createBlankEditor = this.createBlankEditor.bind(this);

    // Running Files
    this.execCommand = this.execCommand.bind(this);
    this.onRun = this.onRun.bind(this);
    this.onRunLine = this.onRunLine.bind(this);

    // File manipulation
    this.onFileChange = this.onFileChange.bind(this);
    this.onFileSelectionChange = this.onFileSelectionChange.bind(this);
    this.onOpen = this.onOpen.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onSaveAs = this.onSaveAs.bind(this);
    this.onClose = this.onClose.bind(this);

    this.onFontSizeChange = this.onFontSizeChange.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  /**
   * Handles deletion of file by clicking "x" above.
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  onTabDelete(id) {
    const tk = Object.keys(this.state.tabs);
    var aid,
      tabs = {};
    tk.map(t => {
      if (this.state.tabs[t].id !== id) {
        tabs[t] = this.state.tabs[t];
      }
    });

    if (this.state.activeItem === id) {
      // Length = 1 => deleted only tab
      if (tk.length === 1) {
        let be = this.createBlankEditor(false);
        aid = be.id;
        tabs[be.id] = be.m;
      } else {
        aid = tabs[tk[0] === id ? tk[1] : tk[0]].id;
      }
    } else {
      aid = this.state.activeItem;
    }

    this.setState({ tabs: tabs, activeItem: aid });
  }

  /**
   * Sets up a new file.
   * @param  {Boolean} setState If true, set the state once this occurs (default)
   * @return {[Object, null]}           [description]
   */
  createBlankEditor(setState = true) {
    let id = shortid.generate();
    let m = {
      name: "untitled",
      id: id,
      lastSaved: null,
      value: "",
      unsaved: false,
      localPath: null,
      workspacePath: null,
    };

    if (setState) {
      this.setState(
        update(this.state, {
          activeItem: { $set: id },
          tabs: {
            $merge: { [id]: m },
          },
        })
      );
      return null;
    } else {
      return { id, m };
    }
  }

  /**
   * Updates string value of file in state.
   * @param  {[type]} v [description]
   * @return {[type]}   [description]
   */
  onFileChange(v) {
    this.setState({
      tabs: update(this.state.tabs, {
        [this.state.activeItem]: {
          value: {
            $set: v,
          },
          unsaved: {
            $set: true,
          },
        },
      }),
    });
  }

  /**
   * Callback for when selection in editor changes. Necessary to track for purposes of running one line of script.
   * @param  {[type]} e [description]
   * @return {[type]}   [description]
   */
  onFileSelectionChange(e) {
    var newCursorPosition,
      updateState = false;
    if (e.lead.row === e.anchor.row) {
      newCursorPosition = {
        cursorPosition: { full: false, lastLine: e.lead.row, rows: null },
      };
    } else {
      newCursorPosition = {
        cursorPosition: {
          full: true,
          lastLine: null,
          rows: [
            Math.min(e.lead.row, e.anchor.row),
            Math.max(e.lead.row, e.anchor.row) + 1,
          ],
        },
      };
    }

    // Add this to prevent unnecessary state updates.
    if (newCursorPosition.full !== this.state.cursorPosition.full) {
      updateState = true;
    } else if (
      newCursorPosition.lastLine !== this.state.cursorPosition.lastLine
    ) {
      updateState = true;
    } else if (
      newCursorPosition.rows[0] !== this.state.cursorPosition.rows[0]
    ) {
      updateState = true;
    } else if (
      newCursorPosition.rows[1] !== this.state.cursorPosition.rows[1]
    ) {
      updateState = true;
    }

    if (updateState) {
      this.setState(newCursorPosition);
    }
  }

  /**
   * Runs a given command. Called internally by `.onRun*` methods.
   * @param  {String} cmd [description]
   * @return {[type]}     [description]
   */
  execCommand(cmd) {
    if (this.props.info.ready) {
      this.props.yapij.exec(cmd);
    } else {
      this.props.yapij.start(() => this.props.yapij.exec(cmd));
    }
  }

  onRun(e) {
    this.execCommand(this.state.tabs[this.state.activeItem].value);
  }

  onRunLine(e) {
    var rows, cmd;

    // Multi-line selection
    if (this.state.cursorPosition.full) {
      rows = this.state.cursorPosition.rows;
    } else {
      // Single-line selection
      if (e.selection.lead.row !== this.state.cursorPosition.lastLine) {
        rows = [e.selection.lead.row, e.selection.lead.row + 1];
      } else if (
        e.selection.anchor.row !== this.state.cursorPosition.lastLine
      ) {
      } else {
        rows = [
          this.state.cursorPosition.lastLine,
          this.state.cursorPosition.lastLine + 1,
        ];
      }
    }

    try {
      cmd = e.selection.lead.document.$lines.slice(rows[0], rows[1]).join("\n");
    } catch (err) {
      cmd = this.state.tabs[this.state.activeItem].value
        .split("\n")
        .slice(rows[0], rows[1])
        .join("\n");
    }

    this.execCommand(cmd);
  }

  onOpen(e) {
    let defaultPath = this.state.tabs[this.state.activeItem].localPath
      ? Path.dirname(this.state.tabs[this.state.activeItem].localPath)
      : null;

    dialog.showOpenDialog(
      null,
      {
        title: "Open File",
        defaultPath: defaultPath,
        buttonLabel: "Open",
        properties: ["openFile", "multiSelections"],
      },
      (filepaths, bookmarks) => {
        var promises = filepaths.map((fp, ii) => {
          return new Promise((resolve, reject) => {
            FS.readFile(fp, {}, (err, data) => {
              if (err) reject(err);
              let p = Path.parse(fp);
              resolve({
                name: p.base,
                id: shortid.generate(),
                lastSaved: new Date(),
                value: data.toString(),
                unsaved: false,
                localPath: p.dir,
                workspacePath: null,
              });
            });
          });
        });

        Promise.all(promises).then(newTabs => {
          let newTabsObject = {},
            lid;
          newTabs.map(nt => {
            newTabsObject[nt.id] = nt;
            lid = nt.id;
          });
          this.setState({
            tabs: { ...this.state.tabs, ...newTabsObject },
            activeItem: lid,
          });
        });
      }
    );
  }

  onSaveAs(e, title = "Save As", callback = null) {
    let file = this.state.tabs[this.state.activeItem];
    let defaultPath = Path.resolve(
      file.localPath ? Path.dirname(file.localPath) : homedir(),
      file.name
    );
    dialog.showSaveDialog(
      null,
      {
        title: title,
        defaultPath: defaultPath,
        buttonLabel: "Save",
      },
      (filename, bookmark) => {
        if (!filename) {
          return null;
        }
        FS.writeFile(filename, file.value, {}, err => {
          if (err) throw err;
          let p = Path.parse(filename);
          this.setState(
            {
              tabs: update(this.state.tabs, {
                [this.state.activeItem]: {
                  lastSaved: {
                    $set: new Date(),
                  },
                  localPath: { $set: p.dir },
                  name: { $set: p.base },
                  unsaved: {
                    $set: false,
                  },
                },
              }),
            },
            state => {
              if (callback) {
                callback(state);
              }
            }
          );
        });
      }
    );
  }

  onSave(e, callback = null) {
    let file = this.state.tabs[this.state.activeItem];

    if (!file.lastSaved) {
      this.onSaveAs(e, "Save", callback);
    } else {
      let filename = Path.resolve(file.localPath, file.name);
      FS.writeFile(filename, file.value, {}, err => {
        if (err) throw err;
        this.setState(
          {
            tabs: update(this.state.tabs, {
              [this.state.activeItem]: {
                lastSaved: {
                  $set: new Date(),
                },
                unsaved: {
                  $set: false,
                },
              },
            }),
          },
          state => {
            if (callback) {
              callback(state);
            }
          }
        );
      });
    }
  }

  onClose(e) {
    let file = this.state.tabs[this.state.activeItem];

    if (file.unsaved) {
      dialog.showMessageBox(
        null,
        {
          type: "warning",
          buttons: ["Yes", "No", "Cancel"],
          title: "Save Changes?",
          message: `${file.name} has been modified, save changes?`,
        },
        (res, c) => {
          switch (res) {
            case 0:
              this.onSave(e, () => this.onTabDelete(file.id));
              break;
            case 1:
              this.onTabDelete(file.id);
              break;
            default:
              return null;
              break;
          }
        }
      );
    } else {
      this.onTabDelete(file.id);
    }
  }

  onFontSizeChange(d) {
    this.props.setConfig(
      "fontSize",
      Math.max(this.props.config.fontSize + d, 1)
    );
  }

  render() {
    const {
      config,
      info,
      serverName,
      setConfig,
      yapij,
      dispatch,
      ...otherProps
    } = this.props;
    const { activeItem, tabs } = this.state;
    const activeTab = tabs[activeItem];

    return (
      <ReactResizeDetector
        handleWidth
        handleHeight
        onResize={(w, h) => {
          this.setState({
            dims: {
              height: `${h - 45}px`,
              width: `${w}px`,
            },
          });
        }}
      >
        <div className="yapij-editor" {...otherProps}>
          <Navbar
            tabs={tabs}
            activeTab={activeItem}
            onTabSelect={id =>
              this.setState({
                activeItem: id,
              })
            }
            onTabDelete={this.onTabDelete}
            onTabNew={this.createBlankEditor}
            config={this.props.config}
          />
          <div className="main">
            <EditorPanel
              config={config}
              id={activeTab.id}
              value={activeTab.value}
              onChange={this.onFileChange}
              onSelectionChange={this.onFileSelectionChange}
              onRun={this.onRun}
              onRunLine={this.onRunLine}
              onSave={this.onSave}
              onSaveAs={this.onSaveAs}
              onOpen={this.onOpen}
              onClose={this.onClose}
              onFontSizeIncrease={e => this.onFontSizeChange(1)}
              onFontSizeDecrease={e => this.onFontSizeChange(-1)}
              onNewFile={this.createBlankEditor}
              dims={this.state.dims}
            />
          </div>
          <Footer file={activeTab} config={config} />
        </div>
      </ReactResizeDetector>
    );
  }
}

Editor.propTypes = {
  config: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.string])
  ).isRequired,
  info: PropTypes.object.isRequired,
  serverName: PropTypes.string.isRequired,
  setConfig: PropTypes.func.isRequired,
  yapij: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.func)])
  ).isRequired,
};

Editor.defaultProps = {};
