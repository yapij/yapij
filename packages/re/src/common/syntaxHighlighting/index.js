import SyntaxHighlighter, {
  registerLanguage,
} from "react-syntax-highlighter/prism-light";
import jsx from "react-syntax-highlighter/languages/prism/jsx";
import bash from "react-syntax-highlighter/languages/prism/bash";

import brace from "brace";
import AceEditor from "react-ace";
import ReactJson from "react-json-view-extended";

import "brace/mode/python";
import "brace/theme/tomorrow_night_bright";
import "brace/theme/tomorrow";
import "brace/ext/searchbox";

import * as rsl from "./rsl-palettes.js";
import python3 from "./python3-lang.js";
import "./ace-editor-hljs-custom.scss";

registerLanguage("python3", python3);
registerLanguage("jsx", jsx);
registerLanguage("bash", bash);

const syntaxThemes = {
  dark: { rsl: rsl.dark, rjv: "monokai", ace: "tomorrow_night_bright" },
  light: { rsl: rsl.light, rjv: "rjv-default", ace: "tomorrow" },
};

const intent = {
  dark: {
    warn: "#e78c45", // rgb(231,140,69),
    danger: "#d54e53", //rgb(213,78,83),
    success: "#b9ca4a", //rgb(185,202,74),
    info: "#7aa6da", //rgb(122,166,218),
    default: "#eaeaea", //rgba(92, 92, 92, 1),
  },
  light: {
    warn: "#f5871f", //rgba(245, 135, 31, 1),
    danger: "#c82829", //rgba(200, 40, 41, 1),
    success: "#718c00", //rgba(113, 140, 0, 1),
    info: "#4271ae", //rgba(66, 113, 174, 1),
    default: "#4d4d4c",
  },
};

const background = { dark: "#1d1f21", light: "#fefefe" };

export {
  AceEditor,
  SyntaxHighlighter,
  ReactJson,
  syntaxThemes,
  background,
  intent,
};
