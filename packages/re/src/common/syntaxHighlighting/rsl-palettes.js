export const dark = {
    'code[class*="language-"]': {
        fontFamily: "monospace",
        textAlign: "left",
        whiteSpace: "pre",
        wordSpacing: "normal",
        wordBreak: "normal",
        wordWrap: "normal",
        lineHeight: "1.25",
        MozTabSize: "2",
        OTabSize: "2",
        tabSize: "2",
        WebkitHyphens: "none",
        MozHyphens: "none",
        msHyphens: "none",
        hyphens: "none",
        background: "none",
        color: "#eaeaea"
    },
    'pre[class*="language-"]': {
        fontFamily: "monospace",
        textAlign: "left",
        whiteSpace: "pre",
        wordSpacing: "normal",
        wordBreak: "normal",
        wordWrap: "normal",
        lineHeight: "1.25",
        MozTabSize: "2",
        OTabSize: "2",
        tabSize: "2",
        WebkitHyphens: "none",
        MozHyphens: "none",
        msHyphens: "none",
        hyphens: "none",
        padding: "0",
        margin: "0",
        overflow: "auto",
        background: "none"
    },
    ':not(pre) > code[class*="language-"]': {
        padding: "0em",
        borderRadius: "0em",
        boxShadow: "none",
        whiteSpace: "normal"
    },
    comment: {
        color: "#aaa"
    },
    prolog: {
        color: "#aaa"
    },
    doctype: {
        color: "#aaa"
    },
    cdata: {
        color: "#aaa"
    },
    punctuation: {
        color: "#999"
    },
    ".namespace": {
        Opacity: ".7"
    },
    property: {
        color: "#0cf"
    },
    tag: {
        color: "#0cf"
    },
    boolean: {
        color: "red",
        fontStyle: "italic"
    },
    number: {
        color: "red"
    },
    constant: {
        color: "#0cf"
    },
    symbol: {
        color: "#0cf"
    },
    selector: {
        color: "yellow"
    },
    "attr-name": {
        color: "yellow"
    },
    string: {
        color: "yellow"
    },
    char: {
        color: "yellow"
    },
    builtin: {
        color: "yellow"
    },
    operator: {
        color: "yellowgreen"
    },
    entity: {
        color: "yellowgreen",
        cursor: "help"
    },
    url: {
        color: "yellowgreen"
    },
    ".language-css .token.string": {
        color: "yellowgreen"
    },
    ".toke.variable": {
        color: "yellowgreen"
    },
    inserted: {
        color: "yellowgreen"
    },
    atrule: {
        color: "deeppink"
    },
    "attr-value": {
        color: "deeppink"
    },
    keyword: {
        color: "deeppink"
    },
    regex: {
        color: "orange"
    },
    important: {
        color: "orange",
        fontWeight: "bold"
    },
    bold: {
        fontWeight: "bold"
    },
    italic: {
        fontStyle: "italic"
    },
    deleted: {
        color: "red"
    }
};

export const light = {
    'code[class*="language-"]': {
        color: "#4d4d4c",
        background: "none",
        textAlign: "left",
        whiteSpace: "pre",
        wordSpacing: "normal",
        wordBreak: "normal",
        wordWrap: "normal",
        lineHeight: "1.25",
        MozTabSize: "2",
        OTabSize: "2",
        tabSize: "2",
        WebkitHyphens: "none",
        MozHyphens: "none",
        msHyphens: "none",
        hyphens: "none"
    },
    'pre[class*="language-"]': {
        color: "#4d4d4c",
        background: "none",
        textAlign: "left",
        whiteSpace: "pre",
        wordSpacing: "normal",
        wordBreak: "normal",
        wordWrap: "normal",
        lineHeight: "1.25",
        MozTabSize: "2",
        OTabSize: "2",
        tabSize: "2",
        WebkitHyphens: "none",
        MozHyphens: "none",
        msHyphens: "none",
        hyphens: "none",
        padding: "0em",
        margin: " 0",
        overflow: "visible"
    },
    ':not(pre) > code[class*="language-"]': {
        background: "none",
        padding: "0em",
        borderRadius: ".3em",
        whiteSpace: "normal"
    },
    comment: {
        color: "#8e908c"
    },
    "block-comment": {
        color: "#8e908c"
    },
    prolog: {
        color: "#999"
    },
    doctype: {
        color: "#8e908c"
    },
    cdata: {
        color: "#999"
    },
    punctuation: {
        color: "#40442d"
    },
    tag: {
        color: "#e2777a"
    },
    "attr-name": {
        color: "#e2777a"
    },
    namespace: {
        color: "#e2777a"
    },
    deleted: {
        color: "#e2777a"
    },
    "function-name": {
        color: "#3e999f"
    },
    boolean: {
        color: "#c82829",
        fontStyle: "italic"
    },
    number: {
        color: "#f5871f"
    },
    function: {
        color: "#8959a8"
    },
    property: {
        color: "#4271ae",
        fontStyle: "italic"
    },
    "class-name": {
        color: "#f99157"
    },
    constant: {
        color: "#3e999f"
    },
    symbol: {
        color: "#c82829"
    },
    selector: {
        color: "#8959a8"
    },
    important: {
        color: "#cc99cd",
        fontWeight: "bold"
    },
    atrule: {
        color: "#cc99cd"
    },
    keyword: {
        color: "#4271ae",
        fontStyle: "italic"
    },
    builtin: {
        color: "#cc99cc"
    },
    string: {
        color: "#718c00"
    },
    char: {
        color: "#718c00"
    },
    "attr-value": {
        color: "#4d4d4c"
    },
    regex: {
        color: "#cc99cc"
    },
    variable: {
        color: "#4d4d4c"
    },
    operator: {
        color: "#c82829"
    },
    entity: {
        color: "#67cdcc",
        cursor: "help"
    },
    url: {
        color: "#99cc99"
    },
    bold: {
        fontWeight: "bold"
    },
    italic: {
        fontStyle: "italic"
    },
    inserted: {
        color: "green"
    }
};
