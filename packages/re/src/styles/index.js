// @hack Make these scss rather than css (copied from node_modules) in order to do easy import.
import "./normalize.scss";
import "./blueprint.scss";

import "./primereact.scss";
import "./primereact-yapij.scss";
import "./primeicons.scss";

import '../../node_modules/@fortawesome/fontawesome-free/scss/fontawesome.scss'
import '../../node_modules/@fortawesome/fontawesome-free/scss/solid.scss'
import '../../node_modules/@fortawesome/fontawesome-free/scss/regular.scss'
import '../../node_modules/@fortawesome/fontawesome-free/scss/brands.scss'

import "./blueprint-overwrite.scss";
import "./colors.scss";
import "./index.scss";
