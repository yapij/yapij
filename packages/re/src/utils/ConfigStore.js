"use strict";
const ConfigStore_ = require("configstore");

class Configstore extends ConfigStore_ {
  constructor(id, defaults, options = {}) {
    super(id, defaults, options);

    if (defaults) {
      if (options.reset) {
        this.reset();
      }
    }
  }

  reset() {
    this.all = this.defaults;
  }
}

module.exports = Configstore;
