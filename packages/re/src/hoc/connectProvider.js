import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";
import { createSelector } from "reselect";

import { getDisplayName, checkForDuplicates } from "../utils";
import { yapijActions } from "../redux";
import connectWorkspaceDialog_ from "./connectWorkspaceDialog.js";

export default function setKeys(reduxKey, pubSubKey) {
  const connectWorkspaceDialog = connectWorkspaceDialog_(reduxKey, pubSubKey);

  function connectInner(
    name,
    propNames = {
      servers: "servers",
    }
  ) {
    const mapDispatchToProps = dispatch => {
      return {
        serverActions: {
          createNewServer: (serverName, server) =>
            dispatch(yapijActions.createNewServer(serverName, server)),
          startKernel: server => dispatch(yapijActions.startKernel(server)),
          stopKernel: server => dispatch(yapijActions.stopKernel(server)),
          restartKernel: server => dispatch(yapijActions.restartKernel(server)),
          createNewCmd: (server, cmd) =>
            dispatch(yapijActions.createNewCmd(server, cmd)),
          appendOutputCmd: (name, msg) =>
            dispatch(yapijActions.appendOutputCmd(name, msg)),
          appendStatusCmd: (server, out) =>
            dispatch(yapijActions.appendStatusCmd(server, out)),
          createServerMsg: (server, msg, intent) =>
            dispatch(yapijActions.createServerMsg(server, msg, intent)),
          setConfig: (key, value) =>
            dispatch(yapijActions.setConfig(key, value)),
          setInfo: (server, value) =>
            dispatch(yapijActions.setInfo(server, value)),
          removeAllServerMsg: () => dispatch(yapijActions.removeAllServerMsg()),
          removeOneServerMsg: key =>
            dispatch(yapijActions.removeOneServerMsg(key)),
        },
        setServers: s => dispatch(yapijActions.setServers(s)),
      };
    };

    const mapStateToProps = state => {
      return {
        [propNames.servers]: state[reduxKey]["servers"],
      };
    };

    function connectFactory(WrappedComponent) {
      class WrappedComponent2 extends React.Component {
        render() {
          return (
            <React.Fragment>
              <WrappedComponent
                {...this.props}
                pubSubKey={pubSubKey}
                connectWorkspaceDialog={connectWorkspaceDialog}
              />
              {}
            </React.Fragment>
          );
        }
      }
      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      WrappedComponent2.displayName = `ConnectedYapijProvider(${getDisplayName(
        WrappedComponent
      )})`;

      return connect(
        mapStateToProps,
        mapDispatchToProps
      )(WrappedComponent2);
    }

    return connectFactory;
  }

  return connectInner;
}
