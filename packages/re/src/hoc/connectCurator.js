import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";
import { createSelector } from "reselect";

import {
  getDisplayName,
  yapijReduxKey,
  yapijPubSubKey,
  yapijCuratorPropNames,
} from "../utils";

import connectCollection_ from "./connectCollection.js";
import connectInfo_ from "./connectInfo.js";
import connectYapij_ from "./connectYapij.js";
import connectConfig_ from "./connectConfig.js";
import connectMsg_ from "./connectMsg.js";

export default function setKeys(reduxKey, pubSubKey) {
  const connectCollection = connectCollection_(reduxKey, pubSubKey),
    connectInfo = connectInfo_(reduxKey, pubSubKey),
    connectConfig = connectConfig_(reduxKey, pubSubKey),
    connectMsg = connectMsg_(reduxKey, pubSubKey),
    connectYapij = connectYapij_(reduxKey, pubSubKey);

  function connectInner(
    name = "default",
    propNames = {
      yapij: "yapij",
      collection: "collection",
      config: "config",
      info: "info",
      msgs: "msgs",
    },
    includeQuiet = false
  ) {
    name = name || "default";
    var m = [],
      c = [];

    if (propNames.info) {
      c.push(connectInfo(name, propNames.info, false));
      m.push("ConnectedInfo");
    }
    if (propNames.msgs) {
      c.push(connectMsg(name, propNames.msgs, includeQuiet, false));
      m.push("ConnectedMsg");
    }
    if (propNames.config || propNames.configSetter) {
      c.push(
        connectConfig(
          { config: propNames.config, setter: propNames.configSetter },
          false
        )
      );
      m.push("ConnectedConfig");
    }
    if (propNames.collection) {
      c.push(connectCollection(name, propNames.collection, false));
      m.push("ConnectedCollection");
    }
    if (propNames.yapij) {
      c.push(connectYapij(name, propNames.yapij, false));
      m.push("ConnectedYapij");
    }

    function connectCuratorFactory(WrappedComponent) {
      var ConnectedCurator = WrappedComponent;
      for (let ii = 0; ii < c.length; ii++) {
        ConnectedCurator = c[ii](ConnectedCurator);
      }

      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      ConnectedCurator.displayName = `ConnectedCurator[${m.join(
        ","
      )}](${getDisplayName(WrappedComponent)})`;

      return ConnectedCurator;
    }

    return connectCuratorFactory;
  }
  return connectInner;
}
