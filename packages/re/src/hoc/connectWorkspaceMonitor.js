import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";
import { createSelector } from "reselect";

import { getDisplayName } from "../utils";
import connectYapij_ from "./connectYapij.js";
import connectInfo_ from "./connectInfo.js";

const emptyInfo = {
  date: new Date("1-1-1900"),
  out: [
    {
      env: [],
      metadata: {},
      stats: { total_size: null, n_objects: null },
    },
  ],
};

export default function setKeys(reduxKey, pubSubKey) {
  const connectYapij = connectYapij_(reduxKey, pubSubKey),
    connectInfo = connectInfo_(reduxKey, pubSubKey);

  function connectInner(
    name = "default",
    propNames = { workspace: "workspace", info: "info", yapij: "yapij" },
    wrapDisplayName = false
  ) {
    const ci_ = connectInfo(name, propNames.info, false);
    const cy_ = connectYapij(name, propNames.yapij, false);

    function connectYapijWorkspaceMonitorFactory(WrappedComponent) {
      class ConnectedWorkspaceMonitor extends React.Component {
        constructor(props) {
          super(props);
        }

        render() {
          return <WrappedComponent {...this.props} servername={name} />;
        }
      }

      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      if (wrapDisplayName) {
        ConnectedWorkspaceMonitor.displayName = `ConnectedWorkspaceMonitor(${getDisplayName(
          WrappedComponent
        )})`;
      }

      return ci_(cy_(ConnectedWorkspaceMonitor));
    }

    return connectYapijWorkspaceMonitorFactory;
  }
  return connectInner;
}
