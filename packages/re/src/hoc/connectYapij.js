import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";

import { getDisplayName } from "../utils";

export default function setKeys(reduxKey, pubSubKey) {
  function connectInner(
    name = "default",
    propName = "yapij",
    wrapDisplayName = false
  ) {
    function connectYapijFactory(WrappedComponent) {
      const dialogKey = [pubSubKey, "dialog"].join("-");
      const globalDialogKey = [pubSubKey, "global", "dialogs"].join("-");

      class ConnectedYapij extends React.Component {
        constructor(props) {
          super(props);

          this.uid = shortid.generate();
          this.pub = this.pub.bind(this);
          this.pubToWorkspace = this.pubToWorkspace.bind(this);
          this.pubToGlobalDialogs = this.pubToGlobalDialogs.bind(this);

          this.yapij = {
            [propName]: {
              start: callback => this.pub("start", null, callback),
              stop: callback => this.pub("stop", null, callback),
              restart: callback => this.pub("restart", null, callback),
              ready: (verbose, callback) =>
                this.pub("ready", {
                  verbose,
                }),
              exec: (cmd, quiet, callback) =>
                this.pub("exec", { cmd, quiet }, callback),
              interrupt: callback => this.pub("interrupt", null, callback),
              on: (method, onCallback, callback) =>
                this.pub("on", { on: method, callback: onCallback }, callback),
              workspace: {
                saveAs: filename =>
                  this.pubToWorkspace("SaveAs", { filename: filename }),
                save: filename =>
                  this.pubToWorkspace("Save", { filename: filename }),
                load: filename =>
                  this.pubToWorkspace("Load", { filename: filename }),
                new: filename =>
                  this.pubToWorkspace("New", { filename: filename }),
                rename: filename =>
                  this.pubToWorkspace("Rename", { filename: filename }),
                delete: filename =>
                  this.pubToWorkspace("Delete", { filename: filename }),
                copy: filename =>
                  this.pubToWorkspace("Copy", { filename: filename }),
                cd: cd => this.pubToWorkspace("CD", { cd: cd }),
              },
              globalDialogs: {
                installer: () => this.pubToGlobalDialogs("installer"),
                settings: () => this.pubToGlobalDialogs("settings"),
              },
            },
          };
        }

        componentDidMount() {
          this.pub("register", null, null);
        }

        pub(method, args = null, callback = null) {
          PubSub.publish(pubSubKey, {
            serverName: name,
            collectorName: this.uid,
            method: method,
            args: args,
            callback: callback,
          });
        }

        pubToWorkspace(method, args = null) {
          PubSub.publish(dialogKey, {
            serverName: name,
            collectorName: this.uid,
            method: method,
            args: args,
          });
        }

        pubToGlobalDialogs(method) {
          PubSub.publish(globalDialogKey, method);
        }

        render() {
          return <WrappedComponent {...this.props} {...this.yapij} />;
        }
      }

      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      if (wrapDisplayName) {
        ConnectedYapij.displayName = `ConnectedYapij(${getDisplayName(
          WrappedComponent
        )})`;
      }

      return ConnectedYapij;
    }

    return connectYapijFactory;
  }
  return connectInner;
}
