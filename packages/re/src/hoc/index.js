import connectBeacon from "./connectBeacon.js";
import connectCollection from "./connectCollection.js";
import connectConfig from "./connectConfig.js";
import connectCurator from "./connectCurator.js";
import connectInfo from "./connectInfo.js";
import connectMsg from "./connectMsg.js";
import connectProvider from "./connectProvider.js";
import connectWorkspaceMonitor from "./connectWorkspaceMonitor.js";
import connectYapij from "./connectYapij.js";
import connectEditor from "./connectEditor.js";

export default function HOC(redux = "YAPIJ", pubsub = "YAPIJ") {
    return {
        connectBeacon: connectBeacon(redux, pubsub),
        connectCollection: connectCollection(redux, pubsub),
        connectConfig: connectConfig(redux, pubsub),
        connectCurator: connectCurator(redux, pubsub),
        connectInfo: connectInfo(redux, pubsub),
        connectMsg: connectMsg(redux, pubsub),
        connectProvider: connectProvider(redux, pubsub),
        connectWorkspaceMonitor: connectWorkspaceMonitor(redux, pubsub),
        connectYapij: connectYapij(redux, pubsub),
        connectEditor: connectEditor(redux, pubsub),
    };
}
