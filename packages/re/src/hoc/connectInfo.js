import React from "react";
import { connect } from "react-redux";
import shortid from "shortid";
import { createSelector } from "reselect";

import { getDisplayName } from "../utils";

export default function setKeys(reduxKey, pubSubKey) {
  function connectInner(name, propName = "info", wrapDisplayName = false) {
    const getInfo = createSelector(
      state => state[reduxKey]["serverInfo"][name],
      s => s || {}
    );

    const mapStateToProps = state => {
      return { [propName]: getInfo(state) };
    };

    function connectYapijInfoFactory(WrappedComponent) {
      class ConnectedInfo extends React.Component {
        constructor(props) {
          super(props);
        }

        render() {
          return <WrappedComponent {...this.props} />;
        }
      }

      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      if (wrapDisplayName) {
        ConnectedInfo.displayName = `ConnectedInfo(${getDisplayName(
          WrappedComponent
        )})`;
      }

      return connect(
        mapStateToProps,
        null
      )(ConnectedInfo);
    }

    return connectYapijInfoFactory;
  }

  return connectInner;
}
