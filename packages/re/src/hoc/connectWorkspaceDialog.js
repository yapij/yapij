import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";
import { createSelector } from "reselect";

import { getDisplayName, checkForDuplicates } from "../utils";
import { yapijActions } from "../redux";
import connectYapij_ from "./connectYapij.js";

export default function setKeys(reduxKey, pubSubKey) {
  const dialogKey = [pubSubKey, "dialog"].join("-");

  function connectInner(name, workspaceDialogs) {
    const connectYapij = connectYapij_(reduxKey, pubSubKey)(
      name,
      "yapij",
      false
    );

    const mapStateToProps = state => {
      return {
        info: state[reduxKey]["serverInfo"][name],
      };
    };

    function connectFactory(WrappedComponent) {
      const WC = connectYapij(WrappedComponent);
      class WrappedComponent2 extends React.Component {
        render() {
          return (
            <WC
              {...this.props}
              serverName={name}
              pubSubKey={dialogKey}
              workspaceDialogs={workspaceDialogs}
            />
          );
        }
      }
      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      WrappedComponent2.displayName = `ConnectedWorkspaceDialog(${getDisplayName(
        WrappedComponent
      )})`;

      return connect(
        mapStateToProps,
        null
      )(WrappedComponent2);
    }

    return connectFactory;
  }

  return connectInner;
}
