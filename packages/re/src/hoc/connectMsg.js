import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";
import { createSelector } from "reselect";

import { yapijActions } from "../redux";
import { getDisplayName } from "../utils";

export default function setKeys(reduxKey, pubSubKey) {
  function connectInner(
    name = null,
    propName = "msgs",
    wrapDisplayName = false
  ) {
    const __gm_ = state => state[reduxKey]["serverMsg"];

    // Various possibilities for server to report on
    const __getAll = s => s || [];
    const __getOne = s => s.filter(m => m.server === name) || [];
    const __getSome = s => s.filter(m => m.server in name) || [];

    // Get the corrent fetcher
    const __get = name
      ? typeof name === "string"
        ? __getOne
        : __getSome
      : __getAll;

    // Count the number of each type of message
    const __countEm = s => {
      var out = { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0 };
      s.map(si => {
        switch (si.intent) {
          case 0:
            out[0] += 1;
            break;
          case 1:
            out[1] += 1;
            break;
          case 2:
            out[2] += 1;
            break;
          case 3:
            out[3] += 1;
            break;
          case 4:
            out[4] += 1;
            break;
        }
      });
      return out;
    };

    // Combined selector: Gets the right messages then counts them.
    const __getAndCount = s => {
      var s_ = __get(s);

      return {
        [propName]: s_,
        [propName + "Count"]: __countEm(s_),
      };
    };

    // Selector
    const getMsgs = createSelector(__gm_, __getAndCount);

    const mapStateToProps = state => getMsgs(state);

    function connectYapijMsgFactory(WrappedComponent) {
      class ConnectedMsg extends React.Component {
        constructor(props) {
          super(props);
        }

        render() {
          return <WrappedComponent {...this.props} />;
        }
      }

      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      if (wrapDisplayName) {
        ConnectedMsg.displayName = `ConnectedMsg(${getDisplayName(
          WrappedComponent
        )})`;
      }

      return connect(
        mapStateToProps,
        {
          removeOneServerMsg: yapijActions.removeOneServerMsg,
          removeAllServerMsg: yapijActions.removeAllServerMsg,
        }
      )(ConnectedMsg);
    }

    return connectYapijMsgFactory;
  }
  return connectInner;
}
