import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";
import { createSelector } from "reselect";

import { getDisplayName, yapijReduxKey, yapijPubSubKey } from "../utils";

export default function setKeys(reduxKey, pubSubKey) {
  function connectInner(
    name,
    propName = "collection",
    includeQuiet = false,
    wrapDisplayName = false
  ) {
    const getCollection = createSelector(
      state => state[reduxKey]["collections"][name],
      s => {
        var si = s || {};
        if (!includeQuiet) {
          var out = {},
            sik = Object.keys(si);

          for (let ii = 0; ii < sik.length; ii++) {
            if (!si[sik[ii]].quiet) {
              out[sik[ii]] = si[sik[ii]];
            }
          }
          return out;
        } else {
          return si;
        }
      }
    );

    const mapStateToProps = state => {
      return { [propName]: getCollection(state) };
    };

    function connectYapijCollectionFactory(WrappedComponent) {
      class ConnectedCollection extends React.Component {
        constructor(props) {
          super(props);
        }

        render() {
          return <WrappedComponent {...this.props} />;
        }
      }

      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      if (wrapDisplayName) {
        ConnectedCollection.displayName = `ConnectedCollection(${getDisplayName(
          WrappedComponent
        )})`;
      }

      return connect(
        mapStateToProps,
        null
      )(ConnectedCollection);
    }

    return connectYapijCollectionFactory;
  }
  return connectInner;
}
