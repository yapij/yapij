import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";
import { createSelector } from "reselect";

import { getDisplayName } from "../utils";
import connectYapij_ from "./connectYapij.js";
import connectMsg_ from "./connectMsg.js";

export default function setKeys(reduxKey, pubSubKey) {
  const connectYapij = connectYapij_(reduxKey, pubSubKey),
    connectMsg = connectMsg_(reduxKey, pubSubKey);

  function connectInner(
    name = null,
    propNames = { msgs: "msgs", yapij: "yapij" }
  ) {
    function noneFactory(WrappedComponent) {
      var c = WrappedComponent;
      c.displayName = `ConnectedBeacon[Null](${getDisplayName(
        WrappedComponent
      )})`;
      return c;
    }

    function bothFactory(WrappedComponent) {
      const cm = connectMsg(name, propNames.msgs, false),
        cy = connectYapij(propNames.yapij, false);
      var c = cy(cm(WrappedComponent));
      c.displayName = `ConnectedBeacon[ConnectedMsg, ConnectedYapij](${getDisplayName(
        WrappedComponent
      )})`;
      return c;
    }

    function msgFactory(WrappedComponent) {
      var c = connectMsg(name, propNames.msgs, false)(WrappedComponent);
      c.displayName = `ConnectedBeacon[ConnectedMsg](${getDisplayName(
        WrappedComponent
      )})`;
      return c;
    }

    function yapijFactory(WrappedComponent) {
      var c = connectMsg(propNames.yapij, false)(WrappedComponent);
      c.displayName = `ConnectedBeacon[ConnectedYapij](${getDisplayName(
        WrappedComponent
      )})`;
      return c;
    }

    if (propNames.msgs && propNames.yapij) {
      return bothFactory;
    } else if (propNames.msgs) {
      return msgFactory;
    } else if (propNames.yapij) {
      return yapijFactory;
    } else {
      return noneFactory;
    }
  }
  return connectInner;
}
