import React from "react";
import { connect } from "react-redux";
import PubSub from "pubsub-js";
import shortid from "shortid";

import { getDisplayName, yapijReduxKey, yapijPubSubKey } from "../utils";
import { yapijActions } from "../redux";

export default function setKeys(reduxKey, pubSubKey) {
  function connectInner(
    propNames = { config: "config", setter: "setConfig" },
    wrapDisplayName = true
  ) {
    const mapStateToProps = state => {
      return { [propNames.config]: state[reduxKey]["config"] || {} };
    };

    const mapDispatchToProps = propNames.setter
      ? {
          [propNames.setter]: yapijActions.setConfig,
        }
      : null;

    function connectYapijConfigFactory(WrappedComponent) {
      class YapijConfig extends React.Component {
        constructor(props) {
          super(props);
        }

        render() {
          return <WrappedComponent {...this.props} />;
        }
      }

      // Following guidance here: https://reactjs.org/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
      if (wrapDisplayName) {
        YapijConfig.displayName = `YapijConfig(${getDisplayName(
          WrappedComponent
        )})`;
      }

      return connect(
        mapStateToProps,
        mapDispatchToProps
      )(YapijConfig);
    }

    return connectYapijConfigFactory;
  }
  return connectInner;
}
