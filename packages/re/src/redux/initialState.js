import { syntaxThemes, background, intent } from "../common/syntaxHighlighting";
import Immutable from "immutable";

const initialState = {
  servers: Immutable.Map({}),
  serverMsg: [],
  serverInfo: {},
  collections: {},
  config: {
    reactJsonView: {
      iconStyle: "circle",
      indentWidth: 2,
      collapseStringsAfterLength: true,
      enableClipboard: false,
      displayObjectSize: true,
      displayDataTypes: false,
      sortKeys: false,
      validationMessage: "reactJsonView Validation Error",
      theme: syntaxThemes.dark.rjv,
    },
    reactSyntaxHighlighter: {
      showLineNumbers: false,
      theme: syntaxThemes.dark.rsl,
      wrapLines: true,
    },
    reactAce: {
      fontSize: 11,
      highlightActiveLine: false,
      tabSize: 2,
      enableBasicAutocompletion: false,
      enableLiveAutocompletion: false,
      theme: syntaxThemes.dark.ace,
      wrapEnabled: true,
    },
    theme: "dark",
    style: {
      tabSize: 2,
      fontSize: "11px",
      color: "#eaeaea",
      backgroundColor: "rgba(0,0,0,0)",
    },
    fontSize: 11,
    background: background.dark,
    intent: intent.dark,
    prec: {
      prec: 4,
      num: "0,0.[0000]",
      pct: "0,0.[0000]%",
      sgn: "+0,0.[0000]",
      usd: "$0,0.[0000]",
      exp: "0.0000e+0",
    },
  },
};

export default initialState;
