import update from "immutability-helper";
import shortid from "shortid";

import * as topics from "./topics.js";
import initialState from "./initialState.js";

import { syntaxThemes, background, intent } from "../common/syntaxHighlighting";

const createNewServer = (state, server) => {
  if (Object.keys(state.collections).some(s => s === server)) {
    throw new Error("Server names must be unique.");
  } else {
    return update(state, {
      collections: {
        $merge: { [server]: {} },
      },
      serverInfo: {
        $merge: {
          [server]: {
            dateCreated: new Date(),
            active: false,
            ready: false,
            workspace: null,
          },
        },
      },
    });
  }
};

const startKernel = (state, server) => {
  return update(state, {
    collections: {
      $merge: { [server]: {} },
    },
    serverInfo: { [server]: { ready: { $set: true } } },
  });
};

const stopKernel = (state, server) => {
  return update(state, {
    collections: {
      $merge: { [server]: {} },
    },
    serverInfo: { [server]: { ready: { $set: false } } },
  });
};

const restartKernel = (state, server) => {
  return update(state, {
    collections: {
      $merge: { [server]: {} },
    },
    serverInfo: { [server]: { ready: { $set: true } } },
  });
};

const newCmd = (state, server, cmd) => {
  return update(state, {
    collections: {
      [server]: {
        $merge: { [cmd.uid]: cmd },
      },
    },
  });
};

const appendOutputCmd = (state, server, out) => {
  return update(state, {
    collections: {
      [server]: { [out.uid]: { out: { $push: [out] } } },
    },
  });
};

const appendStatusCmd = (state, server, out) => {
  return update(state, {
    collections: {
      [server]: {
        [out.uid]: { status: { $push: [out] } },
      },
    },
  });
};

const addServerMsg = (state, msg) => {
  return update(state, {
    serverMsg: { $push: [msg] },
  });
};

const updateConfigAttr = (state, k, v) => {
  return update(state, {
    config: {
      [k[0]]: {
        $merge: { [k[1]]: v },
      },
    },
  });
};

const updateConfigPrecision = (state, v) => {
  return update(state, {
    config: {
      prec: {
        $set: v,
      },
    },
  });
};

const updateConfigFontSize = (state, v) => {
  return update(state, {
    config: {
      style: {
        fontSize: {
          $set: `${v}px`,
        },
      },
      reactAce: {
        fontSize: {
          $set: v,
        },
      },
      fontSize: { $set: v },
    },
  });
};

const updateConfigTabSize = (state, v) => {
  return update(state, {
    config: {
      style: {
        tabSize: {
          $set: v,
        },
      },
      reactAce: {
        tabSize: {
          $set: v,
        },
      },
      reactJsonView: {
        indentWidth: {
          $set: v,
        },
      },
    },
  });
};

const updateConfigTheme = (state, v) => {
  return update(state, {
    config: {
      theme: {
        $set: v,
      },
      reactAce: {
        theme: {
          $set: syntaxThemes[v].ace,
        },
      },
      reactJsonView: {
        theme: {
          $set: syntaxThemes[v].rjv,
        },
      },
      reactSyntaxHighlighter: {
        theme: {
          $set: syntaxThemes[v].rsl,
        },
      },
      background: { $set: background[v] },
      intent: { $set: intent[v] },
      style: {
        color: { $set: intent[v].default },
      },
    },
  });
};

const updateConfigAll = (state, v) => {
  return update(state, {
    config: {
      $merge: v,
    },
  });
};

const setInfo = (state, server, v) => {
  return update(state, {
    serverInfo: {
      [server]: { $merge: v },
    },
  });
};

const removeAllServerMsg = state => {
  return update(state, { serverMsg: { $set: [] } });
};

const removeOneServerMsg = (state, key) => {
  return update(state, { serverMsg: { $splice: [[key, 1]] } });
};

const setServers = (state, v) => {
  return update(state, { servers: { $set: v } });
};

export default function yapij(state = initialState, action) {
  switch (action.type) {
    case topics.NEW_SERVER:
      return createNewServer(state, action.server);
      break;
    case topics.START_KERNEL:
      return startKernel(state, action.server);
      break;
    case topics.STOP_KERNEL:
      return stopKernel(state, action.server);
      break;
    case topics.RESTART_KERNEL:
      return startKernel(state, action.server);
      break;

    case topics.NEW_CMD:
      return newCmd(state, action.server, action.cmd);
      break;
    case topics.APPEND_OUTPUT_CMD:
      return appendOutputCmd(state, action.server, action.out);
      break;
    case topics.APPEND_STATUS_CMD:
      return appendStatusCmd(state, action.server, action.out);
      break;

    case topics.ADD_SERVER_MSG:
      return addServerMsg(state, action.msg);
      break;
    case topics.REMOVE_ALL_SERVER_MSG:
      return removeAllServerMsg(state);
      break;
    case topics.REMOVE_ONE_SERVER_MSG:
      return removeOneServerMsg(state, action.key);
      break;

    case topics.SET_CONFIG_ATTR:
      return updateConfigAttr(state, action.k, action.v);
      break;
    case topics.SET_CONFIG_FONTSIZE:
      return updateConfigFontSize(state, action.v);
      break;
    case topics.SET_CONFIG_TABSIZE:
      return updateConfigTabSize(state, action.v);
      break;
    case topics.SET_CONFIG_THEME:
      return updateConfigTheme(state, action.v);
      break;
    case topics.SET_CONFIG_PRECISION:
      return updateConfigPrecision(state, action.v);
      break;
    case topics.SET_CONFIG_ALL:
      return updateConfigAll(state, action.v);
      break;

    case topics.SET_INFO:
      return setInfo(state, action.server, action.v);
      break;

    case topics.SET_SERVERS:
      return setServers(state, action.v);
      break;

    default:
      return state;
  }
}
