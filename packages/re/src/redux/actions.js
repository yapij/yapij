import * as topics from "./topics.js";
import Immutable from "immutable";

export function createNewServer(server) {
  return {
    type: topics.NEW_SERVER,
    server: server,
  };
}

export function startKernel(server) {
  return {
    type: topics.START_KERNEL,
    server: server,
  };
}

export function stopKernel(server) {
  return {
    type: topics.STOP_KERNEL,
    server: server,
  };
}

export function restartKernel(server) {
  return {
    type: topics.START_KERNEL,
    server: server,
  };
}

export function createNewCmd(server, cmd) {
  cmd.out = [];
  cmd.status = [];
  return {
    type: topics.NEW_CMD,
    server: server,
    cmd: cmd,
  };
}

export function appendOutputCmd(server, out) {
  // @hack: Don't know how to deal with this nicelt yet.
  if (out.pyType !== "PyEndStr") {
    return {
      type: topics.APPEND_OUTPUT_CMD,
      server: server,
      out: out,
    };
  }
}

export function appendStatusCmd(server, out) {
  return {
    type: topics.APPEND_STATUS_CMD,
    server: server,
    out: out,
  };
}

export function createServerMsg(server, msg, intent = 0) {
  return {
    type: topics.ADD_SERVER_MSG,
    msg: {
      server: server,
      msg: msg,
      intent: intent,
      date: new Date(),
    },
  };
}

export function setConfig(key, value) {
  var k = key.split(".");
  switch (k[0]) {
    case "tabSize":
      var v = parseInt(value);
      if (v !== value) throw new Error("Value must be a positive int");
      if (v < 1) throw new Error("Value must be positive.");
      return { type: topics.SET_CONFIG_TABSIZE, v: v };
    case "theme":
      if (!(value === "dark" || value === "light"))
        throw new Error('Theme must be either "light" or "dark".');
      return { type: topics.SET_CONFIG_THEME, v: value };
      break;
    case "precision":
      var v = parseInt(value);
      if (v !== value) throw new Error("Value must be an int");
      if (v < 1) throw new Error("Value must be positive.");
      let r = "0".repeat(v);
      let v = {
        prec: value,
        num: `0,0.[${r}]`,
        pct: `0,0.[${r}]%`,
        sgn: `+0,0.[${r}]`,
        usd: `$0,0.[${r}]`,
        exp: `0.${r}e+0`,
      };
      return { type: topics.SET_CONFIG_PRECISION, v: v };
      break;
    case "fontSize":
      var v = parseFloat(value);
      if (v !== value) throw new Error("Value must be an float");
      if (v < 1) throw new Error("Value must be positive.");
      return { type: topics.SET_CONFIG_FONTSIZE, v: v };
      break;
    case "all":
      return { type: topics.SET_CONFIG_ALL, v: v };
      break;
    default:
      if (k.length !== 2) throw new Error("Invalid config key");
      return { type: topics.SET_CONFIG_ATTR, k: k, v: value };
  }
}

export function setInfo(server, value) {
  return { type: topics.SET_INFO, server: server, v: value };
}

export function removeAllServerMsg() {
  return { type: topics.REMOVE_ALL_SERVER_MSG };
}

export function removeOneServerMsg(key) {
  return { type: topics.REMOVE_ONE_SERVER_MSG, key: key };
}

export function setServers(servers) {
  return { type: topics.SET_SERVERS, v: Immutable.Map(servers) };
}




