// Kernel Actions
export const NEW_SERVER = "NEW_SERVER";
export const START_KERNEL = "START_KERNEL";
export const STOP_KERNEL = "STOP_KERNEL";
export const RESTART_KERNEL = "RESTART_KERNEL";

// Execution Related
export const NEW_CMD = "NEW_CMD";
export const APPEND_OUTPUT_CMD = "APPEND_OUTPUT_CMD";
export const APPEND_STATUS_CMD = "APPEND_STATUS_CMD";

// Server status
export const ADD_SERVER_MSG = "ADD_SERVER_MSG";
export const REMOVE_ALL_SERVER_MSG = "REMOVE_ALL_SERVER_MSG";
export const REMOVE_ONE_SERVER_MSG = "REMOVE_ONE_SERVER_MSG";

// Updating the config
export const SET_CONFIG_ATTR = "SET_CONFIG_ATTR";
export const SET_CONFIG_FONTSIZE = "SET_CONFIG_FONTSIZE";
export const SET_CONFIG_TABSIZE = "SET_CONFIG_TABSIZE";
export const SET_CONFIG_THEME = "SET_CONFIG_THEME";
export const SET_CONFIG_PRECISION = "SET_CONFIG_PRECISION";
export const SET_CONFIG_ALL = "SET_CONFIG_ALL";

// Updating info
export const SET_INFO = "SET_INFO";

// Updating SERVERS
export const SET_SERVERS = "SET_SERVERS";
