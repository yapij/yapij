import React from "react";
import { connect } from "react-redux";
import shortid from "shortid";

var Server_;
if (process.env.YAPIJ_RE_ENV === "development") {
  Server_ = require("@yapij/js/src/DummyServer/DummyServer.js");
} else {
  Server_ = require("@yapij/js").Server;
}

import { yapijActions } from "../redux";

function unwindNestedYapijPyType(obj) {
  if (obj.pyType) {
    switch (obj.pyType) {
      case "PyDict":
        let out = {};
        Object.keys(obj.value).map(k => {
          out[k] = unwindNestedYapijPyType(obj.value[k]);
        });
        return out;
        break;
      case "PyList":
        return obj.value.map(k => unwindNestedYapijPyType(k));
      case "PySet":
        return obj.value.map(k => unwindNestedYapijPyType(k));
      case "PyTuple":
        return obj.value.map(k => unwindNestedYapijPyType(k));
      default:
        return obj.value;
    }
  } else {
    return obj;
  }
}

export default class YapijServer {
  constructor(name, actions, config) {
    this.name = name;
    this.actions = actions;

    this.uid = shortid.generate();
    this.workspaceId = null;
    this.activeUids = 0;

    this.server = new Server_(config.pythonArgs, config.config, false);

    this.server.on("session.input", (ch, msg) => {
      if (msg.in.startsWith("%workspace_info")) {
        this.workspaceId = msg.uid;
      }
      this.actions.createNewCmd(this.name, msg);
    });
    this.server.on("session.output", (ch, msg) => {
      var msg_ = msg;
      if (
        this.workspaceId &&
        msg.uid === this.workspaceId &&
        msg.pyType === "PyDict"
      ) {
        // @todo: FIX so that can deal with nested values if necessary.
        this.actions.setInfo(this.name, {
          workspace: {
            date: msg.date,
            workspace: unwindNestedYapijPyType(msg),
          },
        });
      }

      // @hack
      if (msg.type !== "end") {
        this.actions.appendOutputCmd(this.name, msg);
      }
    });
    this.server.on("session.status", (ch, msg) => {
      if (msg.codes[0] === 3) {
        this.activeUids -= 1;
        if (this.activeUids <= 0) {
          this.actions.setInfo(this.name, { active: false });

          if (this.activeUids < 0) {
            this.activeUids = 0;
          }
        }

        // Disable workspace id checking
        if (this.workspaceId && msg.uid === this.workspaceId) {
          this.workspaceId = null;
        }
      }

      this.actions.appendStatusCmd(this.name, msg);
    });
    this.server.on("heartbeat.fail", msg => {
      this._serverMsg(msg, 3);
    });
    this.server.on("kernel.close", msg => {
      this._serverMsg("Python kernel closed.", 1);
    });
    this.server.on("kernel.stdout", msg => {
      this._serverMsg(`[STDOUT]\n${msg}`, 1);
    });
    this.server.on("kernel.stderr", msg => {
      this._serverMsg(`[STDERR]\n${msg}`, 3);
    });

    this.actions.createNewServer(this.name);
  }

  start() {
    this._serverMsg("Starting Python kernel...", 1);

    return this.server.start().then(
      () => {
        this.actions.startKernel(
          this.name,
          this.server.workers.session.sessionId
        );
        this._serverMsg("Python kernel started.", 1);
      },
      e => {
        this._serverMsg(`[Failure on python start] ${e}`, 3);
      }
    );
  }

  stop() {
    this._serverMsg("Stopping Python kernel...", 1);
    var sid = this.server.workers.session.sessionId;

    return this.server.stop().then(
      () => {
        this.actions.stopKernel(this.name, sid);
        this._serverMsg("Python kernel stopped.", 1);
        this.activeUids = 0;
      },
      e => {
        this._serverMsg(
          `Python Kernel failed to stop.\nFull error message:\n${e}`,
          3
        );
      }
    );
  }

  restart() {
    this._serverMsg("Restarting Python kernel...", 1);
    if (!this.server.ready) {
      this._serverMsg("Cannot restart: server not started.", 2);
      return new Promise((r, j) => {
        j("Server not started.");
      });
    }

    this.actions.setInfo(this.name, { ready: false });
    this.activeUids = 0;
    var sid = this.server.workers.session.sessionId;

    return this.server.restart().then(
      () => {
        this.actions.restartKernel(
          this.name,
          sid,
          this.server.workers.session.sessionId
        );
        this._serverMsg("Python kernel restarted.", 1);
      },
      e => {
        this._serverMsg(
          `Python Kernel failed to restart.\nFull error message:\n${e}`,
          3
        );
      }
    );
  }

  exec(cmd, quiet) {
    if (!this.server.ready) {
      return new Promise((r, j) => {
        this._serverMsg(`Server not yet started.`, 3);
        j("Server not started.");
      });
    }

    return new Promise((resolve, reject) => {
      try {
        this.actions.setInfo(this.name, { active: true });
        this.activeUids += 1;

        var eo = this.server.exec(cmd, quiet);
        if (cmd.startsWith("%workspace_info")) {
          this.workspaceId = eo.uid;
        }
        resolve(eo);
      } catch (e) {
        this._serverMsg(`${e}`, 3);
        reject(e);
      }
    });
  }

  interrupt() {
    if (!this.server.ready) {
      return new Promise((r, j) => {
        this._serverMsg(`Cannot interrupt: Server not yet started.`, 3);
        j("Cannot interrupt: Server not started.");
      });
    }
    this._serverMsg(`Attempting to interrupt kernel...`, 2);
    return new Promise((resolve, reject) => {
      try {
        resolve(this.server.interrupt());
      } catch (e) {
        reject(e);
      }
    });
  }

  ready(verbose = false) {
    var r = this.server.ready;
    if (verbose) {
      if (r) {
        this._serverMsg("Server started and ready.", 4);
      } else {
        this._serverMsg("Server not started.", 2);
      }
    }
    this.actions.setInfo(this.name, { ready: r });
    return new Promise((resolve, reject) => {
      resolve(r);
    });
  }

  _serverMsg(msg, intent = 0) {
    this.actions.createServerMsg(this.name, msg, intent);
  }
}
