import React from "react";
import { connect } from "react-redux";
import { Switch, NumericInput, FormGroup, Button } from "@blueprintjs/core";

import shortid from "shortid";

import {
  connectCurator,
  connectBeacon,
  connectConfig,
  connectWorkspaceMonitor,
  YapijProvider,
  SimpleCurator,
  SimpleBeacon,
  PyType,
  Settings,
  Inputs,
  Shell,
  MessageList,
  Beacon,
  WorkspaceMonitor,
  PyRunning,
} from "../src";
import WD, {
  Default as DefaultWorkspaceDialog,
  SingleWorkspaceDialog,
} from "../src/Components/WorkspaceDialogs";
import * as pte from "./samples/samplePyTypes.js";
import { YapijReduxKey } from "../src/common";

import "./examples.scss";

const SimpleCuratorWrapped = connectCurator()(SimpleCurator);
const SimpleBeaconWrapped = connectBeacon()(SimpleBeacon);

const Simple = () => (
  <div className="simple">
    <SimpleCuratorWrapped />
    <SimpleBeaconWrapped />
  </div>
);

const DarkLightPyType = ({ ex, name, config, ...props }) => (
  <div className="dark-light-pytype" {...props}>
    <h3>
      <code>
        <em>{name}</em>
      </code>
    </h3>
    <div className={`${config.theme}`}>
      <PyType output={ex} config={config} />
    </div>
    <hr />
  </div>
);

class PyTypeExamples_ extends React.Component {
  render() {
    return (
      <div>
        {Object.keys(pte).map((ex, ii) => (
          <DarkLightPyType
            ex={pte[ex]}
            name={pte[ex].pyType}
            key={`pte-${ii}`}
            config={this.props.config}
          />
        ))}
      </div>
    );
  }
}
const PyTypeExamples = connect(
  state => {
    return { config: state.YAPIJ.config };
  },
  null
)(PyTypeExamples_);

const SettingsPane = connectConfig()(Settings.Settings);

const WorkspaceMonitorWrapped = connectWorkspaceMonitor()(WorkspaceMonitor);
const WorkspaceMonitorExamples = () => (
  <div className="workspace-monitor-exmaples">
    <h3>Workspace Monitor</h3>
    <WorkspaceMonitorWrapped />
  </div>
);

class InputsExamples_ extends React.Component {
  constructor(props) {
    super(props);

    this.state = { value: "" };
  }

  render() {
    return (
      <div>
        <h3>Active input for shell</h3>
        <h3>Self-Contained</h3>
        <em>Value of input is saved within component state.</em>
        <Inputs.ActiveSelfContained
          config={this.props.config}
          onSubmit={v => console.log(v)}
        />
        <h3>Not Self-Contained</h3>
        <em>Value of input is saved in state of the parent component.</em>
        <Inputs.Active
          config={this.props.config}
          onSubmit={v => console.log(v)}
          onChange={v => {
            console.log(v);
            this.setState({ value: v });
          }}
          value={this.state.value}
        />
        <h3>Passive input</h3>
        <div className={`${this.props.config.theme}`}>
          <Inputs.Passive
            config={this.props.config}
            input={`# A few more loops.

# Powers of 2 (for no obvious reason)
power = 1
for y in range(0,21):
    print("2 to the", y, "is", power)
    power = 2 * power
    
# Scanning a list.
fred = ['And', 'now', 'for', 'something', 'completely', 'different.'];
for i in range(0,len(fred)):
    print(i, fred[i])`}
          />
        </div>
      </div>
    );
  }
}
const InputsExamples = connectConfig()(InputsExamples_);

const ShellWrapper = connectCurator()(Shell);

const ShellStyle = { maxHeight: "500px" };
const ShellExample = () => (
  <div className="shell-example">
    <h3>Shell</h3>
    <div style={ShellStyle}>
      <ShellWrapper />
    </div>
  </div>
);

const MessageListWrapped = connectCurator("default", {
  config: "config",
  msgs: "msgs",
})(MessageList);

const BeaconWrapped = connectCurator("default", {
  config: "config",
  msgs: "msgs",
})(Beacon);

const MessageListExamples = () => (
  <div className="message-list-exmaples">
    <h3>Message List</h3>
    <MessageListWrapped />
    <hr />
    <h3>Beacon</h3>
    <BeaconWrapped />
  </div>
);

class PyRunningExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = { running: false, started: false, theme: "dark", size: 24 };
  }

  render() {
    return (
      <div>
        <h3>PyRunning</h3>
        <div className={`py-running-example-container ${this.state.theme}`}>
          <div>
            <PyRunning
              running={this.state.running}
              started={this.state.started}
              theme={this.state.theme}
              size={this.state.size}
            />
          </div>
        </div>
        <hr />
        <div>
          <Switch
            checked={this.state.started}
            onChange={() => this.setState({ started: !this.state.started })}
            label="Started"
          />
          <Switch
            checked={this.state.running}
            onChange={() => this.setState({ running: !this.state.running })}
            label="Running"
            disable={!this.state.started}
          />
          <Switch
            checked={this.state.theme === "dark"}
            onChange={() =>
              this.setState({
                theme: this.state.theme === "dark" ? "light" : "dark",
              })
            }
            label="Theme"
          />
          <FormGroup label="Indicator Size">
            <NumericInput
              value={this.state.size}
              onValueChange={v => this.setState({ size: v })}
            />
          </FormGroup>
        </div>
      </div>
    );
  }
}

const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
};

const generateFile = currentFile => {
  if (Math.random() > 0.5) {
    return currentFile;
  }

  let n = getRandomInt(1, 4),
    f = [];
  for (let ii = 0; ii < n; ii++) {
    f.push(shortid.generate());
  }
  return "/" + f.join("/");
};

class WorkspaceDialogExamples extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);

    this.state = {
      SaveAs: false,
      Save: false,
      Load: false,
      New: false,
      Rename: false,
      Delete: false,
      Copy: false,
      CD: false,

      workspace: { metadata: { info: { filename: null } }, env: {}, stats: {} },
    };
  }

  onSubmit(k, v) {
    this.setState(
      {
        [k]: false,
        workspace: {
          metadata: {
            info: { filename: generateFile(this.state.currentFile) },
          },
          env: {},
          stats: {},
        },
      },
      () => alert(`Submitted "${k}" with value "${v}"`)
    );
  }

  onCancel(k) {
    this.setState({ [k]: false }, () => alert(`Canceled "${k}"`));
  }

  render() {
    return (
      <div className="workspace-dialog">
        <h3>Workspace Dialogs</h3>
        <p>
          These are <em>minimal working examples</em>. Full implementations
          would want to leverage the file system.
        </p>
        <hr />
        <Button
          text="Save As"
          onClick={() => this.setState({ SaveAs: !this.state.SaveAs })}
          minimal
          large
        />
        <div className="spacer" />
        <Button
          text="Save"
          onClick={() => this.setState({ Save: !this.state.Save })}
          minimal
          large
        />
        <div className="spacer" />
        <Button
          text="New"
          onClick={() => this.setState({ New: !this.state.New })}
          minimal
          large
        />
        <div className="spacer" />
        <Button
          text="Load"
          onClick={() => this.setState({ Load: !this.state.Load })}
          minimal
          large
        />
        <div className="spacer" />
        <Button
          text="Rename"
          onClick={() => this.setState({ Rename: !this.state.Rename })}
          minimal
          large
        />
        <div className="spacer" />
        <Button
          text="Delete"
          onClick={() => this.setState({ Delete: !this.state.Delete })}
          minimal
          large
        />
        <div className="spacer" />
        <Button
          text="Copy"
          onClick={() => this.setState({ Copy: !this.state.Copy })}
          minimal
          large
        />
        <div className="spacer" />
        <Button
          text="CD (Set working directory)"
          onClick={() => this.setState({ CD: !this.state.CD })}
          minimal
          large
        />
        <hr />

        <SingleWorkspaceDialog isOpen={this.state.SaveAs}>
          <DefaultWorkspaceDialog.SaveAs
            onSubmit={v => this.onSubmit("SaveAs", v)}
            onCancel={() => this.onCancel("SaveAs")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
        <SingleWorkspaceDialog isOpen={this.state.Save}>
          <DefaultWorkspaceDialog.Save
            onSubmit={v => this.onSubmit("Save", v)}
            onCancel={() => this.onCancel("Save")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
        <SingleWorkspaceDialog isOpen={this.state.Load}>
          <DefaultWorkspaceDialog.Load
            onSubmit={v => this.onSubmit("Load", v)}
            onCancel={() => this.onCancel("Load")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
        <SingleWorkspaceDialog isOpen={this.state.New}>
          <DefaultWorkspaceDialog.New
            onSubmit={v => this.onSubmit("New", v)}
            onCancel={() => this.onCancel("New")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
        <SingleWorkspaceDialog isOpen={this.state.Rename}>
          <DefaultWorkspaceDialog.Rename
            onSubmit={v => this.onSubmit("Rename", v)}
            onCancel={() => this.onCancel("Rename")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
        <SingleWorkspaceDialog isOpen={this.state.Delete}>
          <DefaultWorkspaceDialog.Delete
            onSubmit={v => this.onSubmit("Delete", v)}
            onCancel={() => this.onCancel("Delete")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
        <SingleWorkspaceDialog isOpen={this.state.Copy}>
          <DefaultWorkspaceDialog.Copy
            onSubmit={v => this.onSubmit("Copy", v)}
            onCancel={() => this.onCancel("Copy")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
        <SingleWorkspaceDialog isOpen={this.state.CD}>
          <DefaultWorkspaceDialog.CD
            onSubmit={v => this.onSubmit("CD", v)}
            onCancel={() => this.onCancel("CD")}
            workspace={this.state.workspace}
          />
        </SingleWorkspaceDialog>
      </div>
    );
  }
}
export {
  Simple,
  PyTypeExamples,
  SettingsPane,
  InputsExamples,
  ShellExample,
  MessageListExamples,
  WorkspaceMonitorExamples,
  PyRunningExample,
  WorkspaceDialogExamples,
};
