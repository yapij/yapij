import React from "react";
import ReactDOM from "react-dom";
import { Navbar, Button, Card, Callout, Intent, Menu } from "@blueprintjs/core";

import {
  editorExample,
  shellExample,
  simpleCuratorExample,
} from "./examples.js";

import {
  Simple,
  PyTypeExamples,
  SettingsPane,
  InputsExamples,
  ShellExample,
  MessageListExamples,
  WorkspaceMonitorExamples,
  PyRunningExample,
  WorkspaceDialogExamples,
} from "./examples.js";

import "./index.scss";

import { YapijProvider, connectWorkspaceMonitor } from "../src";
import WorkspaceDialogs from "../src/Components/WorkspaceDialogs";

const WorkspaceDialogsWrapped = connectWorkspaceMonitor()(WorkspaceDialogs);

class Index extends React.Component {
  constructor(props) {
    super(props);

    this.state = { display: "shell" };
  }

  render() {
    return (
      <div>
        <YapijProvider>
          <div className="demo-container">
            <div className="navbar-container">
              <Navbar>
                <Navbar.Group>
                  <Navbar.Heading>
                    <code>yapij-re</code>
                  </Navbar.Heading>
                  <Navbar.Divider />
                  <Button
                    minimal
                    className="top-menu"
                    onClick={() => this.setState({ display: "shell" })}
                  >
                    Shell
                  </Button>{" "}
                  <Button
                    minimal
                    className="top-menu"
                    onClick={() => this.setState({ display: "simple" })}
                  >
                    Simple
                  </Button>
                  <Button
                    minimal
                    className="top-menu"
                    onClick={() => this.setState({ display: "pytypes" })}
                  >
                    PyTypes
                  </Button>
                  <Button
                    minimal
                    className="top-menu"
                    onClick={() => this.setState({ display: "inputs" })}
                  >
                    Inputs
                  </Button>
                  <Button
                    className="top-menu"
                    minimal
                    onClick={() => this.setState({ display: "msg-list" })}
                  >
                    Message List
                  </Button>
                  <Button
                    minimal
                    className="top-menu"
                    onClick={() =>
                      this.setState({ display: "workspace-monitor" })
                    }
                  >
                    Workspace Monitor
                  </Button>
                  <Button
                    minimal
                    className="top-menu"
                    onClick={() => this.setState({ display: "py-running" })}
                  >
                    Pyrunning Indicator
                  </Button>
                  <Button
                    minimal
                    className="top-menu"
                    onClick={() =>
                      this.setState({ display: "workspace-dialogs" })
                    }
                  >
                    Workspace Dialogs
                  </Button>
                </Navbar.Group>
              </Navbar>
            </div>
            <div className="settings-container">
              <Card elevation={2}>
                <SettingsPane />
              </Card>
            </div>

            <div className="info-nonsense-container">
              <Callout intent="danger">
                If you try to play around with the shell you'll quickly find
                that it spits out nonsense. This is just for testing purposes on
                the web.
                <br />
                <br />
                If this is run within an electron app (i.e. with access to node
                server) then it will communicate with an actual python process.
              </Callout>
            </div>

            <div className="example-container-9">
              <Card elevation={2} className="ex-card">
                {this.state.display === "simple" ? <Simple /> : null}
                {this.state.display === "pytypes" ? <PyTypeExamples /> : null}
                {this.state.display === "inputs" ? <InputsExamples /> : null}
                {this.state.display === "shell" ? <ShellExample /> : null}
                {this.state.display === "msg-list" ? (
                  <MessageListExamples />
                ) : null}
                {this.state.display === "workspace-monitor" ? (
                  <WorkspaceMonitorExamples />
                ) : null}
                {this.state.display === "py-running" ? (
                  <PyRunningExample />
                ) : null}
                {this.state.display === "workspace-dialogs" ? (
                  <WorkspaceDialogExamples />
                ) : null}
              </Card>
            </div>
            <WorkspaceDialogsWrapped />
          </div>
        </YapijProvider>
      </div>
    );
  }
}

ReactDOM.render(<Index />, document.getElementById("root"));
