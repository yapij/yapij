![yapij logo](https://i.imgur.com/k0yOWtV.png)

React+Redux components for plugging into the lower-level `yapij` package.

Go to the **[docs](https://yapij.gitlab.io/)**.
