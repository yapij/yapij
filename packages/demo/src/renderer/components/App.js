import React from "react";

import { Button } from "primereact/button";

const {
  YapijProvider,
  SimpleCurator,
  SimpleBeacon,
  Shell,
  Editor,
  InstallYapij,
  HOC,
  // FileBrowser,
} = window.require("@yapij/re");

import TitleBar from "./TitleBar.js";
import Body from "./Body.js";

import "./App.scss";

// Make it so that the title bar is connected into these actions
const TitleBarConnected = HOC.connectYapij()(TitleBar);

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <YapijProvider>
          <TitleBarConnected />

          <Body right={<Shell />} left={<Editor />} />
        </YapijProvider>
      </div>
    );
  }
}

export default App;
