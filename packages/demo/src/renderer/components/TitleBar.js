import React from "react";
import { Button } from "primereact/button";
import { Sidebar } from "primereact/sidebar";

import "./TitleBar.scss";

export default class TitleBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = { sidebarVisible: false };
  }

  render() {
    return (
      <div className="title-bar">
        <div
          className="popout"
          onClick={() =>
            this.setState({ sidebarVisible: !this.state.sidebarVisible })
          }
        >
          <Sidebar
            visible={this.state.sidebarVisible}
            onHide={e => this.setState({ sidebarVisible: false })}
            className="yapij-demo-sidebar"
            showCloseIcon={false}
          >
            <div className="title">yapij</div>
            <hr />
            <div className="header">Workspace</div>
            <div className="action" onClick={this.props.yapij.workspace.new}>
              New
            </div>
            <div className="action" onClick={this.props.yapij.workspace.load}>
              Load
            </div>
            <div className="action" onClick={this.props.yapij.workspace.save}>
              Save
            </div>
            <div className="action" onClick={this.props.yapij.workspace.saveAs}>
              Save As
            </div>
            <div className="action" onClick={this.props.yapij.workspace.rename}>
              Rename
            </div>
            <div className="action" onClick={this.props.yapij.workspace.copy}>
              Copy
            </div>
            <hr />
            <div className="header">
              <span className="fab fa-python" />
              Kernel
            </div>
            <div className="action" onClick={this.props.yapij.start}>
              <span className="fas fa-play" />
              Start
            </div>
            <div className="action" onClick={this.props.yapij.stop}>
              <span className="fas fa-stop" />
              Stop
            </div>
            <div className="action" onClick={this.props.yapij.restart}>
              <span className="fas fa-sync" />
              Restart
            </div>
            <hr />
            <div
              className="action"
              onClick={this.props.yapij.globalDialogs.settings}
            >
              <span className="fas fa-cog" /> Settings
            </div>
            <div
              className="action"
              onClick={this.props.yapij.globalDialogs.installer}
            >
              <span className="fas fa-box" /> Installation
            </div>
          </Sidebar>
          <span className="fas fa-bars icon" />
        </div>
        <div className="title">yapij</div>
      </div>
    );
  }
}
