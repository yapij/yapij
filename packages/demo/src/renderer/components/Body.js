import React from "react";
import { Card } from "primereact/card";

import "./Body.scss";

export default class Body extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      style: { gridTemplateColumns: "auto 18px 450px" },
    };

    this.handleResize = this.handleResize.bind(this);
  }

  handleResize(e) {
    if (e.pageX !== 0) {
      this.setState({
        style: { gridTemplateColumns: `${e.pageX}px 18px auto` },
      });
    }
  }

  render() {
    return (
      <div className="body-container">
        <div className="body" style={this.state.style}>
          <Card className="left">{this.props.left}</Card>
          <div
            className="grip"
            onDrag={this.handleResize}
            onDragEnter={this.handleResize}
          >
            <span className="fas fa-grip-vertical icon" />
          </div>
          <Card className="right">{this.props.right}</Card>
        </div>
      </div>
    );
  }
}
