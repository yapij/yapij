import { createStore, combineReducers } from "redux";
const isDevMode = window.require("electron-is-dev");
const yapijReducers = window.require("@yapij/re").redux.reducers;

const reducers = combineReducers({
  ...{ YAPIJ: yapijReducers },
});

export default (isDevMode
  ? createStore(
      reducers,
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    )
  : createStore(reducers));
