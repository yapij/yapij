class CmdHandler {
  constructor(sessionId, uid, idx, cmd, on, quiet) {
    this.uid = uid;
    this.idx = idx;
    this.in = cmd;
    this.on = on;
    this.sessionId = sessionId;
    this.quiet = quiet;

    this.dateCreated = new Date();

    this.out = [];
    this.statuses = [];

    this.on.input(this, cmd);
  }

  updateStatus(status) {
    this.statuses.push(status);
    this.on.status(this, status);
  }

  updateOutput(output) {
    this.out.push(output);
    this.on.output(this, output);
  }

  get received() {
    return this.statuses.find(s => s.receiver) || false;
  }

  get queued() {
    return this.statuses.find(s => s.queuer) || false;
  }

  get started() {
    return this.statuses.find(s => s.starter) || false;
  }

  get finished() {
    return this.statuses.find(s => s.finisher) || false;
  }
}

module.exports = CmdHandler;
