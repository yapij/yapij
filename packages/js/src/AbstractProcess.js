const { defaultOutputHandlers } = require("./defaults.js");

class AbstractProcess {
  constructor(_on, ports, stopServer) {
    // Note: in child classes `_on` initializes to a default set of helpers.
    this._on = _on;
    this.ports = ports;
    this.stopServer = stopServer;
  }

  start(ports) {
    throw new Error("Not Implemented");
  }

  /**
   * Stops the process. Should not be called directly
   * @return {null}
   */
  _stop() {
    throw new Error("Not Implemented");
  }

  on(k, callback) {
    // Callback must be a function
    if (typeof callback !== "function") {
      throw new Error(
        `Callback must be of type 'function', got type ${typeof callback}`
      );
    }

    // Check that key is valid
    let allKeys = Object.keys(this._on),
      keyMatch = false;
    for (let ii = 0; ii < allKeys.length; ii++) {
      if (k == allKeys[ii]) {
        keyMatch = true;
        break;
      }
    }
    if (!keyMatch) {
      throw new Error(`Key '${k}' not valid!`);
    } else {
      this._on[k] = callback;
    }
  }

  _checkPorts(ports = null) {
    if (ports) {
      this.ports = ports;
    }
    if (this.ports.frontend === null) {
      throw new Error(
        `Invalid ports. Cannot start. Remember to start from 'Server.start', NOT, e.g. 'Server.kernel.start()'.`
      );
    }
  }
}

module.exports = AbstractProcess;
