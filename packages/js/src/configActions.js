const Path = require("path");
const child_process = require("child_process");
const os = require("os");
const fs = require("graceful-fs");
const xdgBasedir = require("xdg-basedir");

const defaultDone = (msg, status) => {
  if (!status) {
    throw new Error(msg);
  }
};

class VirtualEnvs {
  contructor(config, verbose = false) {
    this.config = config;
    this.verbose = verbose;

    // Establish if we're in a windows system
    this.isWin = process.platform === "win32";

    // Get the configuration directory that
    // For example:
    //    xdgBasedir.config => 'C:\\Users\\us57144\\.config'
    //    Path.join(os.tmpdir(), uniqueString()) => 'C:\\Users\\us57144\\AppData\\Local\\Temp\\979f046de6ed00465e80bc731b17295d'
    this.configDir =
      xdgBasedir.config || Path.join(os.tmpdir(), uniqueString());
  }

  addNew({ path, name }) {
    const createEnv = (p, cmd, tryAgain) => {
      return child_process.execSync(cmd, (err, stdout, stderr) => {
        if (err) {
          // May be necessary to specify that want python 3 in some cases.
          if (this.isWin && tryAgain) {
            var cmd2 = `${this.config.get(
              "python.python.exec"
            )} -3 -m venv ${p}`;
            return createEnv(p, cmd2, false);
          } else {
            throw err;
          }
        }
      });
    };

    this._log(`Creating virtual environment...`);
    this.checkNew(path);

    var p = path === "" ? this.configDir : Path.resolve(path);
    p = Path.join(p, `yapij-venv-${Math.round(Math.random() * 1e9)}`);

    var cmd = `${this.config.get("python.python.exec")} -m venv ${p}`;
    createEnv(p, cmd, true);

    venvs.push({ name: name, path: p });
    this.config.set("python.venvDirs", venvs);
    this._log(`Added '${name}' to virtualenvs at '${p}'`);
    return true;
  }

  addExisting({ path, name }) {
    this.checkExisting(path);
    var p = Path.resolve(ath);
    var venvs = this.config.get("python.venvDirs");
    venvs.push({ name: name, path: p });
    this.config.set("python.venvDirs", venvs);
    this._log(`Added '${name} to virtualenvs at '${p}'`);
  }

  setDefault(idx) {
    if (idx === -1) {
      this.config.set("python.defaultVenv", {
        path: this.config.get("python.python.exec"),
        name: null
      });
    } else if (idx === parseInt(idx)) {
      var venv = this.config.get("python.venvDirs")[idx];
      venv.path = this.isWin
        ? Path.join(venv.path, "Scripts", "python")
        : Path.join(venv.path, "env", "python");

      this.config.set("python.defaultVenv", venv);
    } else {
      var venv = this.config.get("python.venvDirs").find(k => k.name === idx);
      if (!venv) {
        throw new Error(`Could not find virutalenv: '${idx}'`);
      }

      venv.path = this.isWin
        ? Path.join(venv.path, "Scripts", "python")
        : Path.join(venv.path, "env", "python");

      this.config.set("python.defaultVenv", venv);
    }
  }

  remove(name) {}

  checkNew(p, done = defaultDone) {
    if (p === "") {
      done(null, true);
    } else if (this._checkPath(p)) {
      done(null, true);
    } else {
      done("Invalid path. Please try again.", false);
    }
  }

  checkExisting(p, done = defaultDone) {
    if (this._checkPath(p)) {
      if (!fs.statSync(Path.join(p, "pyvenv.cfg")).isFile()) {
        done(
          "Did not find `pyvenv.cfg` in directory. Will not work as virtual env",
          false
        );
      } else if (!fs.statSync(Path.join(p, "Include")).isDirectory()) {
        done(
          "Did not find `/Include` in directory. Will not work as virtual env",
          false
        );
      } else if (!fs.statSync(Path.join(p, "Scripts")).isDirectory()) {
        done(
          "Did not find `/Scripts` in directory. Will not work as virtual env",
          false
        );
      } else if (!fs.statSync(Path.join(p, "Lib")).isDirectory()) {
        done(
          "Did not find `/Lib` in directory. Will not work as virtual env",
          false
        );
      } else {
        done(null, true);
      }
    } else {
      done("Invalid path. Please try again.", false);
    }
  }

  checkNames(name, done = defaultDone) {
    if (name === "") {
      done("Name needs at least a character", false);
    } else if (this.config.get("python.venvDirs").some(k => k.name === name)) {
      done("That name already exists as a virtualenv", false);
    } else {
      done(null, true);
    }
  }

  _log(msg) {
    if (this.verbose) {
      console.log(msg);
    }
  }

  _checkPath(path) {
    var s = fs.statSync(path);
    return s.isDirectory();
  }
}



