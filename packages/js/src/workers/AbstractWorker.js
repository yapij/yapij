const AbstractProcess = require("../AbstractProcess.js");

class AbstractWorker extends AbstractProcess {
  constructor(_on, ports, stopServer) {
    // Note: in child classes `_on` initializes to a default set of helpers.
    super(_on, ports, stopServer);
  }
}

module.exports = AbstractWorker;
