const session = {
  PUB: "PY_EXEC",
  SUB: "PY_OUT",
  QUIT: "PY_QUIT",
  STATUS: "PY_STATUS"
};
const heartbeat = { PUB: "PY_EXEC", SUB: "PY_OUT" };

module.exports = { session, heartbeat };
