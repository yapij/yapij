const zmq = require("zeromq");
const AbstractWorker = require("./AbstractWorker.js");

class Bridge extends AbstractWorker {
  constructor(ports) {
    super({}, ports, null);

    this.xpub = null;
    this.xsub = null;

    this.ready = false;
  }

  start(ports) {
    return new Promise((resolve, reject) => {
      try {
        this._checkPorts(ports);
        this.xpub = zmq.socket("xpub");
        this.xsub = zmq.socket("xsub");

        this.xpub.bindSync(this.ports.frontend);
        this.xsub.bindSync(this.ports.backend);

        zmq.proxy(this.xpub, this.xsub);

        resolve((this.ready = true));
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Stops the process. Should not be called directly
   * @return {null}
   */
  _stop() {
    if (this.xpub) {
      this.xpub.close();
    }
    if (this.xsub) {
      this.xsub.close();
    }
    this.xpub = null;
    this.xsub = null;
    this.ready = false;
  }
}

module.exports = Bridge;
