const zmq = require("zeromq");
const shortid = require("shortid");

const topics = require("./topics.js");
const Unpacker = require("../unpackers");
const { defaultOutputHandlers } = require("../defaults.js");
const CmdHandler = require("../CmdHandler.js");

const AbstractWorker = require("./AbstractWorker.js");

const unpacker = new Unpacker();

class Session extends AbstractWorker {
  constructor(ports, useCmdHandlers) {
    super(defaultOutputHandlers.session, ports, null);
    this._set_unready_blank();
    this._uch = useCmdHandlers;
  }

  get useCmdHandlers() {
    return this._uch;
  }

  start(ports, key) {
    return new Promise((resolve, reject) => {
      try {
        this.key = key;
        this.PUB_TOPIC = [topics.session.PUB, this.key].join("_");
        this.SUB_TOPIC = [topics.session.SUB, this.key].join("_");
        this.STATUS_TOPIC = [topics.session.STATUS, this.key].join("_");

        this._checkPorts(ports);
        this.pub = zmq.socket("pub");
        this.pub.connect(this.ports.backend);

        this.sub = zmq.socket("sub");
        this.sub.connect(this.ports.frontend);
        this.sub.subscribe(this.SUB_TOPIC);
        this.sub.subscribe(this.STATUS_TOPIC);
        this.sub.on("message", (t, ...m) => this.recv(t, ...m));

        this.history = this._uch ? {} : null;
        this.cmdIndex = 0;
        this.sessionId = shortid.generate();

        resolve((this.ready = true));
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Stops the process. Should not be called directly
   * @return {null}
   */
  _stop() {
    if (this.pub) {
      this.pub.close();
    }
    if (this.sub) {
      this.sub.close();
    }

    this._set_unready_blank();
  }

  _set_unready_blank() {
    this.PUB_TOPIC = null;
    this.SUB_TOPIC = null;

    this.history = null;
    this.cmdIndex = null;

    this.pub = null;
    this.sub = null;
    this.ports = null;
    this.ready = false;
  }

  send(msg, quiet) {
    if (!this.ready || !this.pub) {
      throw new Error("Session not started!");
    }
    var uid = shortid.generate();
    this.cmdIndex += 1;

    var hdl;
    if (this._uch) {
      hdl = new CmdHandler(
        this.sessionId,
        uid,
        this.cmdIndex,
        msg,
        this._on,
        quiet
      );
      this.history[uid] = hdl;
    } else {
      hdl = {
        sessionId: this.sessionId,
        uid: uid,
        index: this.cmdIndex,
        in: msg,
        date: new Date(),
        quiet: quiet,
      };
      this._on.input(null, hdl);
    }

    this.pub.send([this.PUB_TOPIC, uid, msg]);
    return hdl;
  }

  send_stop(force_all = false) {
    if (force_all) {
      this.pub.send([topics.session.QUIT, "1"]);
    } else {
      this.pub.send([[topics.session.QUIT, this.key].join("_"), "1"]);
    }
  }

  recv(topic, ...msg) {
    var umsg;
    switch (topic.toString()) {
      case this.SUB_TOPIC:
        umsg = unpacker.output(this.sessionId, msg);
        if (this._uch) {
          this.history[umsg.uid].updateOutput(umsg);
        } else {
          this._on.output(null, umsg.asObject);
        }
        break;
      case this.STATUS_TOPIC:
        umsg = unpacker.status(this.sessionId, msg);
        if (this._uch) {
          this.history[umsg.uid].updateStatus(umsg);
          // Delete if we've received a "finished" status
          if (umsg.finisher) {
            setTimeout(() => {
              delete this.history[umsg.uid];
            }, 2500);
          }
        } else {
          this._on.status(null, umsg);
        }
        break;
      default:
        throw new Error(
          `Received unknown topic "${topic}" with message: "${msg}". (Legitimate topics: "${
            this.SUB_TOPIC
          }", "${this.STATUS_TOPIC}")`
        );
    }
  }
}

module.exports = Session;
