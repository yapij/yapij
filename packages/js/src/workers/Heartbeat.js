const zmq = require("zeromq");

const { defaultOutputHandlers } = require("../defaults.js");
const AbstractWorker = require("./AbstractWorker.js");

class Heartbeat extends AbstractWorker {
  constructor(ports, stopServer, heartbeat_interval) {
    super(defaultOutputHandlers.heartbeat, ports, stopServer);
    this._hi = 1000 * heartbeat_interval;

    this.req = null;
    this._h = false;
    this.ready = false;
  }

  get heartbeat_interval() {
    return this._hi / 1000;
  }

  start(ports) {
    return new Promise((resolve, reject) => {
      try {
        this._checkPorts(ports);

        this.req = zmq.socket("req");
        this.req.bindSync(this.ports.heartbeat_port);
        this.req.on("message", msg => {
          this._h = true;
        });

        this._startHeartbeatSend();
        this.req.send(1);
        this._initialHeartbeatSend(10000 + this._hi)
          .then(v => {
            resolve(v);
          })
          .catch(ee => {
            throw ee;
          });
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Stops the process. Should not be called directly
   * @return {null}
   */
  _stop() {
    // Stop sending heartbeats
    clearInterval(this._heartbeatSend);
    // Stop checking for a heartbeat
    clearInterval(this._heartbeatCheck);

    if (this.req) {
      this.req.close();
    }

    this._h = false;
    this.ports = null;
    this.req = null;
    this.ready = false;
    this._heartbeatSend = null;
    this._heartbeatCheck = null;
  }

  _startHeartbeatCheck() {
    this._heartbeatCheck = setInterval(() => {
      if (this._h) {
        this._h = false;
      } else {
        this.stopServer();
        this._on.fail(
          `No heartbeat registered in last ${2 *
            this
              ._hi} seconds. Trying to stop and restart. Unsaved work may be lost. (NOTE: NOT YET COMPLETE!)`
        );
      }
    }, 2000 * this._hi);
  }

  _startHeartbeatSend() {
    this._heartbeatSend = setInterval(() => {
      this.req.send(1);
    }, this._hi);
  }

  _initialHeartbeatSend(timeoutms) {
    var init_timeoutms = timeoutms;
    return new Promise((r, j) => {
      var check = () => {
        if (this._h) {
          // Reset the heartbeat counter
          this._h = false;

          // Need to start ordinary checks for heartbeat
          this._startHeartbeatCheck();

          r((this.ready = true));
        } else if ((timeoutms -= 100) < 0) {
          this.stopServer();
          j(`No heartbeat registered in last ${init_timeoutms / 1000} s.`);
        } else {
          setTimeout(check, 100);
        }
      };
      setTimeout(check, 100);
    });
  }
}

module.exports = Heartbeat;
