const findFreePort_ = require("find-free-port");
var crypto = require("crypto");

const ConfigStore = require("./ConfigStore.js");

const findFreePort = n => {
  findFreePort_(4000, 9000, "127.0.0.1", n, (err, ...ports) => {
    if (err) {
      reject(err);
    }
    console.log("here!!", ports);
    ports = ports.map(p => `tcp://127.0.0.1:${p}`);
    return ports;
  });
  return new Promise((resolve, reject) => {
    findFreePort_(4000, 9000, "127.0.0.1", n, (err, ...ports) => {
      if (err) {
        reject(err);
      }
      console.log("here!!");
      ports = ports.map(p => `tcp://127.0.0.1:${p}`);
      resolve(ports);
    });
  });
};

const makeId = n => {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < n; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};

module.exports = { findFreePort, makeId, ConfigStore };
