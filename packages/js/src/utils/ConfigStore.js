"use strict";
const ConfigStore_ = require("configstore");

class Configstore extends ConfigStore_ {
  constructor(id, defaults, options = {}) {
    super(id, defaults, options);

    if (defaults) {
      this._defaults = defaults;
      if (options.reset) {
        this.reset();
      }
    }
  }

  reset() {
    this.set(this._defaults);
  }
}

module.exports = Configstore;
