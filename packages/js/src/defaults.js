const defaultOutputHandlers = {
  session: {
    output: (ch, msg) => {
      console.log(`   [YAPIJ:Out] (${msg.uid}) ${msg}`);
    },
    input: (ch, msg) => {
      console.log(`    [YAPIJ:In] (${msg.uid || ch.uid}) ${msg}`);
    },
    status: (ch, msg) => {
      console.log(
        `[YAPIJ:Status] (${msg.uid}) ${msg.names[0]} | ${msg.names[1]} `
      );
    }
  },
  heartbeat: {
    fail: msg => {
      console.error(`[YAPIJ:FAIL] ${msg}`);
    }
  },
  kernel: {
    close: code => {
      console.log(`[YAPIJ:CLOSE] ${code}`);
    },
    stdout: msg => {
      console.log(`[YAPIJ:STDOUT] ${msg}`);
    },
    stderr: msg => {
      console.log(`[YAPIJ:STDERR]\n${msg}`);
    }
  }
};

const defaultPythonArgs = {
  heartbeat_interval: 120
};

module.exports = { defaultOutputHandlers, defaultPythonArgs };
