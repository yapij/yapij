const shortid = require("shortid");
const {
  getRandomIndex,
  drawOutput,
  drawStatus,
  getInfo,
} = require("./pyTypes.js");

class DummyServer {
  constructor(pythonArgs = {}, config = {}) {
    this.ready = false;
    this.sessionId = null;
    this.cmdIndex = null;

    this.workers = { heartbeat: { _on: {} }, session: { _on: {} } };
    this.kernel = { _on: {} };
    this._env = {};
  }

  start() {
    return new Promise((r, j) => {
      this.ready = true;
      this.sessionId = shortid.generate();
      this.cmdIndex = 0;
      r({
        server: this,
        ports: {
          backend: "127.0.0.1:1001",
          frontend: "127.0.0.1:1002",
          heartbeat: "127.0.0.1:1003",
        },
      });
    });
  }

  stop() {
    return new Promise((r, j) => {
      this.ready = false;
      this.sessionId = null;
      this.cmdIndex = null;
      r({
        server: this,
        ports: {
          backend: "127.0.0.1:1001",
          frontend: "127.0.0.1:1002",
          heartbeat: "127.0.0.1:1003",
        },
      });
    });
  }

  restart() {
    return new Promise((r, j) => {
      this.ready = true;
      this.sessionId = shortid.generate();
      this.cmdIndex = 0;
      r({
        server: this,
        ports: {
          backend: "127.0.0.1:1001",
          frontend: "127.0.0.1:1002",
          heartbeat: "127.0.0.1:1003",
        },
      });
    });
  }

  exec(cmd, quiet = false) {
    if (!this.ready) {
      throw new Error("Session not started!");
    }

    var uid = shortid.generate(),
      hdl;
    this.cmdIndex += 1;
    hdl = {
      sessionId: this.sessionId,
      uid: uid,
      index: this.cmdIndex,
      in: cmd,
      date: new Date(),
      output: false,
      quiet: quiet,
    };

    this.workers.session._on.input(null, hdl);
    this.workers.session._on.status(null, drawStatus(uid, this.sessionId, 0));
    this.workers.session._on.status(null, drawStatus(uid, this.sessionId, 1));
    this.workers.session._on.status(null, drawStatus(uid, this.sessionId, 2));

    if (cmd.startsWith("%")) {
      if (cmd.startsWith("%workspace_info")) {
        this.workers.session._on.output(
          null,
          getInfo(uid, this.sessionId, this._env)
        );
      }
    } else {
      let n = getRandomIndex(5),
        ii = 0;
      while (ii < n) {
        let out = drawOutput(uid, this.sessionId);
        this.workers.session._on.output(null, out);
        this._env[`${out.uid}_${ii}`] = out;
        ii++;
      }
    }

    this.workers.session._on.status(null, drawStatus(uid, this.sessionId, 3));
    return hdl;
  }

  interrupt() {
    return false;
  }

  on(key, callback) {
    var k = key.split(".");

    if (k[0] === "heartbeat") {
      this.workers.heartbeat._on[k[1]] = callback;
    } else if (k[0] === "session") {
      this.workers.session._on[k[1]] = callback;
    } else if (k[0] === "kernel") {
      this.kernel._on[k[1]] = callback;
    }
  }
}

module.exports = DummyServer;
