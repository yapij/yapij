const shortid = require("shortid");

const rLettersPunc = [
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "w",
  ".",
  " ",
  " ",
  " ",
  "\n",
  "\n",
  "\t",
];
const rLetters = ["A", "B", "C", "D", "E", "F", "w"];
function getRandomIndex(max = 900) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getPosNeg() {
  return Math.random() > 0.5 ? 1 : -1;
}

function getRandomFloat() {
  return getPosNeg() * Math.pow(Math.random(), getRandomIndex(10));
}

function getRandomInt(max = 900) {
  return getPosNeg() * getRandomIndex(max);
}

function getRandomLetter(punc = true) {
  if (punc) {
    return rLettersPunc[getRandomIndex(7)];
  } else {
    return rLetters[getRandomIndex(7)];
  }
}

function getRandomString(max = 900, punc = true) {
  var out = "";
  for (let ii = 0; ii < getRandomIndex(max); ii++) {
    out += getRandomLetter(punc);
  }
  return out;
}

function getLetterFloatIntString(noString = false) {
  var d = Math.random();

  if (d < 0.25) {
    return getRandomLetter;
  } else if (d < 0.5) {
    return getRandomInt;
  } else if (d < 0.75 || noString) {
    return getRandomFloat;
  } else {
    return getRandomString;
  }
}

function _getListLike(generator, n) {
  var out = [];
  for (let ii = 0; ii < n; ii++) {
    out.push(generator());
  }
  return out;
}

function getListLike() {
  var generator = getLetterFloatIntString(),
    n = getRandomIndex();
  return _getListLike(generator, n);
}

function getArrayLike() {
  var shape = [getRandomIndex(30), getRandomIndex(30)],
    generator = getLetterFloatIntString(true),
    dtype = null;

  var tt = generator();
  if (tt === parseInt(tt)) {
    dtype = "int";
  } else if (tt === parseFloat(tt)) {
    dtype = "float";
  } else {
    dtype = "str";
  }

  var out = [],
    out_i = [];
  for (let ii = 0; ii < shape[0]; ii++) {
    out_i = [];
    for (let jj = 0; jj < shape[1]; jj++) {
      out_i.push(generator());
    }
    out.push(out_i);
  }
  return { values: out, shape: shape, dtype: dtype };
}

function getFileLike() {
  var n = getRandomIndex(10),
    out = [];
  for (let ii = 0; ii < n; ii++) {
    out.push(getRandomString(20, false));
  }
  return out.join("/");
}

function getRandomObject(n) {
  var out = {};

  for (let ii = 0; ii < n; ii++) {
    out[getRandomString(10, false)] = getLetterFloatIntString()();
  }
  return out;
}

function getPyType(uid, sessionId, pyType, type, value) {
  return {
    uid: uid,
    sessionId: sessionId,
    pyType: "Py" + pyType,
    type: type,
    value: value,
    date: new Date(),
  };
}

/**
 * pyTypes
 */

function List(uid, sessionId) {
  return getPyType(uid, sessionId, "List", "List", getListLike());
}

function PySet(uid, sessionId) {
  return getPyType(uid, sessionId, "Set", "Set", getListLike());
}

function Tuple(uid, sessionId) {
  return getPyType(uid, sessionId, "Tuple", "Tuple", getListLike());
}

function Str(uid, sessionId) {
  return getPyType(uid, sessionId, "Str", "Str", getRandomString());
}

function Float(uid, sessionId) {
  return getPyType(uid, sessionId, "Str", "Str", getRandomFloat());
}

function Int(uid, sessionId) {
  return getPyType(uid, sessionId, "Str", "Str", getRandomInt());
}

function NpArray(uid, sessionId) {
  return getPyType(uid, sessionId, "NpArray", "numpy.ndarray", getArrayLike());
}

function Complex(uid, sessionId) {
  return getPyType(uid, sessionId, "Complex", "complex", {
    i: getRandomFloat(),
    r: getRandomFloat(),
  });
}

function Callable(uid, sessionId) {
  return getPyType(uid, sessionId, "Callable", "callable", getRandomString(40));
}

const exceptionTypes = [
  "SyntaxError",
  "ValueError",
  "IOError",
  "Exception",
  "KeyboardInterrupt",
];
function Exception(uid, sessionId) {
  let type = exceptionTypes[getRandomIndex(5)];
  return getPyType(uid, sessionId, "Callable", type, {
    tb: getRandomString(),
    lineno: getRandomIndex(),
    args: [type],
  });
}

const warningTypes = ["UserWarning", "Warning", "RuntimeWarning"];
function Warning(uid, sessionId) {
  let type = warningTypes[getRandomIndex(3)];
  return getPyType(uid, sessionId, "Warning", type, {
    file: getFileLike(),
    lineno: getRandomIndex(),
    msg: getRandomString(),
    source: getRandomString(),
    type: type,
  });
}

function PandasDataFrame(uid, sessionId) {
  var v_ = getArrayLike(),
    v = {};
  v.d = v_.values;
  v.i = {
    v: _getListLike(() => getRandomString(20, false), v_.shape[0]),
    t: _getListLike(() => "f", v_.shape[0]),
  };
  v.c = {
    v: _getListLike(() => getRandomString(20, false), v_.shape[1]),
    t: _getListLike(() => "f", v_.shape[1]),
  };
  v.s = v_.shape;

  return getPyType(uid, sessionId, "PandasDataFrame", "pandas.DataFrame", v);
}

function PandasSeries(uid, sessionId) {
  var v_ = getListLike(),
    v = {};
  v.d = v_;
  v.s = [v_.length];
  v.i = { v: _getListLike(() => getRandomString(20, false), v_.length) };
  v.c = _getListLike(() => getRandomString(20, false), 1);

  return getPyType(uid, sessionId, "PandasSeries", "pandas.Series", v);
}

function Dict(uid, sessionId) {
  return getPyType(
    uid,
    sessionId,
    "Dict",
    "dict",
    getRandomObject(getRandomIndex(100))
  );
}

function Bool(uid, sessionId) {
  return getPyType(
    uid,
    sessionId,
    "Bool",
    "bool",
    Math.random() > Math.random() ? true : false
  );
}

function Docstring(uid, sessionId) {
  var name = getRandomString(30, false);

  return getPyType(uid, sessionId, "Docstring", "Docstring", {
    name: name,
    signature: `${name}(args, ...)`,
    module: getRandomString(30),
    file: getFileLike(),
    docstring: getRandomString(),
    dir: getFileLike(),
    objType: getRandomString(30, false),
  });
}

function getInfo(uid, sessionId, _env) {
  var stats = {
    total_size: 0,
    n_objects: 0,
  };
  var env = Object.keys(_env).map(k => {
    let sz = parseFloat((Math.random() * 5).toString().slice(0, 5));

    stats.n_objects += 1;
    stats.total_size += sz;

    return { var: k, type: _env[k].type, size: sz };
  });

  return getPyType(uid, sessionId, "Dict", "dict", {
    env: env,
    stats: stats,
    metadata: {
      info: {
        creator_user: "USER",
        creator_login: "LOGIN",
        creator_uuid: "UUID",
        date_created: new Date(),
        filename: getFileLike(),
      },
      edit_history: [],
    },
  });
}

const pyTypes = [
  List,
  PySet,
  Tuple,
  Str,
  Float,
  Int,
  NpArray,
  Complex,
  Callable,
  Exception,
  Warning,
  PandasSeries,
  PandasDataFrame,
  Dict,
  Bool,
  Docstring,
];

function drawOutput(uid, sessionId) {
  return pyTypes[getRandomIndex(pyTypes.length)](uid, sessionId);
}

const statusTypes1 = {
  0: "RECEIVED",
  1: "QUEUED_EXEC",
  2: "STARTED_EXEC",
  3: "FINISHED_EXEC",
  4: "INTERRUPT_EXEC",
  5: "ERROR_EXEC",
  6: "STARTED_MAGIC",
  7: "FINISHED_MAGIC",
  8: "ERROR_MAGIC",
};

const statusTypes0 = {
  0: "RECEIVED",
  1: "QUEUED",
  2: "STARTED",
  3: "FINISHED",
};

function drawStatus(uid, sessionId, code) {
  var code1;
  if (code === 0) {
    code1 = 0;
  } else if (code === 1) {
    code1 = 1;
  } else if (code === 2) {
    code1 = Math.random() > 0.5 ? 2 : 6;
  } else if (code === 3) {
    code1 = [3, 5, 4, 7, 8][getRandomIndex(5)];
  } else {
    throw new Error("invalid code");
  }

  return {
    uid: uid,
    sessionId: sessionId,
    codes: [code, code1],
    names: [statusTypes0[code], statusTypes1[code1]],
    date: new Date(),
  };
}

module.exports = { drawStatus, getRandomIndex, getInfo, drawOutput };
