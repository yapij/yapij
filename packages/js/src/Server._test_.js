// jest.setTimeout(10000);

// const Server = require("./Server.js");

// describe("Python Kernel from Node Spawned Process", () => {
//   describe("Start and stop", () => {
//     var s;

//     beforeEach(() => {
//       s = new Server();
//       s.config.set("python.pythonPkg", ".\\..\\py\\yapij\\run.py");
//     });

//     it("Can start", () => {
//       expect.assertions(1);
//       return s.start().then(() => {
//         expect(s.ready).toBe(true);
//       });
//     });

//     it("Can stop", () => {
//       expect.assertions(1);
//       return s
//         .start()
//         .then(() => {
//           return s.stop();
//         })
//         .then(() => {
//           expect(s.ready).toBe(false);
//           return true;
//         });
//     });

//     afterAll(() => {
//       s.stop();
//     });
//   });

//   describe("Can execute commands", () => {
//     var s;
//     beforeAll(async done => {
//       s = new Server();
//       var f = await s.start();
//       done();
//     });

//     it("Can print something simple", () => {
//       s.exec('print("Hello, World!")');
//     });

//     it("Can interrupt ", () => {
//       expect.assertions(1);
//       s.exec(`
// import time

// a = 0
// for ii in range(5):
//   print(ii)
//   a += 1;
//   time.sleep(2)
//   `);

//       setTimeout(() => {
//         s.interrupt();
//       }, 1500);
//     });

//     afterAll(() => {
//       s.stop();
//     });
//   });
// });

// const Server = require("./Server.js");

// var s = new Server(
//   {},
//   { python: { pythonPkg: ".\\..\\..\\py\\yapij\\run.py" } }
// );

// // Start the server (async)
// s.start()
//   .then(() => {
//     // A script with a longer run that is interrupted
//     s.exec(`
// import time

// a = 0
// for ii in range(5):
//   print(ii)
//   a += 1;
//   time.sleep(2)
//   `);

//     setTimeout(() => {
//       s.interrupt();
//       s.exec("print('a = {}'.format(a))");
//       s.exec('"done!"');
//     }, 2500);
//   })
//   .catch(e => {
//     s.stop().then(() => {
//       throw e;
//     });
//   });
