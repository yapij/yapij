const ConfigStore = require("./utils/ConfigStore");
const pkg = require("../package.json");
const Path = require("path");

var pythonEntryPoint = Path.resolve(Path.dirname(__filename), "static/run.py");

const conf = new ConfigStore(
  pkg.name,
  {
    python: {
      defaultVenv: {
        path: null,
        name: null,
        isDefault: true,
        yapijInstalled: false,
      },
      venvDirs: [],
      python: { exec: null, v: null, yapijInstalled: false },
      pythonEntryPoint: pythonEntryPoint,
    },
    spawn: {
      shell: true,
      cwd: null,
      detached: false,
    },
    installed: false,
  },
  { reset: false }
);

module.exports = conf;
