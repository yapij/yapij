const msgpack = require("msgpack5")();

const pyTypes = require("./pyTypes.js");
const Status = require("./Status.js");

var decoder = msgpack.decode;

const exceptionDecode = d => {
  return new pyTypes.Exception(...decoder(d));
};
msgpack.registerDecoder(1, exceptionDecode);

const warningDecode = d => {
  return new pyTypes.Warning(...decoder(d));
};
msgpack.registerDecoder(2, warningDecode);

const intDecode = d => {
  return new pyTypes.Int(...decoder(d));
};
msgpack.registerDecoder(3, intDecode);

const floatDecode = d => {
  return new pyTypes.Float(...decoder(d));
};
msgpack.registerDecoder(4, floatDecode);

const complexDecode = d => {
  return new pyTypes.Complex(...decoder(d));
};
msgpack.registerDecoder(5, complexDecode);

const iteratorDecode = d => {
  return new pyTypes.Iterator(...decoder(d));
};
msgpack.registerDecoder(6, iteratorDecode);

const listDecode = d => {
  var un = decoder(d);
  return new pyTypes.List(un[0], un[1]);
};
msgpack.registerDecoder(7, listDecode);

const tupleDecode = d => {
  return new pyTypes.Tuple(...decoder(d));
};
msgpack.registerDecoder(8, tupleDecode);

// RANGE
const rangeDecode = d => {
  return new pyTypes.Range(...decoder(d));
};
msgpack.registerDecoder(9, rangeDecode);

const strDecode = d => {
  return new pyTypes.Str(...decoder(d));
};
msgpack.registerDecoder(10, strDecode);

// BYTES - 11
const bytesDecode = d => {
  return new pyTypes.Bytes(...decoder(d));
};
msgpack.registerDecoder(11, bytesDecode);

// BYTEARRAY - 12
const ByteArrayDecode = d => {
  return new pyTypes.ByteArray(...decoder(d));
};
msgpack.registerDecoder(12, ByteArrayDecode);

// MEMORYVIEW - 13
const MemoryViewDecode = d => {
  return new pyTypes.MemoryView(...decoder(d));
};
msgpack.registerDecoder(13, MemoryViewDecode);

const setDecode = d => {
  return new pyTypes.PySet(...decoder(d));
};
msgpack.registerDecoder(14, setDecode);

// FROZEN SET - 15
const FrozenSetDecode = d => {
  return new pyTypes.FrozenSet(...decoder(d));
};
msgpack.registerDecoder(15, FrozenSetDecode);

const dictDecode = d => {
  return new pyTypes.Dict(...decoder(d));
};
msgpack.registerDecoder(16, dictDecode);

// CONTEXT MANAGER - 17
const ContextManagerDecode = d => {
  return new pyTypes.ContextManager(...decoder(d));
};
msgpack.registerDecoder(17, ContextManagerDecode);

// MODULE - 18
const ModuleDecode = d => {
  return new pyTypes.Module(...decoder(d));
};
msgpack.registerDecoder(18, ModuleDecode);

// FUCNTIon
const callableDecode = d => {
  return new pyTypes.Callable(...decoder(d));
};
msgpack.registerDecoder(19, callableDecode);

// METHOD -20
const MethodDecode = d => {
  return new pyTypes.Method(...decoder(d));
};
msgpack.registerDecoder(20, MethodDecode);

// CODE - 21
const CodeDecode = d => {
  return new pyTypes.Code(...decoder(d));
};
msgpack.registerDecoder(21, CodeDecode);

// TYPE - 22
const TypeDecode = d => {
  return new pyTypes.Type(...decoder(d));
};
msgpack.registerDecoder(22, TypeDecode);

const NoneTypeDecode = d => {
  return new pyTypes.NoneType(...decoder(d));
};
msgpack.registerDecoder(23, NoneTypeDecode);

// ELLIPSIS - 24
const EllipsisDecode = d => {
  return new pyTypes.Ellipsis(...decoder(d));
};
msgpack.registerDecoder(24, EllipsisDecode);

// NOTIMPLEMENTED - 25
const NotImplementedDecode = d => {
  return new pyTypes.NotImplemented(...decoder(d));
};
msgpack.registerDecoder(25, NotImplementedDecode);

const boolDecode = d => {
  return new pyTypes.Bool(...decoder(d));
};
msgpack.registerDecoder(26, boolDecode);

const datetimeDecode = d => {
  return new pyTypes.Datetime(...decoder(d));
};
msgpack.registerDecoder(27, datetimeDecode);

/**
 * NUMPY: 30
 */

/**
 * NUMPY+PANDAS+MATPLOTLIB: 90-100
 */

const npArrayDecode = d => {
  return new pyTypes.NpArray(...decoder(d));
};
msgpack.registerDecoder(91, npArrayDecode);

const pandsSeriesDecode = d => {
  return new pyTypes.PandasSeries(...decoder(d));
};
msgpack.registerDecoder(94, pandsSeriesDecode);

const pandsFrameDecode = d => {
  return new pyTypes.PandasDataFrame(...decoder(d));
};
msgpack.registerDecoder(95, pandsFrameDecode);

// FIGURE
// 99
const FigureDecode = d => {
  return new pyTypes.Figure(...decoder(d));
};
msgpack.registerDecoder(99, FigureDecode);

// PARTICULAR TO YAPIJ

const endDecode = d => {
  return new pyTypes.EndStr(...decoder(d));
};
msgpack.registerDecoder(124, endDecode);

const sepDecode = d => {
  return new pyTypes.SepStr(...decoder(d));
};
msgpack.registerDecoder(125, sepDecode);

const docstringDecode = d => {
  return new pyTypes.Docstring(...decoder(d));
};
msgpack.registerDecoder(126, docstringDecode);

const unknownDecode = d => {
  return new pyTypes.UnknownPyType(...decoder(d));
};
msgpack.registerDecoder(127, unknownDecode);

/**
 * Wrapper Class
 */
class Unpacker {
  output(sessionId, d) {
    var out = decoder(d);
    if (!out.value) {
      if (out[1] === " " || out[1] === "\n") {
        out = new pyTypes.Str(...out);
      } else {
        out = new pyTypes.UnhandledType(out);
      }
    }
    out.setSessionId(sessionId);
    return out;
  }

  status(sessionId, d) {
    return new Status(sessionId, d[0].toString(), parseInt(d[1].toString()));
  }
}

module.exports = Unpacker;
