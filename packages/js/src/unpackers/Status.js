const statusTypes1 = {
  0: "RECEIVED",
  1: "QUEUED_EXEC",
  2: "STARTED_EXEC",
  3: "FINISHED_EXEC",
  4: "INTERRUPT_EXEC",
  5: "ERROR_EXEC",
  6: "STARTED_MAGIC",
  7: "FINISHED_MAGIC",
  8: "ERROR_MAGIC"
};

const statusTypes0 = {
  0: "RECEIVED",
  1: "QUEUED",
  2: "STARTED",
  3: "FINISHED"
};

class Status {
  constructor(sessionId, uid, code) {
    this.sessionId = sessionId;
    this.uid = uid;
    this.date = new Date();

    this._initGetType(code);
  }

  _initGetType(code) {
    if (code === 0) {
      var code0 = 0;
    } else if (code === 1) {
      var code0 = 1;
    } else if (code === 2 || code === 6) {
      var code0 = 2;
    } else if (
      code === 3 ||
      code === 4 ||
      code === 5 ||
      code === 7 ||
      code === 8
    ) {
      var code0 = 3;
    }

    this.codes = [code0, code];
    this.names = [statusTypes0[code0], statusTypes1[code]];
  }

  get asObject() {
    return {
      uid: this.uid,
      sessionId: this.sessionId,
      codes: this.codes,
      names: this.names,
      date: this.date
    };
  }
}

module.exports = Status;
