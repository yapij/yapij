class PyType {
  constructor(uid, pyType, type) {
    this.uid = uid;
    this.date = new Date();
    this.pyType = "Py" + pyType;
    this.type = type;

    this.value = null;
  }

  get _v() {
    return this.value;
  }

  setSessionId(id) {
    this.sessionId = id;
  }

  get asObject() {
    return {
      uid: this.uid,
      sessionId: this.sessionId,
      type: this.type,
      pyType: this.pyType,
      value: this.value,
      date: this.date,
      output: true,
    };
  }

  fromObject(obj) {
    this.uid = obj.uid;
    this.date = obj.date;
    this.pyType = obj.pyType;
    this.type = obj.type;
    this.value = obj.value;
  }
}

class UnknownPyType extends PyType {
  constructor(uid, c) {
    super(uid, "UnknownPyType", c.type);
    this.value = c.str;
  }

  get str() {
    return this._v;
  }

  toString() {
    return `${this.type} [ ${this._v} ]`;
  }
}

class ListLike extends PyType {
  constructor(uid, pyType, type, args) {
    super(uid, pyType, type);
    this.value = args;
  }

  get values() {
    return this._v;
  }

  get length() {
    return this._v.length;
  }

  toString() {
    return `${this.type} [ ${this._v} ]`;
  }
}

class List extends ListLike {
  constructor(uid, args) {
    super(uid, "List", "list", args);
  }
}

class PySet extends ListLike {
  constructor(uid, args) {
    super(uid, "Set", "set", args);
  }
}

class Tuple extends ListLike {
  constructor(uid, args) {
    super(uid, "Tuple", "tuple", args);
  }
}

class NpArray extends PyType {
  constructor(uid, { b, s, t }) {
    super(uid, "NpArray", "numpy.ndarray");
    this.value = { values: b, shape: s, dtype: t };
  }

  get values() {
    return this._v.b;
  }

  get shape() {
    return this._v.s;
  }

  get dtype() {
    return this._v.t;
  }

  toString() {
    return `NpArray [ ... ] (${this._v.dtype}, ${this._v.shape})`;
  }
}

class Complex extends PyType {
  constructor(uid, c) {
    super(uid, "Complex", "complex");
    this.value = c;
  }

  get real() {
    return this._v.r;
  }
  get imag() {
    return this._v.i;
  }

  toString() {
    return `${this._v.r} + ${this._v.i}j`;
  }
}

class Callable extends PyType {
  constructor(uid, c) {
    super(uid, "Callable", "callable");
    this.value = c;
  }

  toString() {
    return `Callable [ ${this._v} ]`;
  }
}

class Exception extends PyType {
  constructor(uid, c) {
    super(uid, "Exception", c.type);
    this.value = c;
  }

  get traceback() {
    return this._v.tb;
  }

  get tb() {
    return this._v.tb;
  }

  get lineno() {
    return this._v.lineno;
  }

  get args() {
    return this._v.args;
  }

  toString() {
    return `${this._v.type} [ ${this._v.args} ]\n${this._v.tb}`;
  }
}

class Warning extends PyType {
  constructor(uid, c) {
    super(uid, "Warning", c.type);
    this.value = c;
  }

  get file() {
    return this._v.file;
  }

  get lineno() {
    return this._v.lineno;
  }

  get msg() {
    return this._v.msg;
  }

  get source() {
    return this._v.source;
  }
}

class PandasAbstractFrame extends PyType {
  constructor(uid, pyType, type, c) {
    super(uid, pyType, type);
    this.value = c;
  }

  get values() {
    return this._v.d;
  }

  get index() {
    return this._v.i;
  }

  get columns() {
    return this._v.c;
  }

  get shape() {
    return this._v.s;
  }

  toString() {
    return `${this.type} [ ... ] (${this._v.s})`;
  }
}

class PandasDataFrame extends PandasAbstractFrame {
  constructor(uid, c) {
    super(uid, "PandasDataFrame", "pandas.DataFrame", c);
  }
}

class PandasSeries extends PandasAbstractFrame {
  constructor(uid, c) {
    super(uid, "PandasSeries", "pandas.Series", c);
  }
}

class BuiltInTypes extends PyType {
  constructor(uid, pyType, type, c) {
    super(uid, pyType, type);
    this.value = c;
  }

  toString() {
    return `${this.type} [ ${this._v} ]`;
  }
}

class Str extends BuiltInTypes {
  constructor(uid, c) {
    super(uid, "Str", "str", c);
  }
}

class Dict extends BuiltInTypes {
  constructor(uid, c) {
    super(uid, "Dict", "dict", c);
  }
}

class Float extends BuiltInTypes {
  constructor(uid, c) {
    super(uid, "Float", "float", c);
  }
}

class Int extends BuiltInTypes {
  constructor(uid, c) {
    super(uid, "Int", "int", c);
  }
}

class Bool extends BuiltInTypes {
  constructor(uid, c) {
    super(uid, "Bool", "bool", c === 1);
  }
}

class UnhandledType extends PyType {
  constructor(uid, c) {
    super(uid, "UnhandledType", "UnhandledType");
    this.value = c;
  }
}

class Docstring extends PyType {
  constructor(uid, c) {
    super(uid, "Docstring", "Docstring");
    this.value = c;
  }

  _fmtDocstring() {
    var out = `${this.type} [ ${this._v.name || "Unknown Name"} ]\n`;
    out += this._v.name ? `     Name: ${this._v.name}\n` : "";
    out += this._v.type ? `     Type: ${this._v.type}\n` : "";
    out += this._v.signature ? `Signature: ${this._v.signature}\n` : "";
    out += this._v.dir ? `      Dir: ${this._v.dir}\n` : "";
    out += this._v.module ? `   Module: ${this._v.module}\n` : "";
    out += this._v.file ? `     File: ${this._v.file}\n` : "";
    out += this._v.docstring
      ? `Docstring: ${this._v.docstring}\n`
      : "(No docs)";
    return out;
  }

  toString() {
    return this._fmtDocstring();
  }

  get objType() {
    return this._v.type;
  }

  get name() {
    return this._v.name;
  }

  get signature() {
    return this._v.signature;
  }

  get dir() {
    return this._v.dir;
  }

  get module() {
    return this._v.module;
  }

  get file() {
    return this._v.file;
  }

  get docstring() {
    return this._v.docstring;
  }

  toString() {
    return this._fmtDocstring();
  }
}

class SepStr extends PyType {
  constructor(uid, c) {
    super(uid, "SepStr", "sep");
    this.value = c;
  }
}

class EndStr extends PyType {
  constructor(uid, c) {
    super(uid, "EndStr", "end");
    this.value = c;
  }
}

class Iterator extends PyType {
  constructor(uid, c) {
    super(uid, "Iterator", "iter");
    this.value = c;
  }
}

class Bytes extends PyType {
  constructor(uid, c) {
    super(uid, "Bytes", "bytes");
    this.value = c;
  }
}

class ByteArray extends PyType {
  constructor(uid, c) {
    super(uid, "ByteArray", "bytearray");
    this.value = c;
  }
}

class MemoryView extends PyType {
  constructor(uid, c) {
    super(uid, "MemoryView", "memoryview");
    this.value = c;
  }
}

class Range extends ListLike {
  constructor(uid, args) {
    super(uid, "Range", "range", args);
  }
}

class FrozenSet extends ListLike {
  constructor(uid, args) {
    super(uid, "FrozenSet", "frozenset", args);
  }
}

class ContextManager extends PyType {
  constructor(uid, c) {
    super(uid, "ContextManager", "ContextManager");
    this.value = c;
  }
}

class Module extends PyType {
  constructor(uid, c) {
    super(uid, "Module", "module");
    this.value = c;
  }
}

class Method extends PyType {
  constructor(uid, c) {
    super(uid, "Method", "method");
    this.value = c;
  }
}

class Code extends PyType {
  constructor(uid, c) {
    super(uid, "Code", "Code");
    this.value = c;
  }
}

class Type extends PyType {
  constructor(uid, c) {
    super(uid, "Type", "type");
    this.value = c;
  }
}

class NoneType extends BuiltInTypes {
  constructor(uid, c) {
    super(uid, "NoneType", "None", "None");
  }
}

class Ellipsis extends PyType {
  constructor(uid, c) {
    super(uid, "Ellipsis", "ellipsis");
    this.value = c;
  }
}

class NotImplemented extends PyType {
  constructor(uid, c) {
    super(uid, "NotImplemented", "NotImplemented");
    this.value = c;
  }
}

class Figure extends PyType {
  constructor(uid, c) {
    super(uid, "Figure", "matplotlib.figure.Figure");
    this.value = c;
  }
}

class Datetime extends PyType {
  constructor(uid, c) {
    super(uid, "Datetime", "datetime.datetime");
    this.value = new Date(c);
  }
}

module.exports = {
  UnknownPyType,
  List,
  PySet,
  Tuple,
  NpArray,
  Complex,
  Callable,
  Exception,
  Warning,
  PandasDataFrame,
  PandasSeries,
  Str,
  Dict,
  Float,
  Int,
  Bool,
  UnhandledType,
  Docstring,
  NoneType,
  SepStr,
  EndStr,
  Iterator,
  Range,
  Bytes,
  ByteArray,
  MemoryView,
  FrozenSet,
  ContextManager,
  Module,
  Method,
  Code,
  Type,
  Ellipsis,
  NotImplemented,
  Figure,
  Datetime,
};
