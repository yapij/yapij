const child_process = require("child_process");
const os = require("os");
const Path = require("path");

const { defaultOutputHandlers } = require("./defaults.js");
const AbstractProcess = require("./AbstractProcess.js");

class Kernel extends AbstractProcess {
  constructor(ports, stopServer, pythonArgs, config) {
    super(defaultOutputHandlers.kernel, ports, stopServer);
    this.ports = ports;
    this.pythonArgs = Object.assign(pythonArgs, ports);
    this.config = config;
    this.heartbeat_interval = pythonArgs.heartbeat_interval;

    this.ready = false;
  }

  start(ports = null, key = null) {
    return new Promise((resolve, reject) => {
      try {
        this._checkPorts(ports);
        this.key = key ? key : "";
        this.pythonArgs = Object.assign(this.pythonArgs, this.ports, {
          key: this.key,
        });

        this._startPython();
        resolve((this.ready = true));
      } catch (e) {
        reject(e);
      }
    });
  }

  _startPython() {
    var spawnCmd = `${this.config.python.defaultVenv.path} ${
      this.config.python.pythonEntryPoint
    } `;

    var pa = this.pythonArgs;
    for (let arg in pa) {
      if (this.pythonArgs[arg]) {
        spawnCmd += `--${arg} ${this.pythonArgs[arg]} `;
      }
    }

    if (!("key" in Object.keys(this.pythonArgs))) {
      spawnCmd += ` --key ${this.key}`;
    }

    this.proc = child_process.spawn(spawnCmd, this.config.spawn);

    this.proc.stderr.on("data", msg => {
      this._on.stderr(msg);
      this.stopServer();
    });
    this.proc.stdout.on("data", msg => {
      this._on.stdout(msg);
    });
    this.proc.on("exit", code => {
      this._on.close(code);
      this.stopServer();
    });
  }

  /**
   * Stops the process. Should not be called directly
   * @return {null}
   */
  _stop() {
    if (this.proc) {
      if (os.platform() !== "win32") {
        this.proc.kill();
      } else {
        child_process.exec(
          `taskkill /pid ${this.proc.pid} /T /F`,
          (err, stdout, stderr) => {
            if (!err) {
              null;
            } else {
              // throw new Error(err);
            }
          }
        );
      }
    }

    this.proc = null;
    this.ready = false;
  }
}

module.exports = Kernel;
