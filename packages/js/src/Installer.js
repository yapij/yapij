const child_process = require("child_process");
const Path = require("path");
const FS = require("fs");
// const bcrypt = require("bcrypt");

var config = require("./config.js");
const { yapijPyDownloadHash, saltRounds } = require("./constants.js");

class PythonVersion {
  constructor() {
    //https://docs.python.org/3/using/windows.html#installing-without-ui
    this.defaultAdvancedOptions = {
      InstallAllUsers: 0,
      DefaultAllUsersTargetDir: "C:\\",
      DefaultJustForMeTargetDir: "C:\\",
      DefaultCustomTargetDir: "C:\\",
      AssociatedFiles: 0,
      CompileAll: 1,
      PrependPath: 0,
      Shortcuts: 0,
      Include_doc: 1,
      Include_debug: 1,
      Include_dev: 1,
      Include_exe: 1,
      Include_launcher: 1,
      InstallLauncherAllUsers: 0,
      Include_lib: 1,
      Include_pip: 1,
      Include_tcltk: 0,
      Include_test: 1,
      Include_tools: 1,
      LauncherOnly: 0,
      SimpleInstall: 1,
      SimpleInstallDescription: "Install Python on Windows for YAPIJ.",
    };
  }

  /**
   * Search computer for python version and return info as well as whether it is suitable or not.
   * @param  {[type]} pyName Path to python.
   * @return {[type]}        Object with keys "python" (object suitable for saving to config) and "status" (int for determining whether it is suitable for use).
   */
  search(pyName, autoset) {
    return new Promise((resolve, reject) => {
      child_process.exec(`${pyName} -V`, (err, stdout, stderr) => {
        if (err) {
          resolve({ exec: null, v: null });
          return null;
        }
        var b = stdout.toString().split(" ");

        // Get both the name of python as well as the version (split so that, e.g., 3.7.1 => [3,7])
        resolve({
          exec: pyName,
          v: b[1]
            .split(".")
            .slice(0, 2)
            .map(ii => parseInt(ii)),
        });
      });
    }).then(p => {
      if (autoset) {
        return this.set(p);
      }
      return { python: p, status: this.check(p) };
    });
  }

  /**
   * Given a python executable and version, check whether it is suitable for use.
   * @param  {object} out An object with keys {exec: [null, string], v: [array[2, int], null]}
   * @return {int}     A status integer: "-2" => no python found. "-1" => python version is no good (<3.5). "1" => good to go.
   */
  check(p) {
    if (!p.exec) {
      return -2;
    } else {
      if (p.v[0] < 3 || (p.v[0] === 3) & (p.v[1] < 5)) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  get() {
    return config.get("python.python");
  }

  set(python) {
    if (Object.keys(python)[0] === "python") {
      python = python.python;
    }

    var out = {
      python: python,
      status: this.check(python),
      yapijInstalled: false,
    };

    if (out.status === 1) {
      config.set("python.python", python);
    }

    return out;
  }

  /**
   * Install Python from Web Installer (Windows only).
   * @param  {string}  dir             Name of directory where install should occur.
   * @param  {Object}  advancedOptions Optional options to pass to installer. See https://docs.python.org/3/using/windows.html#installing-without-ui and this.defaultAdvancedOptions for more info.
   * @param  {Boolean} autoset         If true will set the newly-installed path to be the python path on successful completion.
   * @return {Object}                  An object with keys python and status.
   */
  install(dir, advancedOptions = {}, autoset = true) {
    if (process.platform !== "win32") {
      return new Promise((resolve, reject) => {
        resolve({ python: { exec: null, v: null }, status: -3 });
      });
    }

    var o = Object.assign(this.defaultAdvancedOptions, advancedOptions);
    var installCommand = `${Path.resolve(
      Path.dirname(__filename),
      "static/python-3.7.1-webinstall.exe"
    )} /quiet TargetDir="${dir}" Shortcuts=${o.Shortcuts} Include_doc=${
      o.Include_doc
    } Include_debug=${o.Include_debug} Include_dev=${
      o.Include_dev
    } Include_exe=${o.Include_exe} Include_launcher=${
      o.Include_launcher
    } InstallLauncherAllUsers=${o.InstallLauncherAllUsers} Include_lib=${
      o.Include_lib
    } Include_pip=${o.Include_pip} Include_tcltk=${
      o.Include_tcltk
    } Include_test=${o.Include_test} Include_tools=${
      o.Include_tools
    } LauncherOnly=${o.LauncherOnly} SimpleInstall=${
      o.SimpleInstall
    } SimpleInstallDescription="${o.SimpleInstallDescription}"`;

    const P = [
      () =>
        new Promise((resolve, reject) => {
          FS.exists(dir, s => resolve(s));
        }),
      s =>
        new Promise((resolve, reject) => {
          if (!s) {
            FS.mkdir(dir, () => resolve(true));
          } else {
            resolve(true);
          }
        }),
      () =>
        new Promise((resolve, reject) =>
          child_process.exec(installCommand, (err, stdout, stderr) => {
            if (err) reject(err);
            resolve({ exec: Path.join(dir, "python.exe"), v: [3, 7] });
          })
        ),
      out => this.search(out.exec, autoset),
    ];

    // prettier-ignore
    return P[0]() // Checks that directory exists
      .then(s => P[1](s) // Makes directory if it doesn't exist
        .then(() => P[2]() // Runs installer
          .then(out => P[3](out)) // Make sure installation good and set
          )
        );
  }

  modify() {
    if (process.platform !== "win32") {
      return new Promise((resolve, reject) => {
        reject("Only works on windows for now.");
      });
    }

    var installCommand = `${Path.join(
      Path.dirname(__filename),
      "static/python-3.7.1-webinstall.exe"
    )}`;

    return new Promise((resolve, reject) =>
      child_process.exec(installCommand, (err, stdout, stderr) => {
        if (err && err.code !== 1602)
          reject({ err: err, stderr: stderr, stdout: stdout });
        resolve(true);
      })
    );
  }
}

class Venvs {
  constructor() {}

  /**
   * Get the set of Venvs currently registered
   * @return {[type]} [description]
   */
  get venv() {
    return config.get("python.venvDirs");
  }

  get default() {
    return config.get("python.defaultVenv");
  }

  /**
   * Add a pre-existing Venv to file
   */
  add(name, dir, isDefault = false) {
    var sn = name ? name : `yapij-venv-${Math.round(Math.random() * 1e9)}`;
    if (config.get("python.venvDirs").some(nm => nm === sn)) {
      name = `${name}-${Math.round(Math.random() * 1e9)}`;
    }

    // Check to ensure that required Venv files exist in directory.
    var P = [
      new Promise((resolve, reject) => {
        try {
          FS.stat(Path.resolve(dir, "pyvenv.cfg"), (err, s) => {
            if (err) reject(e);
            resolve(s.isFile());
          });
        } catch (e) {
          reject(e);
        }
      }),
      new Promise((resolve, reject) => {
        try {
          FS.stat(Path.resolve(dir, "Include"), (err, s) => {
            if (err) reject(e);
            resolve(s.isDirectory());
          });
        } catch (e) {
          reject(e);
        }
      }),
      new Promise((resolve, reject) => {
        try {
          FS.stat(Path.resolve(dir, "Scripts"), (err, s) => {
            if (err) reject(e);
            resolve(s.isDirectory());
          });
        } catch (e) {
          reject(e);
        }
      }),
      new Promise((resolve, reject) => {
        try {
          FS.stat(Path.resolve(dir, "Lib"), (err, s) => {
            if (err) reject(e);
            resolve(s.isDirectory());
          });
        } catch (e) {
          reject(e);
        }
      }),
    ];

    return Promise.all(P).then(out => {
      if (!out.some(ee => !ee)) {
        var newVenv = {
            path: dir,
            name: sn,
            isDefault: isDefault,
            yapijInstalled: false,
          },
          ovd = config.get("python.venvDirs");

        config.set("python.venvDirs", ovd.concat([newVenv]));
        if (isDefault) {
          config.set("python.defaultVenv", newVenv);
        }
        return true;
      } else {
        return false;
      }
    });
  }

  /**
   * Create a new Venv
   * @return {[type]} [description]
   */
  create(name, dir, isDefault = true) {
    var sn = name ? name : `yapij-venv-${Math.round(Math.random() * 1e9)}`;
    name = Path.resolve(dir, sn);
    if (config.get("python.venvDirs").some(nm => nm === name)) {
      name = `${name}-${Math.round(Math.random() * 1e9)}`;
    }

    return this._createVenv(
      name,
      `${config.get("python.python.exec")} -m venv ${name}`,
      true
    ).then(() => this.add(sn, name, isDefault));
  }

  _createVenv(p, cmd, tryAgain) {
    return new Promise((resolve, reject) => {
      child_process.exec(cmd, (err, stdout, stderr) => {
        if (err) {
          // May be necessary to specify that want python 3 in some cases.
          if (process.platform === "win32" && tryAgain) {
            this._createVenv(
              p,
              `${config.get("python.python.exec")} -3 -m venv ${p}`,
              false
            ).then(e => resolve(true));
          } else {
            reject(err);
          }
        }
        resolve(true);
      });
    });
  }

  /**
   * Remove a Venv from config. Optionally deletes from file system
   * @return {[type]} [description]
   */
  remove(name, deleteDir = false) {
    return new Promise((resolve, reject) => {
      var newVenvs = this.venv.filter(g => g.name !== name);
      if (deleteDir) {
        FS.unlink(this.venv.find(g => g.name === name).path, err => {
          if (err) reject(err);

          config.set("python.venvDirs", newVenvs);
          resolve(true);
        });
      } else {
        config.set("python.venvDirs", newVenvs);
        resolve(true);
      }
    });
  }

  rename(oldName, newName) {
    return new Promise((resolve, reject) => {
      var newVenvs = this.venv.map(g => {
        if (g.name === oldName) {
          return { path: g.path, name: newName };
        } else {
          return g;
        }
      });

      config.set("python.venvDirs", newVenvs);
    });
  }

  setDefault(name) {
    return new Promise((resolve, reject) => {
      if (!name) {
        let pe = config.get("python.python");
        config.set("python.defaultVenv", {
          path: pe.exec,
          name: `Python v${pe.v.join(".")}`,
          isDefault: true,
          yapijInstalled: pe.yapijInstalled,
        });
        resolve(true);
      }

      var ve = this.venv.map(v => {
        if (v.name === name) {
          v.isDefault = true;
        } else {
          v.isDefault = false;
        }
        return v;
      });
      var nd = ve.find(v => v.name === name);
      if (nd) {
        // @hack May be windows-only
        nd.path = Path.join(nd.path, "/Scripts/python.exe");
        config.set("python.defaultVenv", nd);
        resolve(true);
      } else {
        reject(`Venv "${name}" not in list of existing venvs.`);
      }
    });
  }
}

class YapijPy {
  constructor() {}

  get venv() {
    return config.get("python.venvDirs");
  }

  checkVenv(name) {
    return new Promise((resolve, reject) => {
      if (!venv) {
        var p = config.get("python.python");

        resolve({ path: "pip", name: "None" });
      }
      var p = config.get("python.venvDirs").find(v => v.name === venv);
      if (p) {
        resolve({
          path: Path.resolve(p.path, "Scripts/pip.exe"),
          name: p.name,
        });
      } else {
        reject(`Venv "${venv}" not found!`);
      }
    });
  }

  download(password, name) {
    var venv, path;
    if (!name) {
      var pp = config.get("python.python");
      path = "pip";
    } else {
      if (typeof name === "string") {
        venv = this.venv.find(v => v.name === name);
      } else {
        venv = name;
      }
      path = Path.join(venv.path, "Scripts/pip.exe");
    }

    return new Promise((resolve, reject) => {
      child_process.exec(`${path} install yapij-py`, (err, stdout, stderr) => {
        if (err) {
          reject(err);
        }

        if (!name) {
          pp.yapijInstalled = true;
          config.set("python.python", pp);
          resolve(true);
        } else {
          var venvs = this.venv.map(v => {
            if (v.name === venv.name) {
              v.yapijInstalled = true;
            }
            return v;
          });

          config.set("python.venvDirs", venvs);
          resolve(true);
        }
      });
    });
  }

  update(password, venv) {
    const P = [
      out =>
        new Promise((resolve, reject) => {
          if (out[0] && out[1]) {
            return out;
          } else {
            reject("Invalid password or venv passed.");
          }
        }),
      out =>
        new Promise((resolve, reject) => {
          child_process.exec(
            `${
              out[1].path
            } install yapij-py --upgrade -i https://api.python-private-package-index.com/0kjsWbi9m/ `,
            (err, stdout, stderr) => {
              if (err) {
                reject(err);
              }
              resolve(true);
            }
          );
        }),
    ];

    return Promise.all([this.checkPassword(password), this.checkVenv]).then(
      out => P[0].then(out => P[1](out))
    );
  }
}

/**
 * yapij install: Preset Steps for Ensuring YAPIJ is installed.
 *
 * Step 1: Identify Python Version and Program
 * Step 2: Configure virtualenvs
 * Step 3: Download yapij-py
 * Step 4: Advanced config: child_process, python entry point.
 */
class Installer {
  constructor(props) {
    this.PythonVersion = new PythonVersion();
    this.Venvs = new Venvs();
    this.YapijPy = new YapijPy();
    this.config = config;
  }

  reset() {
    this.config.reset();
  }
}

module.exports = Installer;
