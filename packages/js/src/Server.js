const shortid = require("shortid");
const findFreePort = require("find-free-port");

const Kernel = require("./Kernel.js");
const { defaultOutputHandlers, defaultPythonArgs } = require("./defaults.js");
var defaultConfig = require("./config.js");
const Workers = require("./workers");
const utils = require("./utils");

class Server {
  constructor(
    pythonArgs = {},
    config = defaultConfig.all,
    useCmdHandlers = true
  ) {
    this.key = null;
    this.useCmdHandlers = useCmdHandlers;

    this.pythonArgs = Object.assign(defaultPythonArgs, pythonArgs, {
      key: this.key,
    });

    this.config = {
      python: Object.assign(defaultConfig.get("python"), config.python || {}),
      spawn: Object.assign(defaultConfig.get("spawn"), config.spawn || {}),
    };

    this.ports = { frontend: null, backend: null, heartbeat_port: null };

    this.workers = {
      heartbeat: new Workers.Heartbeat(
        this.ports,
        this.stop,
        this.pythonArgs.heartbeat_interval
      ),
      bridge: new Workers.Bridge(this.ports),
      session: new Workers.Session(this.ports, this.useCmdHandlers),
    };
    this.kernel = new Kernel(
      this.ports,
      this.stop,
      this.pythonArgs,
      this.config
    );
  }

  start() {
    if (this.ready) {
      return new Promise((resolve, reject) => {
        resolve(true);
      });
    }

    return findFreePort(4000, 9000, "127.0.0.1", 3)
      .then(ports => {
        ports = ports.map(p => `tcp://127.0.0.1:${p}`);
        this.ports = {
          frontend: ports[0],
          backend: ports[1],
          heartbeat_port: ports[2],
        };
        this.key = shortid.generate();

        return Promise.all([
          this.kernel.start(this.ports, this.key),
          this.workers.heartbeat.start(this.ports),
          this.workers.bridge.start(this.ports),
          this.workers.session.start(this.ports, this.key),
        ]);
      })
      .then(v => {
        return { server: this, ports: this.ports };
      });
  }

  stop(force_all = false) {
    return new Promise((resolve, reject) => {
      if (!this.ready) {
        resolve(2);
      }

      var p = this.ports;

      // Close down python process
      if (this.kernel) {
        this.kernel._stop();
      }

      // Close down workers
      if (this.workers) {
        this.workers.session.send_stop(force_all);
        for (let w in this.workers) {
          if (this.workers[w]) {
            this.workers[w]._stop();
          }
        }
      }
      this.ports = { frontend: null, backend: null, heartbeat_port: null };
      this.key = null;

      resolve({ server: this, ports: p });
    });
  }

  restart() {
    return this.stop().then(() => {
      return this.start();
    });
  }

  setHandlers(handlers) {
    var nh = Object.assign(handlers, {});

    nh.server = Object.assign(handlers.server || {}, this._handlers.server);
    nh.session = Object.assign(handlers.session || {}, this._handlers.session);
    nh.heartbeat = Object.assign(
      handlers.heartbeat || {},
      this._handlers.heartbeat
    );
    nh.kernel = Object.assign(handlers.kernel || {}, this._handlers.kernel);

    this.on = Object.assign(nh, {});
    this.on.heartbeat.fail = msg => {
      nh.heartbeat.fail(msg);
      this.stop();
    };
    this.on.kernel.close = code => {
      nh.kernel.close(code);
      this.stop();
    };

    this._handlers = nh;
  }

  get ready() {
    return (
      this.workers.bridge &&
      this.workers.heartbeat &&
      this.workers.session &&
      this.kernel &&
      this.workers.bridge.ready &&
      this.workers.heartbeat.ready &&
      this.workers.session.ready &&
      this.kernel.ready
    );
  }

  exec(cmd, quiet = false) {
    return this.workers.session.send(cmd, quiet);
  }

  interrupt() {
    this.workers.session.send("%interrupt");
  }

  on(key, callback) {
    var k = key.split(".");

    if (k[0] === "heartbeat") {
      this.workers.heartbeat._on[k[1]] = callback;
    } else if (k[0] === "session") {
      this.workers.session._on[k[1]] = callback;
    } else if (k[0] === "kernel") {
      this.kernel._on[k[1]] = callback;
    }
  }
}

module.exports = Server;
