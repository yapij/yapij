const FS = require("fs");
const Path = require("path");

const Installer = require("./Installer.js");

// Long timeouts for long install/uninstall
jest.setTimeout(500000);

describe("Tests of the Installer component", () => {
  const inst = new Installer();
  const dir = FS.mkdtempSync("installer_test");

  describe("Can reset all", () => {
    expect(inst.reset()).toBeUndefined();
  });

  describe("PythonVersion - Installation of Python", () => {
    describe("Check version >= 3.5", () => {
      it("Working examples", () => {
        expect(inst.PythonVersion.check({ exec: "python", v: [3, 7] })).toEqual(
          1
        );
        expect(inst.PythonVersion.check({ exec: "python", v: [3, 6] })).toEqual(
          1
        );
        expect(inst.PythonVersion.check({ exec: "python", v: [3, 5] })).toEqual(
          1
        );
        expect(
          inst.PythonVersion.check({ exec: "pythoasfan", v: [3, 5] })
        ).toEqual(1);
      });

      it("No Python exec", () => {
        expect(inst.PythonVersion.check({ exec: null, v: [3, 7] })).toEqual(-2);
      });

      it("Version not high enough", () => {
        expect(inst.PythonVersion.check({ exec: "a", v: [2, 7] })).toEqual(-1);
        expect(inst.PythonVersion.check({ exec: "a", v: [3, 4] })).toEqual(-1);
      });
    });

    describe("Can search for python version", () => {
      it("Should exist", () => {
        expect.assertions(1);

        return inst.PythonVersion.search("python", false).then(out => {
          expect(out.status).toBe(1);
        });
      });

      it("Should not exist", () => {
        expect.assertions(1);

        return inst.PythonVersion.search("pythasfan", false).then(out => {
          expect(out.status).toBe(-2);
        });
      });
    });

    it("Can modify/uninstall", () => {
      return expect(inst.PythonVersion.modify()).resolves.toEqual(true);
    });

    it("Can install", () => {
      return expect(
        inst.PythonVersion.install(Path.resolve(dir, "YapijTestSuite"))
      ).resolves.toEqual({
        python: {
          exec: Path.resolve(dir, "YapijTestSuite", "python.exe"),
          v: [3, 7],
        },
        status: 1,
      });
    });
  });

  describe("Virtual Environments", () => {
    it("Add pre-existing venvs - should fail", () => {
      expect(inst.Venvs.add("afa", dir)).resolves.toBe(false);
    });

    it("create a new venv", () => {
      expect(inst.Venvs.create("testVenv", dir)).resolves.toBe(true);
    });

    it("Remove the venv from config but not all of it.", () => {
      expect(inst.Venvs.remove("testVenv", false)).resolves.toBe(true);
    });

    it("Add pre-existing venvs with new name: should succeed", () => {
      expect(
        inst.Venvs.add("testVenv2", Path.resolve(dir, "testVenv"))
      ).resolves.toBe(true);
    });

    it("rename a venv", () => {
      expect(inst.Venvs.rename("testVenv2", "testVenv")).resolves.toBe(true);
    });

    it("delete venv from config and memory", () => {
      expect(inst.Venvs.remove("testVenv", true)).resolves.toBe(true);
    });
  });
});

/*
var Installer = require('./src/Installer.js');
var pi = new Installer()

pi.PythonVersion.modify().then(g => console.log(g)).catch(e => console.error(e))
pi.PythonVersion.install('C:\\Temp\\PythonYapij2', {}, false).then(out=> console.log(out)).catch(e => {console.error(e)})

pi.Venvs.add("afa", "C:/Temp").then(e => console.log(e))


C:\\Temp\\PythonYapij2\\python.exe -m venv testVenv

var p = new Promise((resolve, reject) => {
  child_process.exec(`${pyName} -V`, (err, stdout, stderr) => {
    if (err) {
      resolve({ exec: null, v: null });
      return null;
    }
    var b = stdout.toString().split(" ");
    console.log(b);
    console.log(stdout);
    resolve(true);
  });
});

 */
